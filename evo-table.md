List of Swift Evolution proposals, indicating whether they introduce new syntax and whether that new syntax is accepted by tree-sitter-swifter. Proposals that intended to introduce new syntax but weren't accepted are marked as "n/a" in the "Implemented?" column. An ellipsis indicates that the proposal's status hasn't been resolved yet.

| Number | Syntax? | Implemented? | Description/Link                                                                                                                                                                                                                        |
|--------|---------|--------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 0001   | y       | y            | [Allow (most) keywords as argument labels](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0001-keywords-as-argument-labels.md) |
| 0002   | n       | -            | [Removing currying `func` declaration syntax](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0002-remove-currying.md) |
| 0003   | y       | y            | [Removing `var` from Function Parameters](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0003-remove-var-parameters.md) |
| 0004   | n       | -            | [Remove the `++` and `--` operators](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0004-remove-pre-post-inc-decrement.md) |
| 0005   | n       | -            | [Better Translation of Objective-C APIs Into Swift](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0005-objective-c-name-translation.md) |
| 0006   | n       | -            | [Apply API Guidelines to the Standard Library](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0006-apply-api-guidelines-to-the-standard-library.md) |
| 0007   | y       | y            | [Remove C-style for-loops with conditions and incrementers](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0007-remove-c-style-for-loops.md) |
| 0008   | n       | -            | [Add a Lazy flatMap for Sequences of Optionals](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0008-lazy-flatmap-for-optionals.md) |
| 0009   | n       | -            | [Require self for accessing instance members](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0009-require-self-for-accessing-instance-members.md) |
| 0010   | n       | -            | [Add StaticString.UnicodeScalarView](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0010-add-staticstring-unicodescalarview.md) |
| 0011   | y       | y            | [Replace `typealias` keyword with `associatedtype` for associated type declarations](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0011-replace-typealias-associated.md) |
| 0012   | n       | -            | [Add `@noescape` to public library API](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0012-add-noescape-to-public-library-api.md) |
| 0013   | n       | -            | [Remove Partial Application of Non-Final Super Methods (Swift 2.2)](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0013-remove-partial-application-super.md) |
| 0014   | n       | -            | [Constraining `AnySequence.init`](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0014-constrained-AnySequence.md) |
| 0015   | n       | -            | [Tuple comparison operators](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0015-tuple-comparison-operators.md) |
| 0016   | n       | -            | [Add initializers to Int and UInt to convert from UnsafePointer and UnsafeMutablePointer](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0016-initializers-for-converting-unsafe-pointers-to-ints.md) |
| 0017   | n       | -            | [Change `Unmanaged` to use `UnsafePointer`](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0017-convert-unmanaged-to-use-unsafepointer.md) |
| 0018   | y       | n/a          | [Flexible Memberwise Initialization](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0018-flexible-memberwise-initialization.md) |
| 0019   | n       | -            | [Swift Testing](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0019-package-manager-testing.md) |
| 0020   | y       | y            | [Swift Language Version Build Configuration](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0020-if-swift-version.md) |
| 0021   | y       | y            | [Naming Functions with Argument Labels](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0021-generalized-naming.md) |
| 0022   | y       | y            | [Referencing the Objective-C selector of a method](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0022-objc-selectors.md) |
| 0023   | n       | -            | [API Design Guidelines](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0023-api-guidelines.md) |
| 0024   | n       | -            | [Optional Value Setter `??=`](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0024-optional-value-setter.md) |
| 0025   | n       | -            | [Scoped Access Level](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0025-scoped-access-level.md) |
| 0026   | y       | n/a          | [Abstract classes and methods](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0026-abstract-classes-and-methods.md) |
| 0027   | n       | -            | [Expose code unit initializers on String](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0027-string-from-code-units.md) |
| 0028   | y       | y            | [Modernizing Swift's Debugging Identifiers](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0028-modernizing-debug-identifiers.md) |
| 0029   | n       | -            | [Remove implicit tuple splat behavior from function applications](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0029-remove-implicit-tuple-splat.md) |
| 0030   | y       | n/a          | [Property Behaviors](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0030-property-behavior-decls.md) |
| 0031   | y       | y            | [Adjusting `inout` Declarations for Type Decoration](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0031-adjusting-inout-declarations.md) |
| 0032   | n       | -            | [Add `first(where:)` method to `Sequence`](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0032-sequencetype-find.md) |
| 0033   | n       | -            | [Import Objective-C Constants as Swift Types](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0033-import-objc-constants.md) |
| 0034   | y       | y            | [Disambiguating Line Control Statements from Debugging Identifiers](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0034-disambiguating-line.md) |
| 0035   | n       | -            | [Limiting `inout` capture to `@noescape` contexts](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0035-limit-inout-capture.md) |
| 0036   | n       | -            | [Requiring Leading Dot Prefixes for Enum Instance Member Implementations](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0036-enum-dot.md) |
| 0037   | y       | y¹           | [Clarify interaction between comments & operators](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0037-clarify-comments-and-operators.md) |
| 0038   | n       | -            | [Package Manager C Language Target Support](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0038-swiftpm-c-language-targets.md) |
| 0039   | y       | y            | [Modernizing Playground Literals](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0039-playgroundliterals.md) |
| 0040   | y       | y            | [Replacing Equal Signs with Colons For Attribute Arguments](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0040-attributecolons.md) |
| 0041   | n       | -            | [Updating Protocol Naming Conventions for Conversions](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0041-conversion-protocol-conventions.md) |
| 0042   | n       | -            | [Flattening the function type of unapplied method references](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0042-flatten-method-types.md) |
| 0043   | y       | y            | [Declare variables in 'case' labels with multiple patterns](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0043-declare-variables-in-case-labels-with-multiple-patterns.md) |
| 0044   | n       | -            | [Import as member](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0044-import-as-member.md) |
| 0045   | n       | -            | [Add prefix(while:) and drop(while:) to the stdlib](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0045-scan-takewhile-dropwhile.md) |
| 0046   | n       | -            | [Establish consistent label behavior across all parameters including first labels](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0046-first-label.md) |
| 0047   | n       | -            | [Defaulting non-Void functions so they warn on unused results](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0047-nonvoid-warn.md) |
| 0048   | y       | y            | [Generic Type Aliases](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0048-generic-typealias.md) |
| 0049   | y       | y            | [Move @noescape and @autoclosure to be type attributes](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0049-noescape-autoclosure-type-attrs.md) |
| 0050   | n       | -            | [Decoupling Floating Point Strides from Generic Implementations](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0050-floating-point-stride.md) |
| 0051   | n       | -            | [Conventionalizing `stride` semantics](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0051-stride-semantics.md) |
| 0052   | n       | -            | [Change IteratorType post-nil guarantee](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0052-iterator-post-nil-guarantee.md) |
| 0053   | y       | y            | [Remove explicit use of `let` from Function Parameters](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0053-remove-let-from-function-parameters.md) |
| 0054   | n       | -            | [Abolish `ImplicitlyUnwrappedOptional` type](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0054-abolish-iuo.md) |
| 0055   | n       | -            | [Make unsafe pointer nullability explicit using Optional](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0055-optional-unsafe-pointers.md) |
| 0056   | y       | n/a          | [Allow trailing closures in `guard` conditions](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0056-trailing-closures-in-guard.md) |
| 0057   | n       | -            | [Importing Objective-C Lightweight Generics](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0057-importing-objc-generics.md) |
| 0058   | n       | -            | [Allow Swift types to provide custom Objective-C representations](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0058-objectivecbridgeable.md) |
| 0059   | n       | -            | [Update API Naming Guidelines and Rewrite Set APIs Accordingly](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0059-updated-set-apis.md) |
| 0060   | n       | -            | [Enforcing order of defaulted parameters](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0060-defaulted-parameter-order.md) |
| 0061   | n       | -            | [Add Generic Result and Error Handling to autoreleasepool()](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0061-autoreleasepool-signature.md) |
| 0062   | y       | y            | [Referencing Objective-C key-paths](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0062-objc-keypaths.md) |
| 0063   | n       | -            | [SwiftPM System Module Search Paths](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0063-swiftpm-system-module-search-paths.md) |
| 0064   | n       | -            | [Referencing the Objective-C selector of property getters and setters](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0064-property-selectors.md) |
| 0065   | n       | -            | [A New Model for Collections and Indices](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0065-collections-move-indices.md) |
| 0066   | y       | y            | [Standardize function type argument syntax to require parentheses](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0066-standardize-function-type-syntax.md) |
| 0067   | n       | -            | [Enhanced Floating Point Protocols](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0067-floating-point-protocols.md) |
| 0068   | y       | y            | [Expanding Swift `Self` to class members and value types](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0068-universal-self.md) |
| 0069   | n       | -            | [Mutability and Foundation Value Types](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0069-swift-mutability-for-foundation.md) |
| 0070   | n       | -            | [Make Optional Requirements Objective-C-only](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0070-optional-requirements.md) |
| 0071   | n       | -            | [Allow (most) keywords in member references](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0071-member-keywords.md) |
| 0072   | n       | -            | [Fully eliminate implicit bridging conversions from Swift](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0072-eliminate-implicit-bridging-conversions.md) |
| 0073   | y       | n/a          | [Marking closures as executing exactly once](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0073-noescape-once.md) |
| 0074   | n       | -            | [Implementation of Binary Search functions](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0074-binary-search.md) |
| 0075   | y       | y            | [Adding a Build Configuration Import Test](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0075-import-test.md) |
| 0076   | n       | -            | [Add overrides taking an UnsafePointer source to non-destructive copying methods on UnsafeMutablePointer](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0076-copying-to-unsafe-mutable-pointer-with-unsafe-pointer-source.md) |
| 0077   | y       | y            | [Improved operator declarations](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0077-operator-precedence.md) |
| 0078   | n       | -            | [Implement a rotate algorithm, equivalent to std::rotate() in C++](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0078-rotate-algorithm.md) |
| 0079   | n       | -            | [Allow using optional binding to upgrade `self` from a weak to strong reference](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0079-upgrade-self-from-weak-to-strong.md) |
| 0080   | n       | -            | [Failable Numeric Conversion Initializers](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0080-failable-numeric-initializers.md) |
| 0081   | y       | y            | [Move `where` clause to end of declaration](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0081-move-where-expression.md) |
| 0082   | n       | -            | [Package Manager Editable Packages](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0082-swiftpm-package-edit.md) |
| 0083   | n       | -            | [Remove bridging conversion behavior from dynamic casts](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0083-remove-bridging-from-dynamic-casts.md) |
| 0084   | y       | n/a          | [Allow trailing commas in parameter lists and tuples](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0084-trailing-commas.md) |
| 0085   | n       | -            | [Package Manager Command Names](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0085-package-manager-command-name.md) |
| 0086   | n       | -            | [Drop NS Prefix in Swift Foundation](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0086-drop-foundation-ns.md) |
| 0087   | y       | n/a          | [Rename `lazy` to `@lazy`](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0087-lazy-attribute.md) |
| 0088   | n       | -            | [Modernize libdispatch for Swift 3 naming conventions](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0088-libdispatch-for-swift3.md) |
| 0089   | n       | -            | [Renaming `String.init<T>(_: T)`](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0089-rename-string-reflection-init.md) |
| 0090   | y       | ...⁺         | [Remove `.self` and freely allow type references in expressions](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0090-remove-dot-self.md) |
| 0091   | y       | y            | [Improving operator requirements in protocols](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0091-improving-operators-in-protocols.md) |
| 0092   | y       | y            | [Typealiases in protocols and protocol extensions](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0092-typealiases-in-protocols.md) |
| 0093   | n       | -            | [Adding a public `base` property to slices](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0093-slice-base.md) |
| 0094   | n       | -            | [Add sequence(first:next:) and sequence(state:next:) to the stdlib](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0094-sequence-function.md) |
| 0095   | y       | y            | [Replace `protocol<P1,P2>` syntax with `P1 & P2` syntax](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0095-any-as-existential.md) |
| 0096   | n       | -            | [Converting `dynamicType` from a property to an operator](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0096-dynamictype.md) |
| 0097   | y       | n/a          | [Normalizing naming for "negative" attributes](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0097-negative-attributes.md) |
| 0098   | y       | n/a          | [Lowercase `didSet` and `willSet` for more consistent keyword casing](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0098-didset-capitalization.md) |
| 0099   | y       | y            | [Restructuring Condition Clauses](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0099-conditionclauses.md) |
| 0100   | n       | -            | [Add sequence-based initializers and merge methods to Dictionary](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0100-add-sequence-based-init-and-merge-to-dictionary.md) |
| 0101   | n       | -            | [Reconfiguring `sizeof` and related functions into a unified `MemoryLayout` struct](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0101-standardizing-sizeof-naming.md) |
| 0102   | y       | y            | [Remove `@noreturn` attribute and introduce an empty `Never` type](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0102-noreturn-bottom-type.md) |
| 0103   | y       | y            | [Make non-escaping closures the default](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0103-make-noescape-default.md) |
| 0104   | n       | -            | [Protocol-oriented integers](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0104-improved-integers.md) |
| 0105   | y       | n/a          | [Removing Where Clauses from For-In Loops](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0105-remove-where-from-forin-loops.md) |
| 0106   | y       | y            | [Add a `macOS` Alias for the `OSX` Platform Configuration Test](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0106-rename-osx-to-macos.md) |
| 0107   | n       | -            | [UnsafeRawPointer API](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0107-unsaferawpointer.md) |
| 0108   | n       | -            | [Remove associated type inference](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0108-remove-assoctype-inference.md) |
| 0109   | n       | -            | [Remove the `Boolean` protocol](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0109-remove-boolean.md) |
| 0110   | n       | -            | [Distinguish between single-tuple and multiple-argument function types](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0110-distinguish-single-tuple-arg.md) |
| 0111   | y       | y            | [Remove type system significance of function argument labels](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0111-remove-arg-label-type-significance.md) |
| 0112   | n       | -            | [Improved NSError Bridging](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0112-nserror-bridging.md) |
| 0113   | n       | -            | [Add integral rounding functions to FloatingPoint](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0113-rounding-functions-on-floatingpoint.md) |
| 0114   | n       | -            | [Updating Buffer "Value" Names to "Header" Names](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0114-buffer-naming.md) |
| 0115   | n       | -            | [Rename Literal Syntax Protocols](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0115-literal-syntax-protocols.md) |
| 0116   | n       | -            | [Import Objective-C `id` as Swift `Any` type](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0116-id-as-any.md) |
| 0117   | y       | y            | [Allow distinguishing between public access and public overridability](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0117-non-public-subclassable-by-default.md) |
| 0118   | n       | -            | [Closure Parameter Names and Labels](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0118-closure-parameter-names-and-labels.md) |
| 0119   | y       | n/a          | [Remove access modifiers from extensions](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0119-extensions-access-modifiers.md) |
| 0120   | n       | -            | [Revise `partition` Method Signature](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0120-revise-partition-method.md) |
| 0121   | n       | -            | [Remove `Optional` Comparison Operators](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0121-remove-optional-comparison-operators.md) |
| 0122   | y       | n/a          | [Use colons for subscript declarations](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0122-use-colons-for-subscript-type-declarations.md) |
| 0123   | n       | -            | [Disallow coercion to optionals in operator arguments](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0123-disallow-value-to-optional-coercion-in-operator-arguments.md) |
| 0124   | n       | -            | [`Int.init(ObjectIdentifier)` and `UInt.init(ObjectIdentifier)` should have a `bitPattern:` label](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0124-bitpattern-label-for-int-initializer-objectidentfier.md) |
| 0125   | n       | -            | [Remove `NonObjectiveCBase` and `isUniquelyReferenced`](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0125-remove-nonobjectivecbase.md) |
| 0126   | n       | -            | [Refactor Metatypes, repurpose `T.self` and `Mirror`](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0126-refactor-metatypes-repurpose-t-dot-self-and-mirror.md) |
| 0127   | n       | -            | [Cleaning up stdlib Pointer and Buffer Routines](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0127-cleaning-up-stdlib-ptr-buffer.md) |
| 0128   | n       | -            | [Change failable UnicodeScalar initializers to failable](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0128-unicodescalar-failable-initializer.md) |
| 0129   | n       | -            | [Package Manager Test Naming Conventions](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0129-package-manager-test-naming-conventions.md) |
| 0130   | n       | -            | [Replace repeating `Character` and `UnicodeScalar` forms of String.init](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0130-string-initializers-cleanup.md) |
| 0131   | n       | -            | [Add `AnyHashable` to the standard library](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0131-anyhashable.md) |
| 0132   | n       | -            | [Rationalizing Sequence end-operation names](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0132-sequence-end-ops.md) |
| 0133   | n       | -            | [Rename `flatten()` to `joined()`](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0133-rename-flatten-to-joined.md) |
| 0134   | n       | -            | [Rename two UTF8-related properties on String](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0134-rename-string-properties.md) |
| 0135   | n       | -            | [Package Manager Support for Differentiating Packages by Swift version](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0135-package-manager-support-for-differentiating-packages-by-swift-version.md) |
| 0136   | n       | -            | [Memory layout of values](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0136-memory-layout-of-values.md) |
| 0137   | n       | -            | [Avoiding Lock-In to Legacy Protocol Designs](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0137-avoiding-lock-in.md) |
| 0138   | n       | -            | [UnsafeRawBufferPointer](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0138-unsaferawbufferpointer.md) |
| 0139   | n       | -            | [Bridge Numeric Types to `NSNumber` and Cocoa Structs to `NSValue`](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0139-bridge-nsnumber-and-nsvalue.md) |
| 0140   | n       | -            | [Warn when `Optional` converts to `Any`, and bridge `Optional` As Its Payload Or `NSNull`](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0140-bridge-optional-to-nsnull.md) |
| 0141   | y       | y            | [Availability by Swift version](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0141-available-by-swift-version.md) |
| 0142   | y       | y            | [Permit where clauses to constrain associated types](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0142-associated-types-constraints.md) |
| 0143   | y       | y            | [Conditional conformances](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0143-conditional-conformances.md) |
| 0144   | y       | n/a          | [Allow Single Dollar Sign as a Valid Identifier](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0144-allow-single-dollar-sign-as-valid-identifier.md) |
| 0145   | n       | -            | [Package Manager Version Pinning](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0145-package-manager-version-pinning.md) |
| 0146   | n       | -            | [Package Manager Product Definitions](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0146-package-manager-product-definitions.md) |
| 0147   | n       | -            | [Move UnsafeMutablePointer.initialize(from:) to UnsafeMutableBufferPointer](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0147-move-unsafe-initialize-from.md) |
| 0148   | y       | y            | [Generic Subscripts](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0148-generic-subscripts.md) |
| 0149   | n       | -            | [Package Manager Support for Top of Tree development](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0149-package-manager-top-of-tree.md) |
| 0150   | n       | -            | [Package Manager Support for branches](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0150-package-manager-branch-support.md) |
| 0151   | n       | -            | [Package Manager Swift Language Compatibility Version](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0151-package-manager-swift-language-compatibility-version.md) |
| 0152   | n       | -            | [Package Manager Tools Version](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0152-package-manager-tools-version.md) |
| 0153   | n       | -            | [Compensate for the inconsistency of `@NSCopying`'s behaviour](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0153-compensate-for-the-inconsistency-of-nscopyings-behaviour.md) |
| 0154   | n       | -            | [Provide Custom Collections for Dictionary Keys and Values](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0154-dictionary-key-and-value-collections.md) |
| 0155   | n       | -            | [Normalize Enum Case Representation](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0155-normalize-enum-case-representation.md) |
| 0156   | n       | -            | [Class and Subtype existentials](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0156-subclass-existentials.md) |
| 0157   | n       | -            | [Support recursive constraints on associated types](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0157-recursive-protocol-constraints.md) |
| 0158   | n       | -            | [Package Manager Manifest API Redesign](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0158-package-manager-manifest-api-redesign.md) |
| 0159   | y       | n/a          | [Fix Private Access Levels](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0159-fix-private-access-levels.md) |
| 0160   | n       | -            | [Limiting `@objc` inference](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0160-objc-inference.md) |
| 0161   | y       | y            | [Smart KeyPaths: Better Key-Value Coding for Swift](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0161-key-paths.md) |
| 0162   | n       | -            | [Package Manager Custom Target Layouts](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0162-package-manager-custom-target-layouts.md) |
| 0163   | n       | -            | [string-revision-1](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0163-string-revision-1.md) |
| 0164   | y       | y            | [Remove final support in protocol extensions](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0164-remove-final-support-in-protocol-extensions.md) |
| 0165   | n       | -            | [Dictionary & Set Enhancements](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0165-dict.md) |
| 0166   | n       | -            | [Swift Archival & Serialization](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0166-swift-archival-serialization.md) |
| 0167   | n       | -            | [Swift Encoders](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0167-swift-encoders.md) |
| 0168   | y       | y            | [Multi-Line String Literals](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0168-multi-line-string-literals.md) |
| 0169   | n       | -            | [Improve Interaction Between `private` Declarations and Extensions](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0169-improve-interaction-between-private-declarations-and-extensions.md) |
| 0170   | n       | -            | [NSNumber bridging and Numeric types](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0170-nsnumber_bridge.md) |
| 0171   | n       | -            | [Reduce with `inout`](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0171-reduce-with-inout.md) |
| 0172   | y       | y            | [One-sided Ranges](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0172-one-sided-ranges.md) |
| 0173   | n       | -            | [Add `MutableCollection.swapAt(_:_:)`](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0173-swap-indices.md) |
| 0174   | n       | -            | [Change `RangeReplaceableCollection.filter` to return `Self`](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0174-filter-range-replaceable.md) |
| 0175   | n       | -            | [Package Manager Revised Dependency Resolution](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0175-package-manager-revised-dependency-resolution.md) |
| 0176   | n       | -            | [Enforce Exclusive Access to Memory](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0176-enforce-exclusive-access-to-memory.md) |
| 0177   | n       | -            | [Add clamp(to:) to the stdlib](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0177-add-clamped-to-method.md) |
| 0178   | n       | -            | [Add `unicodeScalars` property to `Character`](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0178-character-unicode-view.md) |
| 0179   | n       | -            | [Swift `run` Command](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0179-swift-run-command.md) |
| 0180   | n       | -            | [String Index Overhaul](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0180-string-index-overhaul.md) |
| 0181   | n       | -            | [Package Manager C/C++ Language Standard Support](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0181-package-manager-cpp-language-version.md) |
| 0182   | y       | y²           | [String Newline Escaping](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0182-newline-escape-in-strings.md) |
| 0183   | n       | -            | [Substring performance affordances](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0183-substring-affordances.md) |
| 0184   | n       | -            | [Unsafe\[Mutable\]\[Raw\]\[Buffer\]Pointer: add missing methods, adjust existing labels for clarity, and remove deallocation size](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0184-unsafe-pointers-add-missing.md) |
| 0185   | n       | -            | [Synthesizing `Equatable` and `Hashable` conformance](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0185-synthesize-equatable-hashable.md) |
| 0186   | n       | -            | [Remove ownership keyword support in protocols](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0186-remove-ownership-keyword-support-in-protocols.md) |
| 0187   | n       | -            | [Introduce Sequence.compactMap(_:)](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0187-introduce-filtermap.md) |
| 0188   | n       | -            | [Make Standard Library Index Types Hashable](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0188-stdlib-index-types-hashable.md) |
| 0189   | n       | -            | [Restrict Cross-module Struct Initializers](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0189-restrict-cross-module-struct-initializers.md) |
| 0190   | y       | y            | [Target environment platform condition](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0190-target-environment-platform-condition.md) |
| 0191   | n       | -            | [Eliminate `IndexDistance` from `Collection`](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0191-eliminate-indexdistance.md) |
| 0192   | y       | y            | [Handling Future Enum Cases](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0192-non-exhaustive-enums.md) |
| 0193   | n       | -            | [Cross-module inlining and specialization](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0193-cross-module-inlining-and-specialization.md) |
| 0194   | n       | -            | [Derived Collection of Enum Cases](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0194-derived-collection-of-enum-cases.md) |
| 0195   | n       | -            | [Introduce User-defined "Dynamic Member Lookup" Types](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0195-dynamic-member-lookup.md) |
| 0196   | y       | y            | [Compiler Diagnostic Directives](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0196-diagnostic-directives.md) |
| 0197   | n       | -            | [Adding in-place `removeAll(where:)` to the Standard Library](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0197-remove-where.md) |
| 0198   | n       | -            | [Playground QuickLook API Revamp](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0198-playground-quicklook-api-revamp.md) |
| 0199   | n       | -            | [Adding `toggle` to `Bool`](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0199-bool-toggle.md) |
| 0200   | y       | y            | [Enhancing String Literals Delimiters to Support Raw Text](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0200-raw-string-escaping.md) |
| 0201   | n       | -            | [Package Manager Local Dependencies](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0201-package-manager-local-dependencies.md) |
| 0202   | n       | -            | [Random Unification](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0202-random-unification.md) |
| 0203   | n       | -            | [Rename Sequence.elementsEqual](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0203-rename-sequence-elements-equal.md) |
| 0204   | n       | -            | [Add `last(where:)` and `lastIndex(where:)` Methods](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0204-add-last-methods.md) |
| 0205   | n       | -            | [`withUnsafePointer(to:_:)` and `withUnsafeBytes(of:_:)` for immutable values](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0205-withUnsafePointer-for-lets.md) |
| 0206   | n       | -            | [Hashable Enhancements](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0206-hashable-enhancements.md) |
| 0207   | n       | -            | [Add an `allSatisfy` algorithm to `Sequence`](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0207-containsOnly.md) |
| 0208   | n       | -            | [Package Manager System Library Targets](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0208-package-manager-system-library-targets.md) |
| 0209   | n       | -            | [Package Manager Swift Language Version API Update](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0209-package-manager-swift-lang-version-update.md) |
| 0210   | n       | -            | [Add an `offset(of:)` method to `MemoryLayout`](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0210-key-path-offset.md) |
| 0211   | n       | -            | [Add Unicode Properties to `Unicode.Scalar`](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0211-unicode-scalar-properties.md) |
| 0212   | y       | y            | [Compiler Version Directive](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0212-compiler-version-directive.md) |
| 0213   | n       | -            | [Literal initialization via coercion](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0213-literal-init-via-coercion.md) |
| 0214   | n       | -            | [Renaming the `DictionaryLiteral` type to `KeyValuePairs`](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0214-DictionaryLiteral.md) |
| 0215   | n       | -            | [Conform `Never` to `Equatable` and `Hashable`](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0215-conform-never-to-hashable-and-equatable.md) |
| 0216   | n       | -            | [Introduce user-defined dynamically "callable" types](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0216-dynamic-callable.md) |
| 0217   | n       | -            | [Introducing the `!!` "Unwrap or Die" operator to the Swift Standard Library](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0217-bangbang.md) |
| 0218   | n       | -            | [Introduce `compactMapValues` to Dictionary](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0218-introduce-compact-map-values.md) |
| 0219   | n       | -            | [Package Manager Dependency Mirroring](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0219-package-manager-dependency-mirroring.md) |
| 0220   | n       | -            | [`count(where:)`](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0220-count-where.md) |
| 0221   | n       | -            | [Character Properties](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0221-character-properties.md) |
| 0222   | n       | -            | [Lazy CompactMap Sequence](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0222-lazy-compactmap-sequence.md) |
| 0223   | n       | -            | [Accessing an Array's Uninitialized Buffer](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0223-array-uninitialized-initializer.md) |
| 0224   | y       | y            | [Support 'less than' operator in compilation conditions](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0224-ifswift-lessthan-operator.md) |
| 0225   | n       | -            | [Adding `isMultiple` to `BinaryInteger`](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0225-binaryinteger-iseven-isodd-ismultiple.md) |
| 0226   | n       | -            | [Package Manager Target Based Dependency Resolution](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0226-package-manager-target-based-dep-resolution.md) |
| 0227   | n       | -            | [Identity key path](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0227-identity-keypath.md) |
| 0228   | n       | -            | [Fix `ExpressibleByStringInterpolation`](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0228-fix-expressiblebystringinterpolation.md) |
| 0229   | n       | -            | [SIMD Vectors](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0229-simd.md) |
| 0230   | n       | -            | [Flatten nested optionals resulting from 'try?'](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0230-flatten-optional-try.md) |
| 0231   | n       | -            | [Optional Iteration](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0231-optional-iteration.md) |
| 0232   | n       | -            | [Remove Some Customization Points from the Standard Library's `Collection` Hierarchy](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0232-remove-customization-points.md) |
| 0233   | n       | -            | [Make  `Numeric`  Refine a new  `AdditiveArithmetic`  Protocol](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0233-additive-arithmetic-protocol.md) |
| 0234   | n       | -            | [Remove `Sequence.SubSequence`](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0234-remove-sequence-subsequence.md) |
| 0235   | n       | -            | [Add Result to the Standard Library](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0235-add-result.md) |
| 0236   | n       | -            | [Package Manager Platform Deployment Settings](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0236-package-manager-platform-deployment-settings.md) |
| 0237   | n       | -            | [Introduce `withContiguous{Mutable}StorageIfAvailable` methods](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0237-contiguous-collection.md) |
| 0238   | n       | -            | [Package Manager Target Specific Build Settings](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0238-package-manager-build-settings.md) |
| 0239   | n       | -            | [Add Codable conformance to Range types](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0239-codable-range.md) |
| 0240   | n       | -            | [Ordered Collection Diffing](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0240-ordered-collection-diffing.md) |
| 0241   | n       | -            | [Deprecate String Index Encoded Offsets](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0241-string-index-explicit-encoding-offset.md) |
| 0242   | n       | -            | [Synthesize default values for the memberwise initializer](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0242-default-values-memberwise.md) |
| 0243   | y       | n/a          | [Integer-convertible character literals](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0243-codepoint-and-character-literals.md) |
| 0244   | y       | y            | [Opaque Result Types](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0244-opaque-result-types.md) |
| 0245   | n       | -            | [Add an Array Initializer with Access to Uninitialized Storage](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0245-array-uninitialized-initializer.md) |
| 0246   | n       | -            | [Generic Math(s) Functions](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0246-mathable.md) |
| 0247   | n       | -            | [Contiguous Strings](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0247-contiguous-strings.md) |
| 0248   | n       | -            | [String Gaps and Missing APIs](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0248-string-gaps-missing-apis.md) |
| 0249   | n       | -            | [Key Path Expressions as Functions](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0249-key-path-literal-function-expressions.md) |
| 0250   | n       | -            | [Swift Code Style Guidelines and Formatter](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0250-swift-style-guide-and-formatter.md) |
| 0251   | n       | -            | [SIMD additions](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0251-simd-additions.md) |
| 0252   | n       | -            | [Key Path Member Lookup](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0252-keypath-dynamic-member-lookup.md) |
| 0253   | y       | y            | [Callable values of user-defined nominal types](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0253-callable.md) |
| 0254   | y       | y            | [Static and class subscripts](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0254-static-subscripts.md) |
| 0255   | n       | -            | [Implicit returns from single-expression functions](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0255-omit-return.md) |
| 0256   | n       | -            | [Introduce `{Mutable}ContiguousCollection` protocol](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0256-contiguous-collection.md) |
| 0257   | y       | n/a          | [Eliding commas from multiline expression lists](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0257-elide-comma.md) |
| 0258   | y       | y            | [Property Wrappers](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0258-property-wrappers.md) |
| 0259   | n       | -            | [Approximate Equality for Floating Point](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0259-approximately-equal.md) |
| 0260   | n       | -            | [Library Evolution for Stable ABIs](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0260-library-evolution.md) |
| 0261   | n       | -            | [Identifiable Protocol](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0261-identifiable.md) |
| 0262   | n       | -            | [Demangle Function](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0262-demangle.md) |
| 0263   | n       | -            | [Add a String Initializer with Access to Uninitialized Storage](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0263-string-uninitialized-initializer.md) |
| 0264   | n       | -            | [Standard Library Preview Package](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0264-stdlib-preview-package.md) |
| 0265   | n       | -            | [Offset-Based Access to Indices, Elements, and Slices](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0265-offset-indexing-and-slicing.md) |
| 0266   | n       | -            | [Synthesized `Comparable` conformance for `enum` types](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0266-synthesized-comparable-for-enumerations.md) |
| 0267   | y       | y            | [`where` clauses on contextually generic declarations](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0267-where-on-contextually-generic.md) |
| 0268   | n       | -            | [Refine `didSet` Semantics](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0268-didset-semantics.md) |
| 0269   | n       | -            | [Increase availability of implicit `self` in `@escaping` closures when reference cycles are unlikely to occur](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0269-implicit-self-explicit-capture.md) |
| 0270   | n       | -            | [Add Collection Operations on Noncontiguous Elements](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0270-rangeset-and-collection-operations.md) |
| 0271   | n       | -            | [Package Manager Resources](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0271-package-manager-resources.md) |
| 0272   | n       | -            | [Package Manager Binary Dependencies](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0272-swiftpm-binary-dependencies.md) |
| 0273   | n       | -            | [Package Manager Conditional Target Dependencies](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0273-swiftpm-conditional-target-dependencies.md) |
| 0274   | y       | y            | [Concise magic file names](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0274-magic-file.md) |
| 0275   | y       | n/a          | [Allow more characters (like whitespaces and punctuations) for escaped identifiers](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0275-allow-more-characters-like-whitespaces-and-punctuations-for-escaped-identifiers.md) |
| 0276   | y       | y            | [Multi-Pattern Catch Clauses](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0276-multi-pattern-catch-clauses.md) |
| 0277   | n       | -            | [float16](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0277-float16.md) |
| 0278   | n       | -            | [Package Manager Localized Resources](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0278-package-manager-localized-resources.md) |
| 0279   | y       | y            | [Multiple Trailing Closures](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0279-multiple-trailing-closures.md) |
| 0280   | n       | -            | [Enum cases as protocol witnesses](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0280-enum-cases-as-protocol-witnesses.md) |
| 0281   | n       | -            | [`@main`: Type-Based Program Entry Points](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0281-main-attribute.md) |
| 0282   | n       | -            | [Clarify the Swift memory consistency model ⚛︎](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0282-atomics.md) |
| 0283   | n       | -            | [Tuples Conform to `Equatable`, `Comparable`, and `Hashable`](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0283-tuples-are-equatable-comparable-hashable.md) |
| 0284   | n       | -            | [Allow Multiple Variadic Parameters in Functions, Subscripts, and Initializers](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0284-multiple-variadic-parameters.md) |
| 0285   | n       | -            | [Ease the transition to concise magic file strings](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0285-ease-pound-file-transition.md) |
| 0286   | n       | -            | [Forward-scan matching for trailing closures](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0286-forward-scan-trailing-closures.md) |
| 0287   | n       | -            | [Extend implicit member syntax to cover chains of member references](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0287-implicit-member-chains.md) |
| 0288   | n       | -            | [Adding `isPower(of:)` to `BinaryInteger`](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0288-binaryinteger-ispower.md) |
| 0289   | n       | -            | [Result builders](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0289-result-builders.md) |
| 0290   | y       | y            | [Unavailability Condition](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0290-negative-availability.md) |
| 0291   | n       | -            | [Package Collections](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0291-package-collections.md) |
| 0292   | n       | -            | [Package Registry Service](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0292-package-registry-service.md) |
| 0293   | y       | y            | [Extend Property Wrappers to Function and Closure Parameters](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0293-extend-property-wrappers-to-function-and-closure-parameters.md) |
| 0294   | n       | -            | [Declaring executable targets in Package Manifests](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0294-package-executable-targets.md) |
| 0295   | n       | -            | [Codable synthesis for enums with associated values](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0295-codable-synthesis-for-enums-with-associated-values.md) |
| 0296   | y       | y            | [Async/await](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0296-async-await.md) |
| 0297   | n       | -            | [Concurrency Interoperability with Objective-C](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0297-concurrency-objc.md) |
| 0298   | n       | -            | [Async/Await: Sequences](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0298-asyncsequence.md) |
| 0299   | n       | -            | [Extending Static Member Lookup in Generic Contexts](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0299-extend-generic-static-member-lookup.md) |
| 0300   | n       | -            | [Continuations for interfacing async tasks with synchronous code](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0300-continuation.md) |
| 0301   | n       | -            | [Package Editor Commands](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0301-package-editing-commands.md) |
| 0302   | n       | -            | [`Sendable` and `@Sendable` closures](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0302-concurrent-value-and-concurrent-closures.md) |
| 0303   | n       | -            | [Package Manager Extensible Build Tools](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0303-swiftpm-extensible-build-tools.md) |
| 0304   | n       | -            | [Structured concurrency](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0304-structured-concurrency.md) |
| 0305   | n       | -            | [Package Manager Binary Target Improvements](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0305-swiftpm-binary-target-improvements.md) |
| 0306   | y       | y            | [Actors](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0306-actors.md) |
| 0307   | n       | -            | [Allow interchangeable use of `CGFloat` and `Double` types](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0307-allow-interchangeable-use-of-double-cgfloat-types.md) |
| 0308   | y       | y            | [`#if` for postfix member expressions](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0308-postfix-if-config-expressions.md) |
| 0309   | n       | -            | [Unlock existentials for all protocols](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0309-unlock-existential-types-for-all-protocols.md) |
| 0310   | y       | y            | [Effectful Read-only Properties](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0310-effectful-readonly-properties.md) |
| 0311   | n       | -            | [Task Local Values](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0311-task-locals.md) |
| 0312   | n       | -            | [Add `indexed()` and `Collection` conformances for `enumerated()` and `zip(_:_:)`](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0312-indexed-and-enumerated-zip-collections.md) |
| 0313   | y       | y            | [Improved control over actor isolation](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0313-actor-isolation-control.md) |
| 0314   | n       | -            | [`AsyncStream` and `AsyncThrowingStream`](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0314-async-stream.md) |
| 0315   | y       | y            | [Type placeholders (formerly, "Placeholder types")](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0315-placeholder-types.md) |
| 0316   | n       | -            | [Global actors](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0316-global-actors.md) |
| 0317   | y       | y            | [`async let` bindings](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0317-async-let.md) |
| 0318   | n       | -            | [Package Creation](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0318-package-creation.md) |
| 0319   | n       | -            | [Conform Never to Identifiable](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0319-never-identifiable.md) |
| 0320   | n       | -            | [Allow coding of non `String` / `Int` keyed `Dictionary` into a `KeyedContainer`](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0320-codingkeyrepresentable.md) |
| 0321   | n       | -            | [Package Registry Service - Publish Endpoint](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0321-package-registry-publish.md) |
| 0322   | n       | -            | [Temporary uninitialized buffers](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0322-temporary-buffers.md) |
| 0323   | n       | -            | [Asynchronous Main Semantics](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0323-async-main-semantics.md) |
| 0324   | n       | -            | [Relax diagnostics for pointer arguments to C functions](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0324-c-lang-pointer-arg-conversion.md) |
| 0325   | n       | -            | [Additional Package Plugin APIs](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0325-swiftpm-additional-plugin-apis.md) |
| 0326   | n       | -            | [Enable multi-statement closure parameter/result type inference](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0326-extending-multi-statement-closure-inference.md) |
| 0327   | n       | -            | [On Actors and Initialization](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0327-actor-initializers.md) |
| 0328   | y       | y            | [Structural opaque result types](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0328-structural-opaque-result-types.md) |
| 0329   | n       | -            | [Clock, Instant, and Duration](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0329-clock-instant-duration.md) |
| 0330   | y       | ...⁺         | [Conditionals in Collections](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0330-collection-conditionals.md) |
| 0331   | n       | -            | [Remove Sendable conformance from unsafe pointer types](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0331-remove-sendable-from-unsafepointer.md) |
| 0332   | n       | -            | [Package Manager Command Plugins](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0332-swiftpm-command-plugins.md) |
| 0333   | n       | -            | [Expand usability of `withMemoryRebound`](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0333-with-memory-rebound.md) |
| 0334   | n       | -            | [Pointer API Usability Improvements](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0334-pointer-usability-improvements.md) |
| 0335   | n       | -            | [Introduce existential `any`](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0335-existential-any.md) |
| 0336   | n       | -            | [Distributed Actor Isolation](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0336-distributed-actor-isolation.md) |
| 0337   | n       | -            | [Incremental migration to concurrency checking](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0337-support-incremental-migration-to-concurrency-checking.md) |
| 0338   | n       | -            | [Clarify the Execution of Non-Actor-Isolated Async Functions](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0338-clarify-execution-non-actor-async.md) |
| 0339   | n       | -            | [Module Aliasing For Disambiguation](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0339-module-aliasing-for-disambiguation.md) |
| 0340   | y       | y            | [Unavailable From Async Attribute](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0340-swift-noasync.md) |
| 0341   | y       | y            | [Opaque Parameter Declarations](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0341-opaque-parameters.md) |
| 0342   | n       | -            | [Statically link Swift runtime libraries by default on supported platforms](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0342-static-link-runtime-libraries-by-default-on-supported-platforms.md) |
| 0343   | n       | -            | [Concurrency in Top-level Code](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0343-top-level-concurrency.md) |
| 0344   | n       | -            | [Distributed Actor Runtime](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0344-distributed-actor-runtime.md) |
| 0345   | y       | y            | [`if let` shorthand for shadowing an existing optional variable](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0345-if-let-shorthand.md) |
| 0346   | y       | y            | [Lightweight same-type requirements for primary associated types](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0346-light-weight-same-type-syntax.md) |
| 0347   | n       | -            | [Type inference from default expressions](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0347-type-inference-from-default-exprs.md) |
| 0348   | n       | -            | [`buildPartialBlock` for result builders](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0348-buildpartialblock.md) |
| 0349   | n       | -            | [Unaligned Loads and Stores from Raw Memory](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0349-unaligned-loads-and-stores.md) |
| 0350   | n       | -            | [Regex Type and Overview](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0350-regex-type-overview.md) |
| 0351   | n       | -            | [Regex builder DSL](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0351-regex-builder.md) |
| 0352   | n       | -            | [Implicitly Opened Existentials](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0352-implicit-open-existentials.md) |
| 0353   | y       | y            | [Constrained Existential Types](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0353-constrained-existential-types.md) |
| 0354   | y       | y³           | [Regex Literals](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0354-regex-literals.md) |
| 0355   | n       | -            | [Regex Syntax and Run-time Construction](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0355-regex-syntax-run-time-construction.md) |
| 0356   | n       | -            | [Swift Snippets](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0356-swift-snippets.md) |
| 0357   | n       | -            | [Regex-powered string processing algorithms](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0357-regex-string-processing-algorithms.md) |
| 0358   | n       | -            | [Primary Associated Types in the Standard Library](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0358-primary-associated-types-in-stdlib.md) |
| 0359   | y       | ...⁺         | [Build-Time Constant Values](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0359-build-time-constant-values.md) |
| 0360   | n       | -            | [Opaque result types with limited availability](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0360-opaque-result-types-with-availability.md) |
| 0361   | y       | y            | [Extensions on bound generic types](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0361-bound-generic-extensions.md) |
| 0362   | y       | y            | [Piecemeal adoption of upcoming language improvements](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0362-piecemeal-future-features.md) |
| 0363   | n⁴      | -            | [Unicode for String Processing](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0363-unicode-for-string-processing.md) |
| 0364   | y       | y            | [Warning for Retroactive Conformances of External Types](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0364-retroactive-conformance-warning.md) |
| 0365   | n       | -            | [Allow implicit `self` for `weak self` captures, after `self` is unwrapped](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0365-implicit-self-weak-capture.md) |
| 0366   | y       | y            | [`consume` operator to end the lifetime of a variable binding](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0366-move-function.md) |
| 0367   | y       | n            | [Conditional compilation for attributes](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0367-conditional-attributes.md) |
| 0368   | n       | -            | [StaticBigInt](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0368-staticbigint.md) |
| 0369   | n       | -            | [Add CustomDebugStringConvertible conformance to AnyKeyPath](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0369-add-customdebugdescription-conformance-to-anykeypath.md) |
| 0370   | n       | -            | [Pointer Family Initialization Improvements and Better Buffer Slices](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0370-pointer-family-initialization-improvements.md) |
| 0371   | n       | -            | [Isolated synchronous deinit](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0371-isolated-synchronous-deinit.md) |
| 0372   | n       | -            | [Document Sorting as Stable](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0372-document-sorting-as-stable.md) |
| 0373   | n       | -            | [Lift all limitations on variables in result builders](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0373-vars-without-limits-in-result-builders.md) |
| 0374   | n       | -            | [Add sleep(for:) to Clock](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0374-clock-sleep-for.md) |
| 0375   | n       | -            | [Opening existential arguments to optional parameters](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0375-opening-existential-optional.md) |
| 0376   | y       | y            | [Function Back Deployment](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0376-function-back-deployment.md) |
| 0377   | y       | y            | [`borrowing` and `consuming` parameter ownership modifiers](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0377-parameter-ownership-modifiers.md) |
| 0378   | n       | -            | [Package Registry Authentication](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0378-package-registry-auth.md) |
| 0379   | n       | -            | [Swift Opt-In Reflection Metadata](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0379-opt-in-reflection-metadata.md) |
| 0380   | y       | y            | [`if` and `switch` expressions](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0380-if-switch-expressions.md) |
| 0381   | n       | -            | [DiscardingTaskGroups](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0381-task-group-discard-results.md) |
| 0382   | y       | y            | [Expression Macros](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0382-expression-macros.md) |
| 0383   | n       | -            | [Deprecate @UIApplicationMain and @NSApplicationMain](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0383-deprecate-uiapplicationmain-and-nsapplicationmain.md) |
| 0384   | n       | -            | [Importing Forward Declared Objective-C Interfaces and Protocols](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0384-importing-forward-declared-objc-interfaces-and-protocols.md) |
| 0385   | n       | -            | [Custom Reflection Metadata](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0385-custom-reflection-metadata.md) |
| 0386   | y       | y            | [New access modifier: `package`](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0386-package-access-modifier.md) |
| 0387   | n       | -            | [Swift SDKs for Cross-Compilation](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0387-cross-compilation-destinations.md) |
| 0388   | n       | -            | [Convenience Async\[Throwing\]Stream.makeStream methods](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0388-async-stream-factory.md) |
| 0389   | n       | -            | [Attached Macros](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0389-attached-macros.md) |
| 0390   | y       | y            | [Noncopyable structs and enums](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0390-noncopyable-structs-and-enums.md) |
| 0391   | n       | -            | [Package Registry Publish](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0391-package-registry-publish.md) |
| 0392   | n       | -            | [Custom Actor Executors](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0392-custom-actor-executors.md) |
| 0393   | y       | y            | [Value and Type Parameter Packs](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0393-parameter-packs.md) |
| 0394   | n       | -            | [Package Manager Support for Custom Macros](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0394-swiftpm-expression-macros.md) |
| 0395   | n       | -            | [Observation](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0395-observability.md) |
| 0396   | n       | -            | [Conform `Never` to `Codable`](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0396-never-codable.md) |
| 0397   | n       | -            | [Freestanding Declaration Macros](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0397-freestanding-declaration-macros.md) |
| 0398   | y       | y            | [Allow Generic Types to Abstract Over Packs](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0398-variadic-types.md) |
| 0399   | y       | y            | [Tuple of value pack expansion](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0399-tuple-of-value-pack-expansion.md) |
| 0400   | y       | y            | [Init Accessors](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0400-init-accessors.md) |
| 0401   | n       | -            | [Remove Actor Isolation Inference caused by Property Wrappers](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0401-remove-property-wrapper-isolation.md) |
| 0402   | n       | -            | [Generalize `conformance` macros as `extension` macros](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0402-extension-macros.md) |
| 0403   | n       | -            | [Package Manager Mixed Language Target Support](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0403-swiftpm-mixed-language-targets.md) |
| 0404   | y       | y            | [Allow Protocols to be Nested in Non-Generic Contexts](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0404-nested-protocols.md) |
| 0405   | n       | -            | [String Initializers with Encoding Validation](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0405-string-validating-initializers.md) |
| 0406   | n       | -            | [Backpressure support for AsyncStream](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0406-async-stream-backpressure.md) |
| 0407   | n       | -            | [Member Macro Conformances](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0407-member-macro-conformances.md) |
| 0408   | y       | y            | [Pack Iteration](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0408-pack-iteration.md) |
| 0409   | y       | y            | [Access-level modifiers on import declarations](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0409-access-level-on-imports.md) |
| 0410   | n       | -            | [Low-Level Atomic Operations ⚛︎](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0410-atomics.md) |
| 0411   | n       | -            | [Isolated default value expressions](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0411-isolated-default-values.md) |
| 0412   | n       | -            | [Strict concurrency for global variables](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0412-strict-concurrency-for-global-variables.md) |
| 0413   | y       | y            | [Typed throws](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0413-typed-throws.md) |
| 0414   | n       | -            | [Region based Isolation](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0414-region-based-isolation.md) |
| 0415   | n       | -            | [Function Body Macros](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0415-function-body-macros.md) |
| 0416   | n       | -            | [Subtyping for keypath literals as functions](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0416-keypath-function-subtyping.md) |
| 0417   | n       | -            | [Task Executor Preference](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0417-task-executor-preference.md) |
| 0418   | n       | -            | [Inferring `Sendable` for methods and key path literals](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0418-inferring-sendable-for-methods.md) |
| 0419   | n       | -            | [Swift Backtrace API](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0419-backtrace-api.md) |
| 0420   | n       | -            | [Inheritance of actor isolation](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0420-inheritance-of-actor-isolation.md) |
| 0421   | n       | -            | [Generalize effect polymorphism for `AsyncSequence` and `AsyncIteratorProtocol`](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0421-generalize-async-sequence.md) |
| 0422   | n       | -            | [Expression macro as caller-side default argument](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0422-caller-side-default-argument-macro-expression.md) |
| 0423   | n       | -            | [Dynamic actor isolation enforcement from non-strict-concurrency contexts](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0423-dynamic-actor-isolation.md) |
| 0424   | n       | -            | [Custom isolation checking for SerialExecutor](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0424-custom-isolation-checking-for-serialexecutor.md) |
| 0425   | n       | -            | [int128](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0425-int128.md) |
| 0426   | n       | -            | [BitwiseCopyable](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0426-bitwise-copyable.md) |
| 0427   | y       | y            | [Noncopyable Generics](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0427-noncopyable-generics.md) |
| 0428   | n       | -            | [Resolve DistributedActor protocols](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0428-resolve-distributed-actor-protocols.md) |
| 0429   | n       | -            | [Partial consumption of noncopyable values](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0429-partial-consumption.md) |
| 0430   | y       | y            | [`sending` parameter and result values](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0430-transferring-parameters-and-results.md) |
| 0431   | y       | y            | [`@isolated(any)` Function Types](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0431-isolated-any-functions.md) |
| 0432   | y       | y            | [Borrowing and consuming pattern matching for noncopyable types](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0432-noncopyable-switch.md) |
| 0433   | n       | -            | [Synchronous Mutual Exclusion Lock 🔒](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0433-mutex.md) |
| 0434   | n       | -            | [Usability of global-actor-isolated types](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0434-global-actor-isolated-types-usability.md) |
| 0435   | n       | -            | [Swift Language Version Per Target](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0435-swiftpm-per-target-swift-language-version-setting.md) |
| 0436   | n       | -            | [Objective-C implementations in Swift](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0436-objc-implementation.md) |
| 0437   | n       | -            | [Noncopyable Standard Library Primitives](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0437-noncopyable-stdlib-primitives.md) |
| 0438   | n       | -            | [Metatype Keypaths](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0438-metatype-keypath.md) |
| 0439   | y       | y            | [Trailing comma lists](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0439-trailing-comma-lists.md) |
| 0440   | n       | -            | [Debug description macro](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0440-debug-description-macro.md) |
| 0441   | n       | -            | [Formalize language-mode terminology](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0441-formalize-language-mode-terminology.md) |
| 0442   | n       | -            | [Allow TaskGroup.ChildTaskResult type to be inferred](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0442-allow-taskgroup-childtaskresult-type-to-be-inferred.md) |
| 0443   | n       | -            | [Warning control flags](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0443-warning-control-flags.md) |
| 0444   | n       | -            | [Member import visibility](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0444-member-import-visibility.md) |
| 0445   | n       | -            | [String.Index printing](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0445-string-index-printing.md) |
| 0446   | n       | -            | [Nonescapable Types](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0446-non-escapable.md) |
| 0447   | n       | -            | [Span: Safe Access to Contiguous Storage](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0447-span-access-shared-contiguous-storage.md) |
| 0448   | n⁴      | -            | [Regex lookbehind assertions](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0448-regex-lookbehind-assertions.md) |
| 0449   | y       | n            | [Allow `nonisolated` to prevent global actor inference](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0449-nonisolated-for-global-actor-cutoff.md)
| 0450   | n       | -            | [Package traits](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0450-swiftpm-package-traits.md)
| 0451   | y       | y            | [Raw identifiers](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0451-escaped-identifiers.md)
| 0452   | y       | …            | [Integer Generic Parameters](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0452-integer-generic-parameters.md) |
| 0453   | n       | -            | [Vector](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0453-vector.md)
| 0454   | n       | -            | [Custom Allocator for Toolchain](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0454-memory-allocator.md)
| 0455   | n       | -            | [SwiftPM @testable build setting](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0455-swiftpm-testable-build-setting.md)
| 0456   | n       | -            | [Add `Span`-providing Properties to Standard Library Types](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0456-stdlib-span-properties.md)
| 0457   | n       | -            | [Expose attosecond representation of `Duration`](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0457-duration-attosecond-represenation.md)
| 0458   | y       | …            | [Opt-in Strict Memory Safety Checking](https://github.com/swiftlang/swift-evolution/blob/main/proposals/0458-strict-memory-safety.md)

⁺Returned for revision

¹The whitespace behavior specified by the proposal may not be perfectly implemented.

²Although the proposal text includes single-line strings, the acceptance excluded them: https://forums.swift.org/t/accepted-se-0182-string-newline-escaping/6355

³Highlighting of regex escapes is planned for the future.

⁴This will have an effect for intra-regex highlighting.
