This is a [Tree-sitter grammar](https://tree-sitter.github.io/tree-sitter/) for Apple's [Swift programming language](https://swift.org).

It has a particular focus on _helpful highlighting_. As one example, the force-unwrap operator, postfix `!`, is matched specially so it can be rendered to stand out from the surrounding code. Numeric literals, string escapes, type punctuation, and compiler expressions (like `#file`) are among the other syntax elements that are given special treatment. See below for examples.

The [evo-table.md](evo-table.md) file documents the grammar's support for Swift Evolution proposals, which support is nearly complete.

# Installation

The packages on [the project  Releases page](https://gitlab.com/woolsweater/tree-sitter-swifter/-/releases) contain the grammar libraries built for Mac Intel and Apple Silicon processors. You can put the appropriate dylib for your machine alongside the other grammar libraries you have installed.

If you would like to build from source, I recommend following the instructions given in the [tree-sitter-langs documentation](https://github.com/emacs-tree-sitter/tree-sitter-langs#building-grammars-from-source). The language grammars are git submodules, so you can simply swap the vended Swift grammar for this repo and compile it.

# Usage

This project is developed alongside an Emacs major mode for Swift, <https://codeberg.org/woolsweater/swift-ts-mode>. In Emacs, you may also use it with the emacs-tree-sitter project for syntax highlighting.

The [highlighting queries](queries/highlights.scm) are tuned to be useful while reading and editing Swift code. For full support, there are a few highlight categories [on top of the elisp-tree-sitter builtins](https://emacs-tree-sitter.github.io/syntax-highlighting/customization/):

- case-pattern
- comment.special
- operator.special
- punctuation.type
- annotation
- annotation.builtin
- annotation.type
- keyword.compiler
- keyword.type
- variable.synthesized

To take advantage of those in Emacs you will need `tree-sitter-hl-face` faces that match the query highlight names. This repo includes a file, 'tree-sitter-swift-query-faces.el', which you can load to define the matching faces. Once they are defined, you can customize them however you prefer, changing the colors, weights, and so on. Note that the parsing will work fine without these definitions; you will just miss out on some of the extra highlighting features.

## Highlighting

Here are some "highlights" 😉 of using this grammar. The screenshots below all use a Solarized Light theme; you can of course change the theme or individual colors and styles themselves to whatever you like.

**String interpolation**: notice how the delimiters stand out. Other string escape sequences, like `\n` and `\u{1f600}` will also be highlighted similarly. This applies to both raw and "normal" strings.

![Swift method go(to:)](screenshots/GoTo.png)

**Inout operator**, **subscript expressions**, **numeric literals**

![Swift method copyBytes](screenshots/CopyBytes.png)

**Attributes**, **force unwrap operator**

![Swift method with inline-always attribute](screenshots/InlineAlways.png)

**Type punctuation**: notice that in the typealiases the parens and function arrows are slightly different than the declaration text.

![Swift class declaration with typealiases](screenshots/StringCompiler.png)

Additionally, a small detail: in all the method signatures you can see that the _external_ parameter label is colored as part of the function declaration (blue, in this theme). And if there is an _internal_ name, that is given the default color, _but_ the colon is still highlighted as part of the external label.

The code above is from [my slox project](https://github.com/woolsweater/slox). Here's a useless piece of code that just shows off a whole bunch of highlight features at once. Notice particularly the highlighting of the **key path expression** and **implicit closure argument**.

![Useless code that shows off special queries](screenshots/Nonsense.png)

And here's a dense snippet from [`OrderedDictionary` in Apple's swift-collections](https://github.com/apple/swift-collections/blob/main/Sources/OrderedCollections/OrderedDictionary/OrderedDictionary.swift):

![Swift subscript declaration](screenshots/OrderedDictionary.png)

The grammar has basic support for regex literals; they are recognized and highlighted as strings. More extensive highlighting -- elements like character classes -- is a future direction.

# Background

I started developing this because I wanted to get good syntax highlighting for my day-to-day work in Emacs. The [swift-mode](https://github.com/swift-emacs/swift-mode) package provides reasonable highlighting, but as usual in Emacs it is based on regexes, which cannot really keep up with the complexity of Swift syntax. The [official Tree-sitter Swift grammar](https://github.com/tree-sitter/tree-sitter-swift) had minimal features and no recent activity; it was not usable. I started from scratch rather than building on that grammar mostly out of an interest in learning how to use Tree-sitter.

# Alternatives

As mentioned, the official Tree-sitter Swift grammar was moribund when I started this, and is now deprecated. There is another grammar linked from there, https://github.com/alex-pinkus/experimental-tree-sitter-swift, which is under active development.
