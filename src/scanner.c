#include <tree_sitter/parser.h>

#include <assert.h>
#include <string.h>
#include <wctype.h>
#include <stdio.h>

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused-parameter"

#define IN_RANGE(c, low, high) (((low) <= (c)) && ((c) <= (high)))
#define FALLTHROUGH

typedef struct
{
  uint32_t row;
  uint32_t column;
} TSPoint;

typedef struct
{
  uint32_t bytes;
  TSPoint extent;
} Length;

/**
 Internal wrapper for the `TSLexer`.

 This definition provides access to the current and marked end positions, which
 enables basic backtracking that's not possible via `TSLexer`.
 */
typedef struct
{
    TSLexer data;
    Length current_position;
    Length token_start_position;
    Length token_end_position;
} Lexer;

/**
 * Grammar elements that Tree-sitter will request validation for.
 */
enum TokenType
{
    AutomaticSemicolon,  // 0

    StringText,  // 1
    MultilineStringText,  // 2

    RawOnelineStringOpenDelim,  // 3
    RawMultilineOpenDelim,  // 4

    RawOnelineStringEscape,  // 5
    RawOnelineInterpolationOpener,  // 6
    RawOnelineStringText,  // 7

    RawMultilineStringEscape,  // 8
    RawMultilineInterpolationOpener,  // 9
    RawMultilineStringText,  // 10

    RawOnelineStringCloseDelim,  // 11
    RawMultilineStringCloseDelim,  // 12

    BlockCommentContents,  // 13

    BinaryOperator,  // 14
    OperatorName,  // 15
    PrefixOperator,  // 16
    PostfixOperator,  // 17

    // Zero-width rules
    ValidFunctionRefParams,  // 18
    VariablePropertyObserverBlock,  // 19
    GenericArgOpenDelim,  // 20
    MultipleTrailingClosures,  // 21
    ReturnValue,  // 22

    ErrorRecovery,  // 23
};

/**
 Outcomes when parsing a portion of a raw string whose interpretation depends
 on the octothorpe count.
 */
typedef enum
{
    /** The special element was scanned successfully. */
    RawStringElementResultMatch,
    /**
     The scanner found ordinary text, not a special element. (The octothorpe
     count did not match the string's.)
     */
    RawStringElementResultNoMatch,
    /**
     A special element was recognized but was malformed. (The octothorpe count
     matched but the remainder was invalid.)
     */
    RawStringElementResultError,
} RawStringElementResult;

/**
 Syntax elements the scanner will recognize in the process of figuring out
 the correct `TokenType`.
 */
typedef enum
{
    /** An opening parenthesis, '('. */
    ConcreteTokenOpenParen,

    /** An opening curly brace, '{'. */
    ConcreteTokenLeftCurlyBrace,

    /** A closing curly brace, '}'. */
    ConcreteTokenRightCurlyBrace,

    /** The opening delimiter of a line comment. */
    ConcreteTokenLineCommentDelimiter,

    /** The opening delimiter of a block comment. */
    ConcreteTokenBlockCommentDelimiter,

    /** The opening delimiter of a regex literal. */
    ConcreteTokenRegexLiteralDelimiter,

    /**
     A single '?' followed by whitespace. (A '?' followed by other operator
     characters will be parsed as `OPERATOR`.)
     */
    ConcreteTokenQuestionMark,

    /** A single '<' followed by any non-operator character. */
    ConcreteTokenLeftAngleBracket,

    /**
     A left angle bracket followed immediately by a right angle bracket. In a
     type context this is an empty generic argument list (for a variadic type);
     otherwise it is an operator.
     */
    ConcreteTokenAngleBracketLeftRight,

    /**
     A sequence of operator characters that could be a binary or prefix
     operator. This excludes certain operator-like tokens that are part of
     other rules (see `is_special_operator`).
     */
    ConcreteTokenOperator,

    /**
     A built-in operator that is recognized specifically in the grammar rather
     than as a general binary or prefix operator.
     */
    ConcreteTokenSpecialOperator,

    /**
     An identifier suffixed with a colon that is labelling a closure expression.
     */
    ConcreteTokenClosureLabel,

    /**
     A keyword that starts a statement or other element that cannot be an
     expression.
     */
    ConcreteTokenStatementKeyword,

    /**
     Any construct that does not fall into one of the above buckets. The
     exact value is not significant for determining `TokenType`.
     */
    ConcreteTokenUncategorized,
} ConcreteToken;

/**
 A count of octothorpe characters ('#') in a raw string context.

 - note: Swift currently only accepts 255 '#' characters in a raw string
 delimiter. https://github.com/apple/swift/issues/60869
 */
typedef uint16_t othrp_n;
#define SCANNER_MAX_NESTING 4
#define SCANNER_STACK_TOP (SCANNER_MAX_NESTING - 1)

/** State of the external scanner. */
typedef struct
{
    /**
     A stack of the number of octothorpes that delimit the current raw string
     literals, most recent last. 0 represents an empty slot.

     The functions `scanner_push_raw_string`, `scanner_pop_raw_string`, and
     `scanner_current_othrp_count` should be used to access the stack.
     */
    othrp_n octothorpe_count[SCANNER_MAX_NESTING];
} Scanner;

/**
 Add `count` to the top of the scanner's `octothorpe_count` stack. Return false
 and do not change the scanner state if this would exceed the maximum supported
 level of nesting.
 */
static bool scanner_push_raw_string(Scanner * scanner, othrp_n count)
{
    for (int i = 0; i <= SCANNER_STACK_TOP; i++) {
        if (0 == scanner->octothorpe_count[i]) {
            scanner->octothorpe_count[i] = count;
            return true;
        }
    }

    // Max nesting exceeded
    return false;
}

/**
 Remove and return the top element of the scanner's `octothorpe_count` stack.
 If there are no elements in the stack, return 0.
 */
static othrp_n scanner_pop_raw_string(Scanner * scanner)
{
    for (int i = SCANNER_STACK_TOP; i >= 0; i--) {
        const othrp_n count = scanner->octothorpe_count[i];
        if (count > 0) {
            scanner->octothorpe_count[i] = 0;
            return count;
        }
    }

    return 0;
}

/**
 Return the top element of the scanner's `octothorpe_count` stack; this will
 be 0 if the stack is empty.
 */
static othrp_n scanner_current_othrp_count(Scanner * scanner)
{
    for (int i = SCANNER_STACK_TOP; i >= 0; i--) {
        if (scanner->octothorpe_count[i] > 0) {
            return scanner->octothorpe_count[i];
        }
    }

    return 0;
}

//MARK:- Helpers

static void advance(TSLexer * lexer)
{
    lexer->advance(lexer, false);
}

static void skip(TSLexer * lexer)
{
    lexer->advance(lexer, true);
}

static void mark_end(TSLexer * lexer)
{
    lexer->mark_end(lexer);
}

static bool is_eof(TSLexer * lexer)
{
    return lexer->eof(lexer);
}

/** Check whether `lookahead` matches the given character. */
static bool match(TSLexer * lexer, int32_t c)
{
    return c == lexer->lookahead;
}

/**
 If `lookahead` matches the given character, then consume it and return `true`;
 else do not advance and return `false`.
 */
static bool scan_char(TSLexer * lexer, int32_t c)
{
    if (c != lexer->lookahead) {
        return false;
    }

    advance(lexer);
    return true;
}

/**
 Step over all contiguous whitespace starting at the current position. Return
 `false` if no characters were scanned. If `multiline` is not `NULL` it will be
 set `true` if a '\n' is anywhere in the scanned characters.
 */
static bool scan_whitespace(TSLexer * lexer, bool * multiline)
{
    bool did_scan = false;
    while (iswspace(lexer->lookahead)) {
        if (match(lexer, '\n') && multiline) {
            *multiline = true;
        }
        did_scan = true;
        skip(lexer);
    }
    return did_scan;
}

static bool is_identifier_head(int32_t c)
{
    return ('_' == c)
        || iswalpha(c)
        || (0x00a8 == c)
        || (0x00aa == c)
        || (0x00ad == c)
        || (0x00af == c)
        || IN_RANGE(c, 0x00b7, 0x00ba)
        || IN_RANGE(c, 0x00b2, 0x00b5)
        || IN_RANGE(c, 0x00bc, 0x00be)
        || IN_RANGE(c, 0x00c0, 0x00d6)
        || IN_RANGE(c, 0x00d8, 0x00f6)
        || IN_RANGE(c, 0x00f8, 0x00ff)
        || IN_RANGE(c, 0x0100, 0x02ff)
        || IN_RANGE(c, 0x0370, 0x167f)
        || IN_RANGE(c, 0x1681, 0x180d)
        || IN_RANGE(c, 0x180f, 0x1dbf)
        || IN_RANGE(c, 0x1e00, 0x1fff)
        || IN_RANGE(c, 0x200b, 0x200d)
        || (0x2054 == c)
        || IN_RANGE(c, 0x202a, 0x202e)
        || IN_RANGE(c, 0x203f, 0x2040)
        || IN_RANGE(c, 0x2060, 0x206f)
        || IN_RANGE(c, 0x2070, 0x20cf)
        || IN_RANGE(c, 0x2100, 0x218f)
        || IN_RANGE(c, 0x2460, 0x24ff)
        || IN_RANGE(c, 0x2776, 0x2793)
        || IN_RANGE(c, 0x2c00, 0x2dff)
        || IN_RANGE(c, 0x2e80, 0x2fff)
        || IN_RANGE(c, 0x3004, 0x3007)
        || IN_RANGE(c, 0x3021, 0x302f)
        || IN_RANGE(c, 0x3031, 0x303f)
        || IN_RANGE(c, 0x3040, 0xd7ff)
        || IN_RANGE(c, 0xf900, 0xfd3d)
        || IN_RANGE(c, 0xfd40, 0xfdcf)
        || IN_RANGE(c, 0xfdf0, 0xfe1f)
        || IN_RANGE(c, 0xfe30, 0xfe44)
        || IN_RANGE(c, 0xfe47, 0xfffd)
        || IN_RANGE(c, 0x10000, 0x1fffd)
        || IN_RANGE(c, 0x20000, 0x2fffd)
        || IN_RANGE(c, 0x30000, 0x3fffd)
        || IN_RANGE(c, 0x40000, 0x4fffd)
        || IN_RANGE(c, 0x50000, 0x5fffd)
        || IN_RANGE(c, 0x60000, 0x6fffd)
        || IN_RANGE(c, 0x70000, 0x7fffd)
        || IN_RANGE(c, 0x80000, 0x8fffd)
        || IN_RANGE(c, 0x90000, 0x9fffd)
        || IN_RANGE(c, 0xa0000, 0xafffd)
        || IN_RANGE(c, 0xb0000, 0xbfffd)
        || IN_RANGE(c, 0xc0000, 0xcfffd)
        || IN_RANGE(c, 0xd0000, 0xdfffd)
        || IN_RANGE(c, 0xe0000, 0xefffd);
}

static bool is_identifier_char(int32_t c)
{
    return is_identifier_head(c)
        || isdigit(c)
        || IN_RANGE(c, 0x0300, 0x036f)
        || IN_RANGE(c, 0x1dc0, 0x1dff)
        || IN_RANGE(c, 0x20d0, 0x20ff)
        || IN_RANGE(c, 0xfe20, 0xfe2f);
}

/**
 Test whether the character is legal as the first character of an operator.
 */
static bool is_operator_head(int32_t c)
{
    switch (c) {
        case '.':
        case '/':
        case '=':
        case '-':
        case '+':
        case '!':
        case '*':
        case '%':
        case '<':
        case '>':
        case '&':
        case '|':
        case '^':
        case '?':
        case '~':
        case 0x00a9:
        case 0x00ab:
        case 0x00ac:
        case 0x00ae:
        case 0x00b0:
        case 0x00b1:
        case 0x00b6:
        case 0x00bb:
        case 0x00bf:
        case 0x00d7:
        case 0x00f7:
        case 0x2016:
        case 0x2017:
        case 0x3001:
        case 0x3002:
        case 0x3003:
        case 0x3030:
            return true;
        default:
            return IN_RANGE(c, 0x00a1, 0x00a7) ||
                IN_RANGE(c, 0x2020, 0x2027) ||
                IN_RANGE(c, 0x2030, 0x203e) ||
                IN_RANGE(c, 0x2041, 0x2053) ||
                IN_RANGE(c, 0x2055, 0x205e) ||
                IN_RANGE(c, 0x2190, 0x23ff) ||
                IN_RANGE(c, 0x2500, 0x2775) ||
                IN_RANGE(c, 0x2794, 0x2bff) ||
                IN_RANGE(c, 0x2e00, 0x2e7f) ||
                IN_RANGE(c, 0x3008, 0x3020);
    }
}

/**
 Test whether the character is valid within an operator. Only operators that
 begin with a dot may contain more dots.
 */
static bool is_operator_character(int32_t c, bool is_dot_allowed)
{
    if ('.' == c) {
        return is_dot_allowed;
    }

    return IN_RANGE(c, 0x0300, 0x036f) ||
        IN_RANGE(c, 0x1dc0, 0x1dff) ||
        IN_RANGE(c, 0x20d0, 0x20ff) ||
        IN_RANGE(c, 0xfe00, 0xfe0f) ||
        IN_RANGE(c, 0xfe20, 0xfe2f) ||
        IN_RANGE(c, 0xe0100, 0xe01ef) ||
        is_operator_head(c);
}

//MARK:- Automatic semicolon

/**
 Given a lexer just after a slash which may open a comment, try to scan a
 line comment delimiter. Return true if one is recognized.
 */
static bool finish_line_comment_delim(TSLexer * lexer)
{
    if (!scan_char(lexer, '/')) {
        return false;
    }

    // Delimiter may be two or three slashes
    scan_char(lexer, '/');
    return true;
}

/**
 Given a lexer just after a slash which may open a comment, try to scan a
 block comment opening delimiter. Return true if one is recognized.
 */
static bool finish_block_comment_open(TSLexer * lexer)
{
    if (!scan_char(lexer, '*')) {
        return false;
    }

    // Delimiter may have one or two asterisks
    scan_char(lexer, '*');
    return true;
}

/** Skip until the end of the line, stopping on the final newline. */
static void skip_line_comment(TSLexer * lexer)
{
    while (!match(lexer, '\n') && !is_eof(lexer)) {
        skip(lexer);
    }
}

/**
 Given a lexer just after the opening '*' of a block comment, skip past the end
 of the entire comment (including any nested comments). Return false if the
 delimiters are unbalanced.

 If `multiline` is not `NULL` it will be set `true` if there was a '\n' anywhere
 within the comment.
 */
static bool skip_block_comment(TSLexer * lexer, bool * multiline)
{
    int nesting = 1;
    while (!is_eof(lexer)) {
        switch (lexer->lookahead) {
            case '/':
                skip(lexer);
                if (scan_char(lexer, '*')) {
                    nesting++;
                }
                break;

            case '*':
                skip(lexer);
                if (scan_char(lexer, '/')) {
                    nesting--;
                }
                break;

            case '\n':
                if (multiline) {
                    *multiline = true;
                }
                FALLTHROUGH;

            default:
                skip(lexer);
        }

        if (nesting == 0) {
            return true;
        }
    }

    return false;
}

/**
 Skip over any number of comments -- line or block -- that are either adjacent
 to each other or separated only by whitespace.
 */
static void skip_comments(TSLexer * lexer)
{
    while (!is_eof(lexer)) {
        scan_whitespace(lexer, NULL);
        if (!scan_char(lexer, '/')) {
            return;
        }

        if (finish_line_comment_delim(lexer)) {
            skip_line_comment(lexer);
        }
        else if (finish_block_comment_open(lexer)) {
            skip_block_comment(lexer, NULL);
        }
        else {
            return;
        }
    }
}

/** Look for 'is' or one of the 'as' tokens at the current position. */
static bool scan_cast_operator(TSLexer * lexer)
{
    const int32_t first_char = lexer->lookahead;
    skip(lexer);
    switch (first_char) {
        case 'i':
            if (!scan_char(lexer, 's')) {
                return false;
            }

            return iswspace(lexer->lookahead);

        case 'a':
            if (!scan_char(lexer, 's')) {
                return false;
            }

            return match(lexer, '!')
                || match(lexer, '?')
                || iswspace(lexer->lookahead);

        default:
            return false;
    }
}

#define TRY_SCAN_TAIL(tail) \
    for (char * chars = tail; *chars != '\0'; chars++) { \
        if (!scan_char(lexer, *chars)) { \
            return false; \
        } \
    } \
    return !is_identifier_char(lexer->lookahead);

/**
 Given a lexer on an identifier character, try to recognize a keyword that
 starts a statement or other element that cannot be an expression.
 */
static bool scan_nonexpression_keyword(TSLexer * lexer)
{
    const int32_t first_char = lexer->lookahead;
    skip(lexer);
    switch (first_char) {
        case 'a':
            TRY_SCAN_TAIL("ctor");

        case 'b':
            TRY_SCAN_TAIL("reak");

        case 'c':
            if (scan_char(lexer, 'a')) {
                TRY_SCAN_TAIL("se");
            }
            else if (scan_char(lexer, 'l')) {
                TRY_SCAN_TAIL("ass");
            }
            else if (scan_char(lexer, 'o') && scan_char(lexer, 'n')) {
                if (scan_char(lexer, 't')) {
                    TRY_SCAN_TAIL("inue");
                }
                else {
                    TRY_SCAN_TAIL("venience");
                }
            }
            else {
                return false;
            }

        case 'd':
            if (scan_char(lexer, 'e')) {
                if (scan_char(lexer, 'f')) {
                    TRY_SCAN_TAIL("ault");
                }
                else {
                    TRY_SCAN_TAIL("init");
                }
            }
            else if (scan_char(lexer, 'i') && scan_char(lexer, 's')) {
                if (scan_char(lexer, 'c')) {
                    TRY_SCAN_TAIL("ard");
                }
                else {
                    TRY_SCAN_TAIL("tributed");
                }
            }
            else {
                TRY_SCAN_TAIL("ynamic");
            }

        case 'e':
            if (scan_char(lexer, 'n')) {
                TRY_SCAN_TAIL("um");
            }
            else {
                TRY_SCAN_TAIL("xtension");
            }

        case 'f':
            if (scan_char(lexer, 'a')) {
                TRY_SCAN_TAIL("llthrough");
            }
            else if (scan_char(lexer, 'o')) {
                TRY_SCAN_TAIL("r");
            }
            else if (scan_char(lexer, 'i')) {
                if (scan_char(lexer, 'n')) {
                    TRY_SCAN_TAIL("nal");
                }
                else {
                    TRY_SCAN_TAIL("leprivate");
                }
            }
            else {
                TRY_SCAN_TAIL("unc");
            }

        case 'g':
            TRY_SCAN_TAIL("uard");

        case 'i':
            if (scan_char(lexer, 'm')) {
                TRY_SCAN_TAIL("port");
            }
            else if (scan_char(lexer, 'n')) {
                if (scan_char(lexer, 'i')) {
                    TRY_SCAN_TAIL("t");
                }
                else if (scan_char(lexer, 'f')) {
                    TRY_SCAN_TAIL("ix");
                }
                else {
                    TRY_SCAN_TAIL("ternal");
                }
            }
            else {
                return false;
            }

        case 'l':
            if (scan_char(lexer, 'a')) {
                TRY_SCAN_TAIL("zy");
            }
            else {
                TRY_SCAN_TAIL("et");
            }

        case 'm':
            if (scan_char(lexer, 'a')) {
                TRY_SCAN_TAIL("cro");
            }
            else {
                TRY_SCAN_TAIL("utating");
            }

        case 'n':
            TRY_SCAN_TAIL("onisolated");

        case 'o':
            if (scan_char(lexer, 'p') && scan_char(lexer, 'e')) {
                if (scan_char(lexer, 'r')) {
                    TRY_SCAN_TAIL("ator");
                }
                else {
                    TRY_SCAN_TAIL("n");
                }
            }
            else {
                TRY_SCAN_TAIL("verride");
            }

        case 'p':
            if (scan_char(lexer, 'a')) {
                TRY_SCAN_TAIL("ckage");
            }
            else if (scan_char(lexer, 'o')) {
                TRY_SCAN_TAIL("stfix");
            }
            else if (scan_char(lexer, 'r')) {
                if (scan_char(lexer, 'e')) {
                    if (scan_char(lexer, 'f')) {
                        TRY_SCAN_TAIL("ix");
                    }
                    else {
                        TRY_SCAN_TAIL("cedencegroup");
                    }
                }
                else if (scan_char(lexer, 'i')) {
                    TRY_SCAN_TAIL("vate");
                }
                else {
                    TRY_SCAN_TAIL("otocol");
                }
            }
            else {
                TRY_SCAN_TAIL("ublic");
            }

        case 'r':
            if (!scan_char(lexer, 'e')) {
                return false;
            }
            if (scan_char(lexer, 'p')) {
                TRY_SCAN_TAIL("eat");
            }
            else if (scan_char(lexer, 'q')) {
                TRY_SCAN_TAIL("uired");
            }
            else {
                TRY_SCAN_TAIL("turn");
            }

        case 's':
            if (scan_char(lexer, 't')) {
                if (scan_char(lexer, 'a')) {
                    TRY_SCAN_TAIL("tic");
                }
                else {
                    TRY_SCAN_TAIL("ruct");
                }
            }
            else {
                TRY_SCAN_TAIL("ubscript");
            }

        case 't':
            if (scan_char(lexer, 'h')) {
                TRY_SCAN_TAIL("row");
            }
            else {
                TRY_SCAN_TAIL("ypealias");
            }

        case 'v':
            TRY_SCAN_TAIL("ar");

        case 'w':
            TRY_SCAN_TAIL("hile");
    }

    return false;
}

/**
 Given a lexer on an identifier character, determine whether it begins a label
 (an identifier + colon) followed by an opening curly brace.
 */
static bool lookahead_labelled_closure(TSLexer * lexer)
{
    while (is_identifier_char(lexer->lookahead)) {
        skip(lexer);
    }

    if (!scan_char(lexer, ':')) {
        return false;
    }

    bool multiline = false;
    scan_whitespace(lexer, &multiline);
    if (multiline) {
        return false;
    }

    return match(lexer, '{');
}

/**
 Given a lexer just after an opening curly brace, look ahead for a property
 observer keyword: either "didSet" or "willSet".
 */
static bool lookahead_property_observer(TSLexer * lexer)
{
    scan_whitespace(lexer, NULL);
    if (scan_char(lexer, 'd')) {
        TRY_SCAN_TAIL("idSet");
    }
    else if (scan_char(lexer, 'w')) {
        TRY_SCAN_TAIL("illSet");
    }
    else {
        return false;
    }
}

/**
 Determine whether an automatic semicolon can be inserted at the current
 position by skipping ahead over comments and whitespace to the next significant
 character.

 This returns false if a literal semicolon is found.
 */
static bool scan_automatic_semicolon(TSLexer * lexer, ConcreteToken lead_token)
{
    if (ConcreteTokenLineCommentDelimiter == lead_token) {
        skip_line_comment(lexer);
    }
    else if (ConcreteTokenBlockCommentDelimiter == lead_token) {
        skip_block_comment(lexer, NULL);
    }

    skip_comments(lexer);
    scan_whitespace(lexer, NULL);

    const int32_t following_char = lexer->lookahead;

    if (is_eof(lexer) || following_char == '}') {
        return true;
    }
    else if (following_char == '{') {
        return false;
    }
    else if (following_char == ';') {
        // Explicit semicolon will be consumed by the JS generated parser so it
        // can be highlighted.
        return false;
    }

    if (is_identifier_char(following_char)) {
        return !scan_cast_operator(lexer);
    }
    else if (is_operator_head(following_char)) {
        const bool is_dot_operator = (following_char == '.');
        int operator_length = 0;
        while (is_operator_character(lexer->lookahead, is_dot_operator)) {
            operator_length++;
            skip(lexer);
        }

        // A prefix operator which is _not_ a member access on the following
        // line allows a semicolon. A binary operator does not. Swift makes the
        // distinction between prefix and binary based on whitespace between the
        // operator and the next token.
        const bool is_member_access = is_dot_operator && (1 == operator_length);
        const bool is_prefix = !iswspace(lexer->lookahead);
        return !(is_member_access) && is_prefix;
    }

    return true;
}

//MARK:- String scanning

static bool scan_multiline_string_terminator(TSLexer * lexer)
{
    advance(lexer);
    if (!scan_char(lexer, '"')) {
        return false;
    }

    return scan_char(lexer, '"');
}

static bool scan_multiline_string_text(TSLexer * lexer)
{
    bool accepted = false;
    while (true) {
        switch (lexer->lookahead) {
            case '\0':
                return false;
            case '\\':
                mark_end(lexer);
                advance(lexer);
                if (match(lexer, '\n')) {
                    goto accept_and_continue;
                }
                else {
                    // Return to TS for escape parsing
                    goto finish;
                }
            case '"':
                mark_end(lexer);
                if (scan_multiline_string_terminator(lexer)) {
                    // Leave the quotes to be consumed by TS
                    goto finish;
                }
                else {
                    goto accept_and_continue;
                }
            default:
                advance(lexer);
            accept_and_continue:
                mark_end(lexer);
                accepted = true;
        }
    }

finish:
    lexer->result_symbol = MultilineStringText;
    return accepted;
}

static bool scan_string_text(TSLexer * lexer)
{
    bool accepted = false;
    while (!is_eof(lexer)) {
        switch (lexer->lookahead) {
            case '\\': FALLTHROUGH;
            case '"':
                lexer->result_symbol = StringText;
                return accepted;
            default:
                advance(lexer);
                accepted = true;
        }
    }

    return false;
}

//MARK:- Raw strings

static othrp_n count_octothorpes(TSLexer * lexer)
{
    othrp_n count = 0;
    while (scan_char(lexer, '#')) {
        count++;
    }

    return count;
}

/**
 Given a lexer just after a '#' character, determine whether there is a valid
 raw string opening delimiter (either single- or multi-line) here. If so, store
 the count of octothorpes in `scanner`.
 */
static bool scan_raw_string_open_delim(TSLexer * lexer,
                                       Scanner * scanner,
                                       const bool * looking_for)
{
    // This function is only entered after the first '#'.
    const othrp_n octothorpe_count = 1 + count_octothorpes(lexer);

    int quote_count = 0;
    while (scan_char(lexer, '"') && quote_count < 4) {
        quote_count++;
        if (quote_count == 1) {
            // Leave following quotes in case they are part of other tokens (see
            // below).
            mark_end(lexer);
        }
    }

    switch (quote_count) {
        case 0:
            return false;

        case 1:
            if (!looking_for[RawOnelineStringOpenDelim]) {
                return false;
            }
            lexer->result_symbol = RawOnelineStringOpenDelim;
            return scanner_push_raw_string(scanner, octothorpe_count);

        case 2:
            if (!(match(lexer, '#') && looking_for[RawOnelineStringOpenDelim])) {
                return false;
            }
            // Empty string like `##""##`. Do *not* mark end so we leave the
            // second quote for the closing delimiter.
            lexer->result_symbol = RawOnelineStringOpenDelim;
            return scanner_push_raw_string(scanner, octothorpe_count);

        case 3:
            if (match(lexer, '\n') && looking_for[RawMultilineOpenDelim]) {
                advance(lexer);
                mark_end(lexer);
                lexer->result_symbol = RawMultilineOpenDelim;
                return scanner_push_raw_string(scanner, octothorpe_count);
            }
            FALLTHROUGH;

        default:
            if (!looking_for[RawOnelineStringOpenDelim]) {
                return false;
            }
            // String starting with quotes as part of its text. Do *not* mark
            // end so the contents can still be scanned.
            lexer->result_symbol = RawOnelineStringOpenDelim;
            return scanner_push_raw_string(scanner, octothorpe_count);
    }
}

static bool in_raw_string(const bool * looking_for)
{
    return looking_for[RawOnelineStringText]
        || looking_for[RawOnelineStringEscape]
        || looking_for[RawOnelineInterpolationOpener]
        || looking_for[RawOnelineStringCloseDelim];
}

static bool in_raw_multiline_string(const bool * looking_for)
{
    return looking_for[RawMultilineStringText]
        || looking_for[RawMultilineStringEscape]
        || looking_for[RawMultilineInterpolationOpener]
        || looking_for[RawMultilineStringCloseDelim];
}

/**
 Given a lexer just after the 'u' of a Unicode string escape, try to scan the
 brackets and digits. Return `true` if a valid escape body is found.
 */
static bool finish_unicode_escape(TSLexer * lexer, enum TokenType result_symbol)
{
    if (!scan_char(lexer, '{')) {
        return false;
    }

    int digit_count = 0;
    while (!is_eof(lexer) && digit_count < 8 && isxdigit(lexer->lookahead)) {
        digit_count++;
        advance(lexer);
    }

    if (digit_count == 0 || !scan_char(lexer, '}')) {
        return false;
    }

    mark_end(lexer);
    lexer->result_symbol = result_symbol;
    return true;
}

/**
 Given a lexer on a '\' in a raw string, count any trailing octothorpes to
 determine whether this is an actual escaped element or plain text. Then
 validate the remainder of the escape.

 If a valid escape is found, set `lexer`'s `result_symbol` and return
 `RawStringElementResultMatch`. If the octothorpe count does not match (meaning
 that this is not an escape), return `RawStringElementResultNoMatch`. If the
 octothorpe count matches but the remainder is not a valid escape, return
 `RawStringElementResultError`.
 */
static RawStringElementResult scan_raw_string_escape(TSLexer * lexer,
                                                     othrp_n octothorpe_count,
                                                     bool is_multiline)
{
    const enum TokenType escape_symbol =
        is_multiline ? RawMultilineStringEscape : RawOnelineStringEscape;

    if (!scan_char(lexer, '\\')) {
        return RawStringElementResultError;
    }

    const othrp_n trailing_octothorpe_count = count_octothorpes(lexer);

    if (trailing_octothorpe_count != octothorpe_count) {
        return RawStringElementResultNoMatch;
    }

    if (scan_char(lexer, 'u')) {
        if (finish_unicode_escape(lexer, escape_symbol)) {
            return RawStringElementResultMatch;
        }
        else {
            return RawStringElementResultError;
        }
    }

    switch (lexer->lookahead) {
        case '(':
            advance(lexer);
            mark_end(lexer);
            lexer->result_symbol = is_multiline
                ? RawMultilineInterpolationOpener
                : RawOnelineInterpolationOpener;
            return RawStringElementResultMatch;

        case '0': FALLTHROUGH;
        case '\\': FALLTHROUGH;
        case 't': FALLTHROUGH;
        case 'n': FALLTHROUGH;
        case 'r': FALLTHROUGH;
        case '"': FALLTHROUGH;
        case '\'': FALLTHROUGH;
            advance(lexer);
            mark_end(lexer);
            lexer->result_symbol = escape_symbol;
            return RawStringElementResultMatch;

        default:
            return RawStringElementResultError;
    }
}

/**
 Given a lexer on a '"' in a raw string (single- or multi-line), check whether
 it is a closing delimiter by counting any trailing octothorpes.

 If a closing delimiter is found, pop an element from `scanner`'s
 `octothorpe_count` stack, to 0, set `lexer`'s `result_symbol`, and return
 `RawStringElementResultMatch`. Otherwise do not touch those fields and return
 `RawStringElementResultNoMatch`.
 */
static bool scan_raw_string_close_delim(TSLexer * lexer,
                                        Scanner * scanner,
                                        bool is_multiline)
{
    if (is_multiline) {
        if (!scan_multiline_string_terminator(lexer)) {
            return RawStringElementResultNoMatch;
        }
    }
    else if (!scan_char(lexer, '"')) {
        return RawStringElementResultError;
    }

    const othrp_n trailing_octothorpe_count = count_octothorpes(lexer);

    if (trailing_octothorpe_count != scanner_current_othrp_count(scanner)) {
        return RawStringElementResultNoMatch;
    }

    scanner_pop_raw_string(scanner);
    mark_end(lexer);
    lexer->result_symbol = is_multiline
        ? RawMultilineStringCloseDelim
        : RawOnelineStringCloseDelim;
    return RawStringElementResultMatch;
}

/**
 Process characters in a raw string (either single- or multi-line) to find
 plain text, escapes, or the closing delimiter.

 - note: Text tokens will be broken up at the characters that mark special
 elements ('\' and '"') because multiple tokens cannot be returned at once. When
 a potential escape or close delimiter is found it must be left to be validated
 on its own scanner pass.
 */
static bool continue_raw_string(TSLexer * lexer,
                                Scanner * scanner,
                                bool is_multiline)
{
    bool have_text = false;
    while (!is_eof(lexer)) {
        if (match(lexer, '\\') || match(lexer, '"')) {
            // Since we can't return multiple tokens, mark everything up to this
            // point as text and wait for the next invocation to evaluate
            // whether this is an escape or close delimiter.
            if (have_text) {
                mark_end(lexer);
                lexer->result_symbol = is_multiline
                    ? RawMultilineStringText
                    : RawOnelineStringText;
                return true;
            }

            RawStringElementResult result;
            if (match(lexer, '"')) {
                result = scan_raw_string_close_delim(lexer,
                                                     scanner,
                                                     is_multiline);
            }
            else {
                const othrp_n count = scanner_current_othrp_count(scanner);
                result = scan_raw_string_escape(lexer, count, is_multiline);
            }

            switch (result) {
                case RawStringElementResultMatch:
                    return true;

                case RawStringElementResultNoMatch:
                    have_text = true;
                    mark_end(lexer);
                    continue;

                case RawStringElementResultError:
                    return false;

                default:
                    assert(false && "Unknown raw string parse result");
                    return false;
            }
        }
        else if (match(lexer, '\n') && !is_multiline) {
            return false;
        }
        else {
            have_text = true;
            advance(lexer);
            continue;
        }
    }

    return false;
}

//MARK:- Comment scanning

static bool scan_block_comment_contents(TSLexer * lexer)
{
    if (skip_block_comment(lexer, NULL)) {
        lexer->result_symbol = BlockCommentContents;
        mark_end(lexer);
        return true;
    }
    else {
        return false;
    }
}

//MARK:- Function references

static bool scan_identifier(TSLexer * lexer)
{
    if (!is_identifier_head(lexer->lookahead)) {
        return false;
    }

    advance(lexer);

    while (is_identifier_char(lexer->lookahead)) {
        advance(lexer);
    }

    return true;
}

/**
 Scan over a sequence of balanced generic argument brackets.

 - note: This does not completely validate the arguments grammatically: it
 checks for basic failure cases like unbalanced parens and square brackets;
 numeric literals; and EOF/completely illegal characters. Other errors will be
 caught by the generated parser.
 */
static bool scan_generic_arg_brackets(TSLexer * lexer)
{
    int paren_count = 0;
    int square_bracket_count = 0;
    while (!scan_char(lexer, '>')) {
        scan_whitespace(lexer, NULL);

        if (is_eof(lexer)) {
            return false;
        }

        switch (lexer->lookahead) {
            case '~':
            case '#':
            case '$':
            case '%':
            case '^':
            case '*':
            case '+':
            case '{':
            case '}':
            case '|':
            case '"':
            case ';':
            case '/':
            case '=':
                return false;

            case '(':
                paren_count++;
                break;

            case ')':
                if (paren_count <= 0) {
                    return false;
                }
                paren_count--;
                break;

            case '[':
                square_bracket_count++;
                break;

            case ']':
                if (square_bracket_count <= 0) {
                    return false;
                }
                square_bracket_count--;
                break;

            case '-':
                advance(lexer);
                if (!scan_char(lexer, '>')) {
                    return false;
                }
                break;

            case '>':
                // Closing delim found after some whitespace
                return true;

            default:
                break;
        }

        if (is_identifier_char(lexer->lookahead)) {
            if (!scan_identifier(lexer)) {
                return false;
            }
            else {
                continue;
            }
        }

        if (scan_char(lexer, '<')) {
            if (!scan_generic_arg_brackets(lexer)) {
                return false;
            }
        }
        else {
            advance(lexer);
        }
    }

    return true;
}

/**
 Given a lexer just after an open paren, look for a valid function reference
 parameter list, *without* consuming it (so that subnodes can still be captured
 by the main parser).
 */
static bool scan_function_ref_params(TSLexer * lexer)
{
    scan_whitespace(lexer, NULL);
    // Scan labels (identifiers with colons)
    bool at_least_one_label = false;
    while (scan_identifier(lexer)) {
        scan_whitespace(lexer, NULL);
        if (!scan_char(lexer, ':')) {
            return false;
        }

        scan_whitespace(lexer, NULL);
        at_least_one_label = true;
    }

    return at_least_one_label && scan_char(lexer, ')');
}

//MARK:- Binary operators

/**
 Given a lexer that has just scanned the first character of a dot operator,
 determine whether it is either a single dot or one of the built-in range
 operators.
 */
static bool is_builtin_dot_operator(TSLexer * lexer)
{
    if (scan_char(lexer, '.')) {
        // Range operator
        return scan_char(lexer, '.') || scan_char(lexer, '<');
    }
    else {
        // Single dot (member access)
        return !is_operator_character(lexer->lookahead, true);
    }
}

/**
 Starting after `head`, look for certain built-in operator-like tokens that
 cannot actually be used in operator positions.
 */
static bool is_reserved_operator_lookalike(TSLexer * lexer, int32_t head)
{
    switch (head) {
        // Assignment operator; single '='
        case '=':
            return !is_operator_character(lexer->lookahead, false);
        // Function return punctuation '->'
        case '-':
            if (scan_char(lexer, '>')) {
                return !is_operator_character(lexer->lookahead, false);
            }
            return false;
        // Comment delimiters
        case '/':
            return match(lexer, '/') || match(lexer, '*');
        case '*':
            return match(lexer, '/');
        default:
            return false;
    }
}

/**
 Starting after `head`, check whether the apparent operator token is either not
 truly a valid operator or is one that belongs to another rule (such as simple
 member access).
 */
static bool is_special_operator(TSLexer * lexer, int32_t head)
{
    if ('.' == head) {
        return is_builtin_dot_operator(lexer);
    }
    else {
        return is_reserved_operator_lookalike(lexer, head);
    }
}

/**
 Given a lexer that has just scanned `head`, scan over a contiguous sequence of
 operator characters for one of the 'OPERATOR' rules. Returns `false` for an
 operator that belongs to some other rule in the generated parser (e.g. a range
 operator; see `is_special_operator`).
 */
static bool scan_operator(TSLexer * lexer, int32_t head)
{
    if (is_special_operator(lexer, head)) {
        return false;
    }

    const bool is_dot_operator = ('.' == head);
    while (is_operator_character(lexer->lookahead, is_dot_operator)) {
        advance(lexer);
    }

    // Do *not* mark the end here; whitespace still needs to be checked.
    return true;
}

/**
 With the lexer just after a forward slash, look ahead to see whether there is a
 single-line regex literal. No characters are consumed in any case.
 */
static bool lookahead_regex_literal(TSLexer * lexer)
{
    if (match(lexer, ' ') || match(lexer, '\t') || match(lexer, '\n')) {
        return false;
    }

    int32_t previous = '/';
    int paren_count = 0;
    while (!is_eof(lexer)) {
        switch (lexer->lookahead) {
            case '\n':
                return false;

            case '(':
                paren_count++;
                break;

            case ')':
                if (0 == paren_count) {
                    return false;
                }
                paren_count--;
                break;

            case '/':
                if ('\\' == previous) {
                    break;
                }
                else if (' ' == previous || '\t' == previous) {
                    return false;
                }
                else {
                    return (paren_count == 0);
                }

            default:
                break;
        }

        previous = lexer->lookahead;
        advance(lexer);
    }

    return false;
}

static ConcreteToken finish_left_angle_token(TSLexer * lexer,
                                             bool * has_trailing_whitespace)
{
    ConcreteToken token = ConcreteTokenUncategorized;
    if (is_operator_character(lexer->lookahead, false)) {
        if (scan_char(lexer, '>')) {
            token = ConcreteTokenAngleBracketLeftRight;
        }
        else if (scan_operator(lexer, '<')) {
            token = ConcreteTokenOperator;
        }
        else {
            token = ConcreteTokenUncategorized;
        }
    }
    else {
        return ConcreteTokenLeftAngleBracket;
    }

    *has_trailing_whitespace = iswspace(lexer->lookahead);
    return token;
}

/**
 With the lexer on a non-whitespace character, try to recognize a
 `ConcreteToken`. Return indirectly whether the recognized item is followed by a
 whitespace character, which is relevant for operator parsing.
 */
static ConcreteToken scan_concrete(TSLexer * lexer,
                                   bool has_leading_whitespace,
                                   bool * has_trailing_whitespace)
{
    if (scan_char(lexer, '(')) {
        *has_trailing_whitespace = iswspace(lexer->lookahead);
        return ConcreteTokenOpenParen;
    }
    else if (scan_char(lexer, '{')) {
        *has_trailing_whitespace = iswspace(lexer->lookahead);
        return ConcreteTokenLeftCurlyBrace;
    }
    else if (scan_char(lexer, '}')) {
        *has_trailing_whitespace = iswspace(lexer->lookahead);
        return ConcreteTokenRightCurlyBrace;
    }
    else if (scan_char(lexer, '<')) {
        return finish_left_angle_token(lexer, has_trailing_whitespace);
    }
    else if (scan_char(lexer, '?')) {
        if (is_operator_character(lexer->lookahead, false)) {
            scan_operator(lexer, '?');
            *has_trailing_whitespace = iswspace(lexer->lookahead);
            return ConcreteTokenOperator;
        }
        else {
            *has_trailing_whitespace = iswspace(lexer->lookahead);
            return ConcreteTokenQuestionMark;
        }
    }
    else if (is_operator_head(lexer->lookahead)){
        const int32_t head = lexer->lookahead;
        advance(lexer);
        if ('/' == head) {
            if (finish_line_comment_delim(lexer)) {
                *has_trailing_whitespace = iswspace(lexer->lookahead);
                return ConcreteTokenLineCommentDelimiter;
            }
            else if (finish_block_comment_open(lexer)) {
                *has_trailing_whitespace = iswspace(lexer->lookahead);
                return ConcreteTokenBlockCommentDelimiter;
            }
            else if (lookahead_regex_literal(lexer)) {
                *has_trailing_whitespace = iswspace(lexer->lookahead);
                return ConcreteTokenRegexLiteralDelimiter;
            }
        }

        const ConcreteToken token = scan_operator(lexer, head)
            ? ConcreteTokenOperator
            : ConcreteTokenSpecialOperator;
        *has_trailing_whitespace = iswspace(lexer->lookahead);
        return token;
    }
    else if (is_identifier_char(lexer->lookahead)) {
        ConcreteToken token;
        if (scan_nonexpression_keyword(lexer)) {
            token = ConcreteTokenStatementKeyword;
        }
        else if (lookahead_labelled_closure(lexer)) {
            token = ConcreteTokenClosureLabel;
        }
        else {
            token = ConcreteTokenUncategorized;
        }
        *has_trailing_whitespace = iswspace(lexer->lookahead);
        return token;
    }

    return ConcreteTokenUncategorized;
}

static bool can_start_expression(uint32_t c)
{
    switch (c) {
        case '(':
        case '[':
        case '`':
            return true;

        default:
            return is_identifier_char(c);
    }
}

/**
 After having found a `ConcreteTokenOperator`, figure out what kind of `Token`
 it is part of. This will set the lexer's result symbol, mark the end of the
 text, and return `true` if the operator is validated.

 The result symbol may instead be a zero-width `ReturnValue`, if that is
 included in `valid_tokens` and the text is either a prefix or a bare operator.
 (This unfortunately means that the scanner will have to come back and re-parse
 the operator for the appropriate expression rule.)
 */
static bool resolve_operator(TSLexer * lexer,
                             const bool * valid_tokens,
                             bool has_leading_whitespace,
                             bool has_trailing_whitespace)
{
    const bool has_consistent_whitespace =
        (has_leading_whitespace == has_trailing_whitespace);

    const bool has_trailing_expression = can_start_expression(lexer->lookahead);
    const bool is_postfix_possible =
        !(has_leading_whitespace) && !(has_trailing_expression);
    const bool is_prefix_possible =
        !(has_trailing_whitespace) && has_trailing_expression;

    if (valid_tokens[PostfixOperator] && is_postfix_possible) {
        mark_end(lexer);
        lexer->result_symbol = PostfixOperator;
        return true;
    }
    else if (valid_tokens[ReturnValue] && is_prefix_possible) {
        // This is a prefix expression but we need to accept the zero-width
        // `ReturnValue` rule first.
        lexer->result_symbol = ReturnValue;
        return true;
    }
    else if (valid_tokens[PrefixOperator] && is_prefix_possible) {
        mark_end(lexer);
        lexer->result_symbol = PrefixOperator;
        return true;
    }
    else if (valid_tokens[BinaryOperator] && has_consistent_whitespace) {
        mark_end(lexer);
        lexer->result_symbol = BinaryOperator;
        return true;
    }
    else if (valid_tokens[ReturnValue]) {
        // The expression is an operator reference but we need to accept the
        // zero-width `ReturnValue` rule first.
        lexer->result_symbol = ReturnValue;
        return true;
    }
    else if (valid_tokens[OperatorName]) {
        mark_end(lexer);
        lexer->result_symbol = OperatorName;
        return true;
    }

    return false;
}

//MARK:- Tree-sitter scanner API

void * tree_sitter_swift_external_scanner_create()
{
    return calloc(1, sizeof(Scanner));
}

void tree_sitter_swift_external_scanner_destroy(void * storage)
{
    free(storage);
}

unsigned int tree_sitter_swift_external_scanner_serialize(void * storage,
                                                          char * buffer)
{
    const size_t size = sizeof(Scanner);
    const Scanner * const scanner = (Scanner *)storage;
    memcpy(buffer, &(scanner->octothorpe_count), size);
    return size;
}

void tree_sitter_swift_external_scanner_deserialize(void * storage,
                                                    const char * buffer,
                                                    unsigned length)
{
    const size_t size = sizeof(Scanner);
    if (size > length) {
        return;
    }

    Scanner * const scanner = (Scanner *)storage;
    memcpy(&(scanner->octothorpe_count), buffer, size);
}

#define ACCEPT_IF(token, condition) lexer->result_symbol = token; \
    return valid_tokens[token] && (condition)

#define ACCEPT(token) ACCEPT_IF(token, true)

bool tree_sitter_swift_external_scanner_scan(void * storage,
                                             TSLexer * lexer,
                                             const bool * valid_tokens)
{
    Scanner * const scanner = (Scanner *)storage;

    if (valid_tokens[ErrorRecovery]) {
        return false;
    }

    if (valid_tokens[StringText]) {
        return scan_string_text(lexer);
    }
    else if (valid_tokens[MultilineStringText]) {
        return scan_multiline_string_text(lexer);
    }
    else if (valid_tokens[RawOnelineStringOpenDelim] ||
             valid_tokens[RawMultilineOpenDelim]) {
        return scan_raw_string_open_delim(lexer, scanner, valid_tokens);
    }
    else if (in_raw_string(valid_tokens)) {
        return continue_raw_string(lexer, scanner, false);
    }
    else if (in_raw_multiline_string(valid_tokens)) {
        return continue_raw_string(lexer, scanner, true);
    }
    else if (valid_tokens[BlockCommentContents]) {
        return scan_block_comment_contents(lexer);
    }

    mark_end(lexer);
    bool accept_semicolon = false;
    const bool has_leading_whitespace =
        scan_whitespace(lexer, &accept_semicolon);

    bool has_trailing_whitespace = false;
    const ConcreteToken lead_token = scan_concrete(lexer,
                                                   has_leading_whitespace,
                                                   &has_trailing_whitespace);

    Lexer * const full_lexer = (Lexer *)lexer;
    Length saved_position = { 0, { 0, 0 } };

    switch (lead_token) {
        case ConcreteTokenOpenParen:
            if (valid_tokens[ValidFunctionRefParams]) {
                ACCEPT_IF(ValidFunctionRefParams,
                          scan_function_ref_params(lexer));
            }
            else if (valid_tokens[ReturnValue]) {
                ACCEPT(ReturnValue);
            }
            else {
                ACCEPT_IF(AutomaticSemicolon, accept_semicolon);
            }

        case ConcreteTokenLeftCurlyBrace:
            if (valid_tokens[VariablePropertyObserverBlock]
                && lookahead_property_observer(lexer)) {
                ACCEPT(VariablePropertyObserverBlock);
            }
            else {
                ACCEPT(ReturnValue);
            }

        case ConcreteTokenRightCurlyBrace:
            ACCEPT(AutomaticSemicolon);

        case ConcreteTokenLineCommentDelimiter:
        case ConcreteTokenBlockCommentDelimiter:
            ACCEPT_IF(AutomaticSemicolon,
                      scan_automatic_semicolon(lexer, lead_token));

        case ConcreteTokenRegexLiteralDelimiter:
            return false;

        case ConcreteTokenQuestionMark:
            if (!has_leading_whitespace) {
                // This is an optional unwrapping "operator"
                mark_end(lexer);
                ACCEPT(PostfixOperator);
            }
            else {
                return false;
            }

        case ConcreteTokenLeftAngleBracket:
            saved_position = full_lexer->current_position;
            if (valid_tokens[GenericArgOpenDelim]
                && scan_generic_arg_brackets(lexer)) {
                ACCEPT(GenericArgOpenDelim);
            }
            else if (valid_tokens[BinaryOperator]) {
                full_lexer->token_end_position = saved_position;
                ACCEPT(BinaryOperator);
            }
            else if (valid_tokens[OperatorName]) {
                full_lexer->token_end_position = saved_position;
                ACCEPT(OperatorName);
            }
            else {
                return false;
            }

        case ConcreteTokenAngleBracketLeftRight:
            if (valid_tokens[GenericArgOpenDelim]) {
                ACCEPT(GenericArgOpenDelim);
            }
            FALLTHROUGH;

        case ConcreteTokenOperator:
            return resolve_operator(lexer,
                                    valid_tokens,
                                    has_leading_whitespace,
                                    has_trailing_whitespace);

        case ConcreteTokenSpecialOperator:
            // Some of these are not expressions (function arrow, '=' for
            // assignment), but since they would be invalid code in this
            // position it does not hurt to include them.
            ACCEPT(ReturnValue);

        case ConcreteTokenClosureLabel:
            if (valid_tokens[MultipleTrailingClosures]) {
                ACCEPT(MultipleTrailingClosures);
            }
            else {
                ACCEPT_IF(AutomaticSemicolon, accept_semicolon);
            }

        case ConcreteTokenStatementKeyword:
            ACCEPT_IF(AutomaticSemicolon, accept_semicolon);

        case ConcreteTokenUncategorized:
            if (valid_tokens[ReturnValue]) {
                ACCEPT(ReturnValue);
            }
            else {
                ACCEPT_IF(AutomaticSemicolon, accept_semicolon);
            }
    }

    assert(false && "Scanner fell out of switch unexpectedly");
}

#pragma clang diagnostic pop
