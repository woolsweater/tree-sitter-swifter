.PHONY: build test clean

build_dir := ./build

build: ${build_dir}/swift.dylib

test: build test/corpus/* queries/highlights.scm
	tree-sitter test

clean:
	rm -rf "${build_dir}"

${build_dir}/swift.dylib: grammar.js src/scanner.c
	tree-sitter generate --build --abi=13 --libdir="${build_dir}"
