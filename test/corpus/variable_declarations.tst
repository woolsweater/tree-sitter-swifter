================
Constant declarations
================

let x: Recursive? = x;

let _secretNumber = 1_234.567;

@usableFromInline let xyzzy = quux;

let color: Int32 = 0xefaaa9;

let x = 0xff, y = 256;

let code = 0o755;

let highMask: UInt8 = 0b1110_0000, lowMask: UInt8 = 0b0001_1111;

let `default`: Default

----

(source_file
    (constant_declaration
        name: (identifier)
        type: (optional_type
                  wrapped: (simple_type (type_identifier)))
        initializer: (identifier))
    (constant_declaration
        name: (identifier)
        initializer: (numeric_literal))
    (constant_declaration
        (attribute name: (identifier))
        name: (identifier)
        initializer: (identifier))
    (constant_declaration
        name: (identifier)
        type: (simple_type (type_identifier))
        initializer: (numeric_literal))
    (constant_declaration
        name: (identifier)
        initializer: (numeric_literal)
        name: (identifier)
        initializer: (numeric_literal))
    (constant_declaration
        name: (identifier)
        initializer: (numeric_literal))
    (constant_declaration
        name: (identifier)
        type: (simple_type (type_identifier))
        initializer: (numeric_literal)
        name: (identifier)
        type: (simple_type (type_identifier))
        initializer: (numeric_literal))
    (constant_declaration
        name: (escaped_identifier)
        type: (simple_type (type_identifier))))


================
Async let declaration
================

async let foo = bar()

----

(source_file
    (constant_declaration
        name: (identifier)
        initializer: (call_expression
                         function: (identifier)
                         (argument_list))))


================
Simple variable declarations
================

var x = x;

var color = 0xefaaa9;
var otherColor: Color = .white;

var x = 10, y: Y = x;

@magic
var code = 0o333;

var accum: UInt8 = 0b0000;

----

(source_file
    (variable_declaration
        name: (identifier)
        initializer: (identifier))
    (variable_declaration
        name: (identifier)
        initializer: (numeric_literal))
    (variable_declaration
        name: (identifier)
        type: (simple_type (type_identifier))
        initializer: (implicit_member_expression (identifier)))
    (variable_declaration
        name: (identifier)
        initializer: (numeric_literal)
        name: (identifier)
        type: (simple_type (type_identifier))
        initializer: (identifier))
    (variable_declaration
        (attribute name: (identifier))
        name: (identifier)
        initializer: (numeric_literal))
    (variable_declaration
        name: (identifier)
        type: (simple_type (type_identifier))
        initializer: (numeric_literal)))


================
Variable initializer with trailing closure
================

var foo: [Foo] = (0...n).map { _ in Foo() }

var bar = baz { Quux.plugh() }

----

(source_file
    (variable_declaration
        name: (identifier)
        type: (array_type
                  element: (simple_type
                               (type_identifier)))
        initializer:
            (call_expression
                function: (member_expression
                               base: (parenthesized_expression
                                         (range_expression
                                             lower_bound: (numeric_literal)
                                             upper_bound: (identifier)))
                               member: (identifier))
                (trailing_closures
                    (closure_expression
                        (parameter_list
                            (parameter name: (identifier)))
                        (body
                            (call_expression
                                function: (identifier)
                                (argument_list)))))))
   (variable_declaration
        name: (identifier)
        initializer:
            (call_expression
                function: (identifier)
                (trailing_closures
                    (closure_expression
                        (body
                            (call_expression
                                function: (member_expression
                                              base: (identifier)
                                              member: (identifier))
                                (argument_list))))))))


================
Lazy variables
================

func foo() {
    lazy var bar = 10

    lazy var baz: Bool = {
        true
    }()
}

----

(source_file
    (function_declaration
        name: (identifier)
        (parameter_list)
        body: (block
                  (variable_declaration
                      name: (identifier)
                      initializer: (numeric_literal))
                  (variable_declaration
                      name: (identifier)
                      type: (simple_type (type_identifier))
                      initializer:
                      (call_expression
                          function: (closure_expression
                                        (body
                                            (boolean_literal)))
                          (argument_list))))))


================
Getters and setters
================

var flag: Bool {
    get { return _flag }
    set { self._flag = newValue }
}

var flag: Bool {
    self._flag
}

var color: Color {
    get { return .blue }
    set (`newColor`) { self._color = newColor }
}

var foo: Foo
{
    switch self {
        case .bar: return Bar()
    }
}

----

(source_file
    (variable_declaration
        name: (identifier)
        type: (simple_type (type_identifier))
        (accessor_block
            (getter_clause
                (block (return_statement value: (identifier))))
            (setter_clause
                (block
                    (assignment_expression
                        location: (member_expression
                                      base: (self)
                                      member: (identifier))
                        value: (identifier))))))
    (variable_declaration
        name: (identifier)
        type: (simple_type (type_identifier))
        implicit_getter: (block
            (member_expression
                base: (self)
                member: (identifier))))
    (variable_declaration
        name: (identifier)
        type: (simple_type (type_identifier))
        (accessor_block
            (getter_clause
                (block
                    (return_statement
                        value: (implicit_member_expression (identifier)))))
            (setter_clause
                setter_name: (escaped_identifier)
                (block
                    (assignment_expression
                        location: (member_expression
                                      base: (self)
                                      member: (identifier))
                        value: (identifier))))))
    (variable_declaration
        name: (identifier)
        type: (simple_type
                  (type_identifier))
        implicit_getter: (block
           (switch_statement
               expression: (self)
               (switch_case
                   (match_descriptor
                       pattern: (member_pattern
                                    member: (identifier)))
                   (case_body
                       (return_statement
                           value: (call_expression
                                      function: (identifier)
                                      (argument_list)))))))))


================
Modify and read accessors
================

var x: Int {
    _modify { yield &_x }
    get { _x }
    _read { yield _x }
}

var foo: Bar {
    mutating _read { }
    get { }
    _modify { }
    set { }
}

----

(source_file
    (variable_declaration
        name: (identifier)
        type: (simple_type (type_identifier))
        (accessor_block
            (modify_clause
                (block (yield_expression (prefix_expression
                                             (prefix_operator)
                                             operand: (identifier)))))
            (getter_clause (block (identifier)))
            (reader_clause (block (yield_expression (identifier))))))
    (variable_declaration
        name: (identifier)
        type: (simple_type (type_identifier))
        (accessor_block
            (reader_clause (block))
            (getter_clause (block))
            (modify_clause (block))
            (setter_clause (block)))))


================
Effectful getters
================

var foo: Foo {
    get async { }
}


var bar: Bar {
    get throws { }
}

var quux: Quux {
    get async throws { }
}

var plugh: Plugh {
    get throws(PlughError) { }
}


----

(source_file
    (variable_declaration
        name: (identifier)
        type: (simple_type (type_identifier))
        (accessor_block
            (getter_clause
                (async_effect)
                (block))))
    (variable_declaration
        name: (identifier)
        type: (simple_type (type_identifier))
        (accessor_block
            (getter_clause
                (throw_effect)
                (block))))
    (variable_declaration
        name: (identifier)
        type: (simple_type (type_identifier))
        (accessor_block
            (getter_clause
                (async_effect)
                (throw_effect)
                (block))))
    (variable_declaration
        name: (identifier)
        type: (simple_type (type_identifier))
        (accessor_block
            (getter_clause
                (throw_effect
                    error_type: (simple_type (type_identifier)))
                (block)))))


================
Init accessors
================

var foo: Int {
    @storageRestrictions(initializes: _foo, _baz, accesses: quux)
    init(bar) {
        print(quux)
        _foo = bar
        _baz = bar
    }

    get { _foo }
    set { _foo = newValue }
}

var plugh: Int {
    init {
    }

    get { 0 }
}

----

(source_file
    (variable_declaration
        name: (identifier)
        type: (simple_type (type_identifier))
        (accessor_block
            (initializer_clause
                (attribute
                    name: (identifier)
                    (argument_list
                        (argument label: (identifier)
                                  value: (identifier))
                        (argument value: (identifier))
                        (argument label: (identifier)
                                  value: (identifier))))
                (init_accessor_parameter_list
                    parameter: (identifier))
                (block
                    (call_expression
                        function: (identifier)
                        (argument_list
                            (argument value: (identifier))))
                    (assignment_expression
                        location: (identifier)
                        value: (identifier))
                    (assignment_expression
                        location: (identifier)
                        value: (identifier))))
            (getter_clause
                (block (identifier)))
            (setter_clause
                (block (assignment_expression
                           location: (identifier)
                           value: (identifier))))))
    (variable_declaration
        name: (identifier)
        type: (simple_type (type_identifier))
        (accessor_block
            (initializer_clause
                (block))
            (getter_clause (block (numeric_literal))))))


================
Unsafe accessors
================

var pointee: Int {
    unsafeAddress { return Pointer(self) }
    nonmutating unsafeMutableAddress { return self }
}

struct Foo {
    subscript(i: Int) -> Int {
        unsafeAddress { return bar }
    }
}

----

(source_file
    (variable_declaration
        name: (identifier)
        type: (simple_type (type_identifier))
        (accessor_block
            (unsafe_accessor_clause
                (block
                    (return_statement
                        value: (call_expression
                                   function: (identifier)
                                   (argument_list
                                       (argument value: (self)))))))
            (unsafe_accessor_clause
                (block (return_statement value: (self))))))
    (struct_declaration
        name: (type_identifier)
        (type_body
            (subscript_declaration
                (parameter_list
                    (parameter
                        local_name: (identifier)
                        type: (simple_type (type_identifier))))
                return_type: (simple_type (type_identifier))
                (accessor_block
                    (unsafe_accessor_clause
                        (block
                            (return_statement
                                value: (identifier)))))))))


================
Property observers
================

var flag: Bool {
    didSet { self.otherFlag = flag }
}

var color: Color {
    willSet {
        self.otherColor = self.color
    }
}

var flag: Bool {
    willSet(foo) { oldValue }
    didSet(bar) { self.otherFlag = flag }
}

var color: Color {
    didSet { oldValue }
    willSet { self.otherColor = self.color }
}

----

(source_file
    (variable_declaration
        name: (identifier)
        type: (simple_type (type_identifier))
        (property_observer_block
            (did_set_clause
                body: (block
                      (assignment_expression
                          location: (member_expression
                                        base: (self)
                                        member: (identifier))
                          value: (identifier))))))
    (variable_declaration
        name: (identifier)
        type: (simple_type (type_identifier))
        (property_observer_block
            (will_set_clause
                body: (block
                    (assignment_expression
                        location: (member_expression
                                      base: (self)
                                      member: (identifier))
                        value: (member_expression
                                   base: (self)
                                   member: (identifier)))))))
    (variable_declaration
        name: (identifier)
        type: (simple_type (type_identifier))
        (property_observer_block
            (will_set_clause
                setter_name: (identifier)
                body: (block (identifier)))
            (did_set_clause
                setter_name: (identifier)
                body: (block
                    (assignment_expression
                        location: (member_expression
                                      base: (self)
                                      member: (identifier))
                        value: (identifier))))))
    (variable_declaration
        name: (identifier)
        type: (simple_type (type_identifier))
        (property_observer_block
            (did_set_clause
                body: (block (identifier)))
            (will_set_clause
                body: (block
                    (assignment_expression
                        location: (member_expression
                                      base: (self)
                                      member: (identifier))
                        value: (member_expression
                                   base: (self)
                                   member: (identifier))))))))


================
Access level modifiers
================

private let x = 10

public var foo: Foo {
    cachedFoo
}

internal var flag: Bool = false

package let frobozz: String = "waldo"

----

(source_file
    (constant_declaration
        (access_level_modifier)
        name: (identifier)
        initializer: (numeric_literal))
    (variable_declaration
        (access_level_modifier)
        name: (identifier)
        type: (simple_type (type_identifier))
        implicit_getter: (block (identifier)))
    (variable_declaration
        (access_level_modifier)
        name: (identifier)
        type: (simple_type (type_identifier))
        initializer: (boolean_literal))
    (constant_declaration
        (access_level_modifier)
        name: (identifier)
        type: (simple_type
                  (type_identifier))
        initializer: (string_literal (string_text))))


================
Ownership modifiers
================

weak var foo: Foo?

unowned var bar: Bar

----

(source_file
    (variable_declaration
        (ownership_modifier)
        name: (identifier)
        type: (optional_type
                  wrapped: (simple_type
                               (type_identifier))))
    (variable_declaration
        (ownership_modifier)
        name: (identifier)
        type: (simple_type (type_identifier))))


================
Tuple bindings
================

let (foo, bar) = (10, 12)

var (baz, quux): (String, Bool)

----

(source_file
    (constant_declaration
        (tuple_binding
            (identifier)
            (identifier))
        initializer: (tuple_expression
                         (numeric_literal)
                         (numeric_literal)))
    (variable_declaration
        (tuple_binding
            (identifier)
            (identifier)
            type: (tuple_type
                      (simple_type
                          (type_identifier))
                      (simple_type
                          (type_identifier))))))


================
Property wrappers
================

@Foo var foo: Bar

@Baz(\.quux, plugh) var frobozz: Frobozz

@Default<False> var isFlagged: Bool

----

(source_file
    (variable_declaration
        (attribute name: (identifier))
        name: (identifier)
        type: (simple_type (type_identifier)))
    (variable_declaration
        (attribute
            name: (identifier)
            (argument_list
                (argument
                    value: (key_path_expression
                               (key_path_segment (identifier))))
                (argument value: (identifier))))
        name: (identifier)
        type: (simple_type (type_identifier)))
    (variable_declaration
        (attribute
            name: (identifier)
            (generic_argument_list
                (simple_type (type_identifier))))
        name: (identifier)
        type: (simple_type (type_identifier))))
