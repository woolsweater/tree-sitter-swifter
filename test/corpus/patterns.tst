================
Case conditions
================

if case (x: _, 10) = t {}

guard case let .load(file)? = action else { return }

if case .foo(.bar(let baz?, quux: _)) = plugh {}

----

(source_file
    (if_statement
        condition: (case_condition
                       pattern: (tuple_pattern
                                    (tuple_pattern_element
                                        label: (identifier)
                                        (identifier))
                                    (tuple_pattern_element (numeric_literal)))
                       value: (identifier))
        body: (block))
    (guard_statement
        condition: (case_condition
            pattern: (binding_match_pattern
                         binding: (optional_pattern
                                      (member_pattern
                                          member: (identifier)
                                          (assoc_value_patterns
                                              (identifier)))))
            value: (identifier))
        body: (block (return_statement)))
    (if_statement
        condition: (case_condition
            pattern: (member_pattern
                member: (identifier)
                (assoc_value_patterns
                    (member_pattern
                        member: (identifier)
                        (assoc_value_patterns
                            (binding_match_pattern
                                binding: (optional_pattern (identifier)))
                            label: (identifier) (identifier)))))
            value: (identifier))
        body: (block)))


================
Tuple binding
================

if let (item, foo: i) = lookUp() {}

if let (bar, baz,) = quux {}

----

(source_file
    (if_statement
        condition:
            (optional_binding_condition
                (tuple_binding
                    (identifier)
                    label: (identifier)
                    (identifier))
                    (initializer
                        (call_expression
                            function: (identifier)
                            (argument_list))))
        body: (block))
    (if_statement
        condition: (optional_binding_condition
                       (tuple_binding
                           (identifier)
                           (identifier))
                       (initializer (identifier)))
        body: (block)))


================
Switch case patterns
================

switch foo.bar {
    case quux:
        break
    case .xyyzy(let plugh):
        break
    case Port.foozle:
        break
    case let Double.fanucci(fromps):
        break
    case Wishy.foo(x: let x, y: let y):
        break
    case .waldo, let frobozz:
        break
    case frotz(10):
        break
    case Megaboz<Zipso>("furgalneti"):
        break
    case Foo.bar.baz():
        break
    case Foo<Bar>.baz.quux:
        break
    case foo..<bar:
        break
}

switch (foo, bar) {
    case let (f: foo?, bar?):
        break
    case let (g: foo?, nil):
        break
    case (h: quux, plugh...(10)):
        break
    case (xyzzy, megaboz,):
        break
    case .foo(/bar/):
        break
    case .baz(#/quux/#):
        break
    case \Plugh.frobozz:
        break
    case \.waldo:
        break
}

----

(source_file
    (switch_statement
        expression: (member_expression
                                base: (identifier)
                                member: (identifier))
        (switch_case
            (match_descriptor pattern: (identifier))
            (case_body (break_statement)))
        (switch_case
            (match_descriptor
                pattern: (member_pattern
                             member: (identifier)
                             (assoc_value_patterns
                                (binding_match_pattern
                                    binding: (identifier)))))
            (case_body (break_statement)))
        (switch_case
            (match_descriptor
                pattern: (member_pattern
                             base: (type_identifier)
                             member: (identifier)))
            (case_body (break_statement)))
        (switch_case
            (match_descriptor
                pattern: (binding_match_pattern
                             binding: (member_pattern
                                          base: (type_identifier)
                                          member: (identifier)
                                          (assoc_value_patterns
                                              (identifier)))))
            (case_body (break_statement)))
        (switch_case
            (match_descriptor
                pattern: (member_pattern
                             base: (type_identifier)
                             member: (identifier)
                             (assoc_value_patterns
                                 label: (identifier)
                                 (binding_match_pattern
                                    binding: (identifier))
                                 label: (identifier)
                                 (binding_match_pattern
                                    binding: (identifier)))))
            (case_body (break_statement)))
        (switch_case
            (match_descriptor
                pattern: (member_pattern member: (identifier)))
            (match_descriptor
                pattern: (binding_match_pattern
                             binding: (identifier)))
                (case_body (break_statement)))
        (switch_case
            (match_descriptor
                pattern: (call_pattern
                             function: (identifier)
                             (argument_list
                                (argument value: (numeric_literal)))))
            (case_body (break_statement)))
        (switch_case
            (match_descriptor
                pattern: (call_pattern
                             function: (generic_type
                                           (generic_argument_list
                                               (simple_type (type_identifier))))
                             (argument_list
                                 (argument
                                    value: (string_literal (string_text))))))
            (case_body (break_statement)))
        (switch_case
            (match_descriptor
                pattern: (member_pattern
                             base: (type_identifier)
                             member: (identifier)
                             member: (identifier)))
            (case_body (break_statement)))
        (switch_case
            (match_descriptor
                pattern: (member_pattern
                             base: (generic_type
                                       (generic_argument_list
                                           (simple_type (type_identifier))))
                             member: (identifier)
                             member: (identifier)))
            (case_body (break_statement)))
        (switch_case
            (match_descriptor
                pattern: (range_pattern
                             lower_bound: (identifier)
                             upper_bound: (identifier)))
            (case_body (break_statement))))
    (switch_statement
        expression: (tuple_expression (identifier) (identifier))
        (switch_case
            (match_descriptor
                pattern: (binding_match_pattern
                             binding: (tuple_pattern
                                          (tuple_pattern_element
                                              label: (identifier)
                                              (optional_pattern
                                                  (identifier)))
                                          (tuple_pattern_element
                                              (optional_pattern
                                                  (identifier))))))
            (case_body (break_statement)))
        (switch_case
            (match_descriptor
                pattern: (binding_match_pattern
                             binding: (tuple_pattern
                                          (tuple_pattern_element
                                              label: (identifier)
                                              (optional_pattern (identifier)))
                                          (tuple_pattern_element (nil)))))
            (case_body (break_statement)))
        (switch_case
            (match_descriptor
                pattern: (tuple_pattern
                            (tuple_pattern_element
                                label: (identifier)
                                (identifier))
                            (tuple_pattern_element
                                (range_pattern
                                    lower_bound: (identifier)
                                    upper_bound: (numeric_literal)))))
            (case_body (break_statement)))
       (switch_case
            (match_descriptor
                pattern: (tuple_pattern
                             (tuple_pattern_element (identifier))
                             (tuple_pattern_element (identifier))))
            (case_body (break_statement)))
       (switch_case
           (match_descriptor
               pattern: (member_pattern
                            member: (identifier)
                            (assoc_value_patterns
                                (regex_literal))))
           (case_body (break_statement)))
       (switch_case
           (match_descriptor
               pattern: (member_pattern
                            member: (identifier)
                            (assoc_value_patterns
                                (regex_literal))))
           (case_body (break_statement)))
       (switch_case
           (match_descriptor
               pattern: (key_path_expression
                            (key_path_base
                                (type_expression (type_identifier)))
                            (key_path_segment (identifier))))
            (case_body (break_statement)))
       (switch_case
           (match_descriptor
               pattern: (key_path_expression
                            (key_path_segment (identifier))))
           (case_body (break_statement)))))


================
Cast patterns
================

switch i {
    case 1 as UInt8:
        break
    case is Double:
        break
    case let x as Int64:
        break
}

switch T.self {
    case is [Int]: break
}

----

(source_file
    (switch_statement
        expression: (identifier)
        (switch_case
            (match_descriptor
                pattern: (as_pattern
                             operand: (numeric_literal)
                             type: (simple_type (type_identifier))))
            (case_body (break_statement)))
        (switch_case
            (match_descriptor
                pattern: (is_pattern
                             (simple_type (type_identifier))))
            (case_body (break_statement)))
        (switch_case
            (match_descriptor
                pattern: (binding_match_pattern
                             binding: (as_pattern
                                          operand: (identifier)
                                          type: (simple_type
                                                    (type_identifier)))))
            (case_body (break_statement))))
    (switch_statement
        expression: (member_expression
                        base: (identifier)
                        member: (self))
        (switch_case
            (match_descriptor
                pattern: (is_pattern
                             (array_type
                                 element: (simple_type
                                              (type_identifier)))))
            (case_body (break_statement)))))
