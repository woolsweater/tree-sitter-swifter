================
Numeric literals
================

0
1
1_0

1.0E-42
3.14159e10

0x1_0.0p16

2_345_678_901_234_567

1213

42

0xfeedface

0x0

0x1a_2b_3c_4d_5e_6f

0x1234567890

0o0

0o1234

0o1_1_2_2_3_4

0b1110_0000

0b0

0b1

0b000000000000000000000000


----

(source_file
    (numeric_literal) (numeric_literal) (numeric_literal)
    (numeric_literal) (numeric_literal)
    (numeric_literal)
    (numeric_literal)
    (numeric_literal)
    (numeric_literal)
    (numeric_literal)
    (numeric_literal)
    (numeric_literal)
    (numeric_literal)
    (numeric_literal)
    (numeric_literal)
    (numeric_literal)
    (numeric_literal)
    (numeric_literal)
    (numeric_literal)
    (numeric_literal))


================
Identifiers
================

x

_notPublic

this_is_my_variable

HowMuchWoodCouldAWoodChuckChuck

vc

----

(source_file
    (identifier) (identifier) (identifier) (identifier) (identifier))


================
Extended (non-ASCII) identifiers
================

let 🐄 = "Copy-on-write"

let Δt = 3.1415

----

(source_file
    (constant_declaration
        name: (identifier)
        initializer: (string_literal (string_text)))
    (constant_declaration
        name: (identifier)
        initializer: (numeric_literal)))


================
Escaped identifiers
================

var `default`: Foo

let `switch` = UISwitch()

----

(source_file
    (variable_declaration
        name: (escaped_identifier)
        type: (simple_type (type_identifier)))
    (constant_declaration
        name: (escaped_identifier)
        initializer: (call_expression
                         function: (identifier)
                         (argument_list))))

================
Assignment
================

x = x

color = 0xfeedface

view.color = .blue

x = (y = 100)

----

(source_file
    (assignment_expression
        location: (identifier)
        value: (identifier))
    (assignment_expression
        location: (identifier)
        value: (numeric_literal))
    (assignment_expression
        location: (member_expression
                      base: (identifier)
                      member: (identifier))
        value: (implicit_member_expression (identifier)))
    (assignment_expression
        location: (identifier)
        value: (parenthesized_expression
                   (assignment_expression
                       location: (identifier)
                       value: (numeric_literal)))))


================
self, Self, and super
================

self

Self

super

----

(source_file (self) (self) (super))


================
Member access
================

x.1.z

self.foo = bar

super.translatesAutoresizingMaskIntoConstraints

foo?.bar()

self?.delegate?.didTheThing()

foo
  .bar()
  .baz
  .quux(plugh: frobozz)

----

(source_file
    (member_expression
        base: (member_expression
                    base: (identifier)
                    member: (numeric_literal))
        member: (identifier))
    (assignment_expression
        location: (member_expression
                      base: (self)
                      member: (identifier))
        value: (identifier))
    (member_expression
        base: (super)
        member: (identifier))
    (call_expression
        function: (member_expression
                      base: (postfix_expression
                                  operand: (identifier)
                                  (postfix_operator))
                      member: (identifier))
        (argument_list))
    (call_expression
        function: (member_expression
                      base: (member_expression
                                  base: (postfix_expression
                                              operand: (self)
                                              (postfix_operator))
                                  member: (postfix_expression
                                              operand: (identifier)
                                              (postfix_operator)))
                      member: (identifier))
        (argument_list))
    (call_expression
        function:
        (member_expression
            base: (member_expression
                      base: (call_expression
                                function: (member_expression
                                              base: (identifier)
                                              member: (identifier))
                                (argument_list))
                      member: (identifier))
            member: (identifier))
        (argument_list
            (argument
                label: (identifier)
                value: (identifier)))))


================
Function calls
================

foo()

foo(bar, baz: &quux)

self.makeMeASandwich(with: .cheddar)

foo?(bar: baz)

quux($xyzzy: plugh)

frobozz($_: bookkeeper)

precondition(temperature < 1_000_000)

megaboz(1, 2,)

waldo(
    10,
    mumble: ...,
)

----

(source_file
    (call_expression
        function: (identifier)
        (argument_list))
    (call_expression
        function: (identifier)
        (argument_list
            (argument value: (identifier))
                (argument
                    label: (identifier)
                    value: (prefix_expression
                               (prefix_operator)
                               operand: (identifier)))))
   (call_expression
       function: (member_expression
                    base: (self)
                    member: (identifier))
       (argument_list
           (argument
               label: (identifier)
               value: (implicit_member_expression
                          (identifier)))))
    (call_expression
        function: (postfix_expression
                      operand: (identifier)
                      (postfix_operator))
        (argument_list
            (argument
                label: (identifier)
                value: (identifier))))
    (call_expression
        function: (identifier)
        (argument_list
            (argument label: (synthesized_identifier)
                      value: (identifier))))
    (call_expression
        function: (identifier)
        (argument_list
            (argument label: (synthesized_identifier)
                      value: (identifier))))
    (call_expression
        function: (identifier)
        (argument_list
            (argument
                value: (binary_expression
                           lhs: (identifier)
                           (binary_operator)
                           rhs: (numeric_literal)))))
    (call_expression
       function: (identifier)
       (argument_list
           (argument value: (numeric_literal))
           (argument value: (numeric_literal))))
    (call_expression
       function: (identifier)
       (argument_list
           (argument value: (numeric_literal))
           (argument
               label: (identifier)
               value: (unbounded_range_expression)))))


================
Boolean literals
================

true

false

----

(source_file
    (boolean_literal)
    (boolean_literal))


================
Nil literal
================

nil

----

(source_file (nil))


================
Void literal
================

let x: Void = ()

foo(bar: ())

----

(source_file
    (constant_declaration
        name: (identifier)
        type: (simple_type (type_identifier))
        initializer: (void_literal))
    (call_expression
        function: (identifier)
        (argument_list
            (argument
                label: (identifier)
                value: (void_literal)))))

================
Tuples
================

(x: foo, 100)

makeABox(size: (width: 10, height: 5))

let vs: (Void, Void) = (x = 10, y = 12)

(10, 20, 30,)

----

(source_file
    (tuple_expression
        label: (identifier)
        (identifier)
        (numeric_literal))
    (call_expression
        function: (identifier)
        (argument_list
            (argument
                label: (identifier)
                value: (tuple_expression
                           label: (identifier) (numeric_literal)
                           label: (identifier) (numeric_literal)))))
    (constant_declaration
        name: (identifier)
        type: (tuple_type
                  (simple_type (type_identifier))
                  (simple_type (type_identifier)))
        initializer: (tuple_expression
                         (assignment_expression
                             location: (identifier)
                             value: (numeric_literal))
                         (assignment_expression
                             location: (identifier)
                             value: (numeric_literal))))
    (tuple_expression
        (numeric_literal)
        (numeric_literal)
        (numeric_literal)))


================
Try expressions
================

try doOrDoNot()

try? thisMightJustWork()

try! thisHadBetterWork()

----

(source_file
    (try_expression
        (call_expression
            function: (identifier)
            (argument_list)))
    (try_expression
        (call_expression
            function: (identifier)
            (argument_list)))
    (try_expression
        (call_expression
            function: (identifier)
            (argument_list))))


================
Await expressions
================

await makeDinner()

try await (standInTheStation() + boardTheTrain())

----

(source_file
    (await_expression
        (call_expression
            function: (identifier)
            (argument_list)))
    (try_expression
        (await_expression
            (parenthesized_expression
                (binary_expression
                    lhs: (call_expression
                             function: (identifier)
                             (argument_list))
                    (binary_operator)
                    rhs: (call_expression
                             function: (identifier)
                             (argument_list)))))))


================
Consume expressions
================

consume foo

_ = consume bar

func baz(quux: Quux) {
    let plugh = consume quux
}

extension Frobozz {
    mutating func waldo() {
        consume self
    }

    func mumble() -> Mumble {
        return
            consume mumble
    }
}

----

(source_file
    (ownership_expression
        operand: (identifier))
    (assignment_expression
        location: (identifier)
        value: (ownership_expression
                   operand: (identifier)))
    (function_declaration
        name: (identifier)
        (parameter_list
            (parameter
                local_name: (identifier)
                type: (simple_type (type_identifier))))
        body: (block
                  (constant_declaration
                      name: (identifier)
                      initializer: (ownership_expression
                                       operand: (identifier)))))
    (extension_declaration
        extended_type: (simple_type
                           (type_identifier))
        (type_body
            (method_declaration
                (member_modifier)
                name: (identifier)
                (parameter_list)
                body: (block
                          (ownership_expression
                              operand: (self))))
            (method_declaration
                name: (identifier)
                (parameter_list)
                (function_result
                    return_type: (simple_type (type_identifier)))
                body: (block
                          (return_statement
                              value: (ownership_expression
                                         operand: (identifier))))))))


================
Copy expressions
================

copy foo

_ = copy bar

func baz(_ quux: borrowing Plugh) -> (Plugh, Plugh) {
     return (copy quux, copy quux)
}

----

(source_file
    (ownership_expression
        operand: (identifier))
    (assignment_expression
        location: (identifier)
        value: (ownership_expression
                   operand: (identifier)))
    (function_declaration
        name: (identifier)
        (parameter_list
            (parameter
                external_name: (identifier)
                local_name: (identifier)
                (arc_modifier)
                type: (simple_type (type_identifier))))
        (function_result
            return_type: (tuple_type
                             (simple_type (type_identifier))
                             (simple_type (type_identifier))))
        body: (block
                  (return_statement
                      value: (tuple_expression
                                 (ownership_expression
                                     operand: (identifier))
                                 (ownership_expression
                                     operand: (identifier)))))))


================
Casts
================

let limit = 128 as UInt8

self.foo as? Bar.Baz

quux as! Waldo.Frobozz<X>

if foo is Bar {}

----

(source_file
    (constant_declaration
        name: (identifier)
        initializer: (cast_expression
                         operand: (numeric_literal)
                         type: (simple_type (type_identifier))))
    (cast_expression
        operand: (member_expression
                     base: (self)
                     member: (identifier))
        type: (simple_type
                  (type_identifier)
                  (type_identifier)))
    (cast_expression
        operand: (identifier)
        type: (simple_type
                  (type_identifier)
                  (generic_type
                      (generic_argument_list
                          (simple_type (type_identifier))))))
    (if_statement
        condition: (cast_expression
                       operand: (identifier)
                       type: (simple_type (type_identifier)))
        body: (block)))


================
Closures
================

let noop: (Quux) -> Void = { _ in }

let sideEffect = { () in }

items.map({ $0 })

delegate.thatThingDidHappen = { foo in { print(foo) } }

delegate.thatThingDidHappen = { @MainActor foo in foo }

let foo: (Bar, Plugh<Baz>) -> Void = { (@Quux quux, @Plugh $plugh) in }

var frobozz: (X) -> Y {
    return { x in }
}

let waldo = { () throws(SquidError) -> MagicRuby in }

----

(source_file
    (constant_declaration
        name: (identifier)
        type: (function_type
                  (parameter_list
                      (parameter
                          type: (simple_type (type_identifier))))
                   (function_result
                       return_type: (simple_type (type_identifier))))
        initializer: (closure_expression
                        (parameter_list
                            (parameter name: (identifier)))))
    (constant_declaration
        name: (identifier)
        initializer: (closure_expression
                         (parameter_list)))
    (call_expression
        function: (member_expression
                      base: (identifier)
                      member: (identifier))
        (argument_list
            (argument value:
                (closure_expression
                    (body (implicit_closure_argument))))))
    (assignment_expression
        location: (member_expression
                      base: (identifier)
                      member: (identifier))
        value: (closure_expression
                   (parameter_list
                       (parameter name: (identifier)))
                   (body
                       (closure_expression
                           (body
                               (call_expression
                                   function: (identifier)
                                   (argument_list
                                       (argument value: (identifier)))))))))
    (assignment_expression
        location: (member_expression
                      base: (identifier)
                      member: (identifier))
        value: (closure_expression
                   (attribute name: (identifier))
                   (parameter_list
                       (parameter name: (identifier)))
                   (body
                       (identifier))))
   (constant_declaration
       name: (identifier)
       type: (function_type
                 (parameter_list
                     (parameter
                         type: (simple_type (type_identifier)))
                     (parameter
                         type: (simple_type
                                   (generic_type
                                       (generic_argument_list
                                           (simple_type (type_identifier)))))))
                 (function_result
                     return_type: (simple_type (type_identifier))))
       initializer: (closure_expression
                        (parameter_list
                            (parameter
                                (attribute name: (identifier))
                                name: (identifier))
                            (parameter
                                (attribute name: (identifier))
                                name: (synthesized_identifier)))))
    (variable_declaration
        name: (identifier)
        type: (function_type
                  (parameter_list
                      (parameter
                          type: (simple_type (type_identifier))))
                  (function_result
                      return_type: (simple_type (type_identifier))))
        implicit_getter:
            (block
                (return_statement
                    value: (closure_expression
                               (parameter_list
                                   (parameter name: (identifier)))))))
   (constant_declaration
       name: (identifier)
       initializer: (closure_expression
                        (parameter_list)
                        (function_result
                            (throw_effect
                                error_type: (simple_type
                                                (type_identifier)))
                            return_type: (simple_type
                                             (type_identifier))))))


================
Trailing closures
================

items.map { $0 }

makeABox { [weak self, foo = bar] (success: Bool) in
    self?.boxComplete(success)
}

numbers.reduce(0) { (total, next) in
    total + next
}

queue.async {
    makeABox()
}

VStack {
    Foo(bar: baz) {

    }
    Quux {

    }
}

xyzzy { [mumble, grault,] (jaz, fred,) in }

----

(source_file
    (call_expression
        function: (member_expression
                      base: (identifier)
                      member: (identifier))
        (trailing_closures              
            (closure_expression
                (body (implicit_closure_argument)))))
    (call_expression
        function: (identifier)
        (trailing_closures
            (closure_expression
                (capture_list
                    (capture_item
                        (ownership_modifier)
                        name: (self))
                    (capture_item
                        name: (identifier)
                        value: (identifier)))
                (parameter_list
                    (parameter
                        name: (identifier)
                        type: (simple_type (type_identifier))))
                (body
                    (call_expression
                        function: (member_expression
                                      base: (postfix_expression
                                                  operand: (self)
                                                  (postfix_operator))
                                      member: (identifier))
                        (argument_list
                            (argument value: (identifier))))))))
    (call_expression
        function: (member_expression
                      base: (identifier)
                      member: (identifier))
        (argument_list
            (argument value: (numeric_literal)))
        (trailing_closures
            (closure_expression
                (parameter_list
                    (parameter name: (identifier))
                    (parameter name: (identifier)))
                (body
                    (binary_expression
                        lhs: (identifier)
                        (binary_operator)
                        rhs: (identifier))))))
    (call_expression
        function: (member_expression
                      base: (identifier)
                      member: (identifier))
        (trailing_closures              
            (closure_expression
                (body
                    (call_expression
                        function: (identifier)
                        (argument_list))))))
    (call_expression
        function: (identifier)
        (trailing_closures        
            (closure_expression
                (body (call_expression
                          function: (identifier)
                          (argument_list
                              (argument label: (identifier)
                                        value: (identifier)))
                      (trailing_closures
                          (closure_expression)))
                      (call_expression
                          function: (identifier)
                          (trailing_closures
                              (closure_expression)))))))
    (call_expression
        function: (identifier)
        (trailing_closures
            (closure_expression
                (capture_list
                    (capture_item name: (identifier))
                    (capture_item name: (identifier)))
                (parameter_list
                    (parameter name: (identifier))
                    (parameter name: (identifier)))))))

================
Multiple trailing closures
================

foo.bar(baz: quux) {
    xyzzy()
} plugh: {
    return
} frobozz: {
    return
}

Bar {}
  baz: {}

Bar {}
baz: while true {
  print()
}

----

(source_file
    (call_expression
        function: (member_expression
                      base: (identifier)
                      member: (identifier))
        (argument_list
            (argument
                label: (identifier)
                value: (identifier)))
        (trailing_closures        
            (closure_expression
                (body
                    (call_expression
                        function: (identifier)
                        (argument_list))))
            (trailing_closure            
                (label (identifier))
                (closure_expression
                    (body (return_statement))))
            (trailing_closure        
                (label (identifier))
                (closure_expression
                    (body (return_statement))))))
   (call_expression
       function: (identifier)
       (trailing_closures
           (closure_expression)
           (trailing_closure
               (label (identifier))
               (closure_expression))))
   (call_expression
       function: (identifier)
       (trailing_closures
           (closure_expression)))
       (labelled_statement
           (statement_label (identifier))
           (while_statement
               condition: (boolean_literal)
               body: (block
                   (call_expression
                       function: (identifier)
                       (argument_list))))))


================
Dollar sign identifiers
================

let bars = foos.map({ $0.toBar() })

Button(self.$buttonText, action: {})

----

(source_file
    (constant_declaration
        name: (identifier)
        initializer:
            (call_expression
                function: (member_expression
                              base: (identifier)
                              member: (identifier))
                (argument_list
                    (argument value: (closure_expression (body
                                         (call_expression
                                             function:
                                             (member_expression
                                                 base: (implicit_closure_argument)
                                                 member: (identifier))
                                             (argument_list))))))))
    (call_expression
        function: (identifier)
        (argument_list
            (argument
                value:
                    (member_expression
                        base: (self)
                        member: (synthesized_identifier)))
            (argument
                label: (identifier)
                value: (closure_expression)))))


================
Key path expressions
================

let path = \Foo<Bar>.baz

_ = \Quux.plugh

bars.map(\.foo)

let slice = \Array<Foo>[10..<12].bar

_ = \.bars[2][12].quux.plugh[0].frobozz

compactMap(\.waldo?[i]!.megaboz[j]?.zipso)

let path = \Foo.Type.bar

let xs = ys.map(\.?.z?.ω)

----

(source_file
    (constant_declaration
        name: (identifier)
        initializer: (key_path_expression
                         (key_path_base
                             (type_expression
                                 (generic_type
                                     (generic_argument_list
                                         (simple_type (type_identifier))))))
                         (key_path_segment (identifier))))
    (assignment_expression
        location: (identifier)
        value: (key_path_expression
                   (key_path_base
                       (type_expression (type_identifier)))
                   (key_path_segment (identifier))))
    (call_expression
        function: (member_expression
                      base: (identifier)
                      member: (identifier))
        (argument_list
            (argument
                value: (key_path_expression
                           (key_path_segment (identifier))))))
    (constant_declaration
        name: (identifier)
        initializer: (key_path_expression

                         (key_path_base
                             (type_expression
                                 (generic_type
                                     (generic_argument_list
                                         (simple_type (type_identifier)))))
                             subscript: (subscriptor
                                            (argument
                                                value: (range_expression
                                                lower_bound: (numeric_literal)
                                                upper_bound: (numeric_literal)))))
                         (key_path_segment (identifier))))
    (assignment_expression
        location: (identifier)
        value: (key_path_expression
                   (key_path_segment
                       (identifier)
                       subscript: (subscriptor
                                      (argument value: (numeric_literal)))
                       subscript: (subscriptor
                                      (argument value: (numeric_literal))))
                   (key_path_segment (identifier))
                   (key_path_segment
                       (identifier)
                       subscript: (subscriptor
                                      (argument value: (numeric_literal))))
                   (key_path_segment (identifier))))
    (call_expression
        function: (identifier)
        (argument_list
            (argument
                value: (key_path_expression
                           (key_path_segment
                               (identifier)
                               subscript: (subscriptor
                                              (argument value: (identifier))))
                           (key_path_segment
                               (identifier)
                               subscript: (subscriptor
                                              (argument value: (identifier))))
                           (key_path_segment (identifier))))))
    (constant_declaration
        name: (identifier)
        initializer: (key_path_expression
                         (key_path_base
                             (type_expression (type_identifier)))
                         (key_path_segment (identifier))
                         (key_path_segment (identifier))))
    (constant_declaration
        name: (identifier)
        initializer: (call_expression
                         function: (member_expression
                                       base: (identifier)
                                       member: (identifier))
                         (argument_list
                             (argument
                                 value:
                                 (key_path_expression
                                     (key_path_segment)
                                     (key_path_segment (identifier))
                                     (key_path_segment (identifier))))))))


================
Key path string expressions (old-style)
================

let foo = #keyPath(Bar.baz)

quux(#keyPath(xyzzy))

----

(source_file
    (constant_declaration
        name: (identifier)
        initializer: (key_path_string_expression
                         root: (type_expression (type_identifier))
                         value: (identifier)))
    (call_expression
        function: (identifier)
        (argument_list
            (argument
                value: (key_path_string_expression
                           value: (identifier))))))


================
Array literals
================

[1, 2, 3, 4]

let x: [String] = []

let items = [plugh]

let foo = {
    [bar, baz, quux, waldo.frobozz]
}

[foo].map(transform)

let fns = [foo(_:), bar(baz:)]

let xs = [x = 10,]

----

(source_file
    (array_literal
        (numeric_literal)
        (numeric_literal)
        (numeric_literal)
        (numeric_literal))
    (constant_declaration
        name: (identifier)
        type: (array_type
                  element: (simple_type (type_identifier)))
        initializer: (array_literal))
    (constant_declaration
        name: (identifier)
        initializer: (array_literal (identifier)))
    (constant_declaration
        name: (identifier)
        initializer: (closure_expression
                         (body
                            (array_literal
                                (identifier)
                                (identifier)
                                (identifier)
                                (member_expression
                                    base: (identifier)
                                    member: (identifier))))))
    (call_expression
        function: (member_expression
                      base: (array_literal (identifier))
                      member: (identifier))
        (argument_list
            (argument value: (identifier))))
    (constant_declaration
        name: (identifier)
        initializer: (array_literal
                         (function_reference
                             name: (identifier)
                             (function_reference_parameter_list
                                 parameter: (identifier)))
                         (function_reference
                              name: (identifier)
                              (function_reference_parameter_list
                                  parameter: (identifier)))))
    (constant_declaration
        name: (identifier)
        initializer: (array_literal
                        (assignment_expression
                            location: (identifier)
                            value: (numeric_literal)))))


================
Dictionary literals
================

[:]

let counts = [
    "Foo"  : 9,
    "Bar"  : 2,
    "Baz"  : 5,
    "Quux" : 100
]

let actions = ["message" : { print($0) }]

let foo = [.bar : baz(quux:plugh:)]

[y : x = 10]

----

(source_file
    (dictionary_literal)
    (constant_declaration
        name: (identifier)
        initializer: (dictionary_literal
                         (dictionary_element
                             key: (string_literal (string_text))
                             value: (numeric_literal))
                         (dictionary_element
                             key: (string_literal (string_text))
                             value: (numeric_literal))
                         (dictionary_element
                             key: (string_literal (string_text))
                             value: (numeric_literal))
                         (dictionary_element
                             key: (string_literal (string_text))
                             value: (numeric_literal))))
    (constant_declaration
        name: (identifier)
        initializer:
            (dictionary_literal
                (dictionary_element
                    key: (string_literal (string_text))
                    value: (closure_expression
                        (body (call_expression
                                  function: (identifier)
                                  (argument_list
                                      (argument value:
                                          (implicit_closure_argument)))))))))
    (constant_declaration
        name: (identifier)
        initializer: (dictionary_literal
                         (dictionary_element
                             key: (implicit_member_expression
                                      (identifier))
                             value: (function_reference
                                        name: (identifier)
                                        (function_reference_parameter_list
                                            parameter: (identifier)
                                            parameter: (identifier))))))
    (dictionary_literal
        (dictionary_element
            key: (identifier)
            value: (assignment_expression
                       location: (identifier)
                       value: (numeric_literal)))))


================
Binary expressions (significant whitespace)
================

foo / bar

foo + bar

foo - bar

foo ~= bar

foo == bar

foo != bar

foo &+ bar

foo**bar.baz

foo **  bar.baz;

foo <^> bar;

foo ?• bar

foo.+.+.bar

(foo) += (bar)

dotLike..edgeCase

functionReturnLike ->> edgeCase;

commentLike .// edgeCase

commentLike **/ edgeCase

doubleFanucci || (tripleUndertrump >= trebledFromps)

---

(source_file
    (binary_expression
        lhs: (identifier)
        (binary_operator)
        rhs: (identifier))
    (binary_expression
        lhs: (identifier)
        (binary_operator)
        rhs: (identifier))
    (binary_expression
        lhs: (identifier)
        (binary_operator)
        rhs: (identifier))
    (binary_expression
        lhs: (identifier)
        (binary_operator)
        rhs: (identifier))
    (binary_expression
        lhs: (identifier)
        (binary_operator)
        rhs: (identifier))
    (binary_expression
        lhs: (identifier)
        (binary_operator)
        rhs: (identifier))
    (binary_expression
        lhs: (identifier)
        (binary_operator)
        rhs: (identifier))
    (binary_expression
        lhs: (identifier)
        (binary_operator)
        rhs: (member_expression
                 base: (identifier)
                 member: (identifier)))
    (binary_expression
        lhs: (identifier)
        (binary_operator)
        rhs: (member_expression
                 base: (identifier)
                 member: (identifier)))
    (binary_expression
        lhs: (identifier)
        (binary_operator)
        rhs: (identifier))
    (binary_expression
        lhs: (identifier)
        (binary_operator)
        rhs: (identifier))
    (binary_expression
        lhs: (identifier)
        (binary_operator)
        rhs: (identifier))
    (binary_expression
        lhs: (parenthesized_expression (identifier))
        (binary_operator)
        rhs: (parenthesized_expression (identifier)))
    (binary_expression
        lhs: (identifier)
        (binary_operator)
        rhs: (identifier))
    (binary_expression
        lhs: (identifier)
        (binary_operator)
        rhs: (identifier))
    (binary_expression
        lhs: (identifier)
        (binary_operator)
        rhs: (identifier))
    (binary_expression
        lhs: (identifier)
        (binary_operator)
        rhs: (identifier))
    (binary_expression
        lhs: (identifier)
        (binary_operator)
        rhs: (parenthesized_expression
                (binary_expression
                     lhs: (identifier)
                     (binary_operator)
                     rhs: (identifier)))))


================
Binary expressions involving '<'
================

if x < y {
    foo()
}

----

(source_file
    (if_statement
        condition: (binary_expression
                       lhs: (identifier)
                       (binary_operator)
                       rhs: (identifier))
        body: (block
                  (call_expression
                      function: (identifier)
                      (argument_list)))))


================
Range expressions
================

4..<5

let x = 0...100

"0"..."9"

quux[...xyzzy]

let plugh: Waldo = ..<frobozz

(foo.bar)..<baz

let x = { $0...$1 }

let foo = bar[baz...]

let quux = baz..<

----

(source_file
    (range_expression
        lower_bound: (numeric_literal)
        upper_bound: (numeric_literal))
    (constant_declaration
        name: (identifier)
        initializer: (range_expression
                         lower_bound: (numeric_literal)
                         upper_bound: (numeric_literal)))

    (range_expression
        lower_bound: (string_literal (string_text))
        upper_bound: (string_literal (string_text)))
    (subscript_expression
        base: (identifier)
        (subscriptor
            (argument
                value: (range_expression
                upper_bound: (identifier)))))
    (constant_declaration
        name: (identifier)
        type: (simple_type (type_identifier))
        initializer: (range_expression
                         upper_bound: (identifier)))
    (range_expression
        lower_bound: (parenthesized_expression
                         (member_expression
                             base: (identifier)
                             member: (identifier)))
        upper_bound: (identifier))
    (constant_declaration
        name: (identifier)
        initializer: (closure_expression
                         (body
                            (range_expression
                                lower_bound: (implicit_closure_argument)
                                upper_bound: (implicit_closure_argument)))))
    (constant_declaration
        name: (identifier)
        initializer: (subscript_expression
                         base: (identifier)
                         (subscriptor
                             (argument
                                 value: (range_expression
                                            lower_bound:
                                            (identifier))))))
    (constant_declaration
        name: (identifier)
        initializer: (range_expression
                         lower_bound: (identifier))))


================
Selector expressions
================

#selector(ViewController.buttonTapped(_:))

#selector(textFieldDidEndEditing(_:reason:))

#selector(C.doThatThing(_:) as (C) -> (NSObject) -> Void)

----

(source_file
    (selector_expression
        (type_expression (type_identifier))
        name: (identifier)
        (function_reference_parameter_list
            parameter: (identifier)))
    (selector_expression
        name: (identifier)
        (function_reference_parameter_list
            parameter: (identifier)
            parameter: (identifier)))
    (selector_expression
        (type_expression (type_identifier))
        name: (identifier)
        (function_reference_parameter_list
            parameter: (identifier))
        explicit_type: (function_type
                           (parameter_list
                               (parameter
                                   type: (simple_type (type_identifier))))
                           (function_result
                               return_type: (function_type
                                   (parameter_list
                                       (parameter
                                           type: (simple_type
                                                     (type_identifier))))
                                   (function_result
                                       return_type: (simple_type
                                                        (type_identifier))))))))


================
Subscript expressions
================

_ = foos?[10]

counts[word, default: 0] += 1

mutate(&self[lastIndex])

let copy = list[...]

let foo = bar[baz: ..., 10,]

----

(source_file
    (assignment_expression
        location: (identifier)
        value: (subscript_expression
                   base: (postfix_expression
                               operand: (identifier)
                               (postfix_operator))
                   (subscriptor
                       (argument
                           value: (numeric_literal)))))
    (binary_expression
        lhs: (subscript_expression
                 base: (identifier)
                 (subscriptor
                     (argument value: (identifier))
                     (argument
                         label: (identifier)
                         value: (numeric_literal))))
        (binary_operator)
        rhs: (numeric_literal))
    (call_expression
        function: (identifier)
        (argument_list
            (argument
                value: (prefix_expression
                           (prefix_operator)
                           operand: (subscript_expression
                               base: (self)
                               (subscriptor
                                   (argument
                                       value: (identifier))))))))
    (constant_declaration
        name: (identifier)
        initializer: (subscript_expression
                         base: (identifier)
                         (subscriptor
                            (argument
                                value: (unbounded_range_expression)))))
    (constant_declaration
        name: (identifier)
        initializer: (subscript_expression
                         base: (identifier)
                         (subscriptor
                             (argument
                                 label: (identifier)
                                 value: (unbounded_range_expression))
                             (argument
                                 value: (numeric_literal))))))


================
Postfix expressions
================

foo.bar(baz: quux)!

let x = y%-=-*%

xyzzy© + plugh--

waldo>^<

----

(source_file
    (postfix_expression
        operand: (call_expression
                     function: (member_expression
                                   base: (identifier)
                                   member: (identifier))
                     (argument_list
                         (argument
                             label: (identifier)
                             value: (identifier))))
        (postfix_operator))
    (constant_declaration
        name: (identifier)
        initializer: (postfix_expression
                         operand: (identifier)
                         (postfix_operator)))
    (binary_expression
        lhs: (postfix_expression
                 operand: (identifier)
                 (postfix_operator))
        (binary_operator)
        rhs: (postfix_expression
                 operand: (identifier)
                 (postfix_operator)))
    (postfix_expression
        operand: (identifier)
        (postfix_operator)))


================
Prefix expressions
================

-(bar)

let x = ~y + ^--^z

if !quux, &%*-xyzzy {}

guard ¬plugh else { continue }

----

(source_file
    (prefix_expression
        (prefix_operator)
        operand: (parenthesized_expression
                     (identifier)))
    (constant_declaration
        name: (identifier)
        initializer: (binary_expression
            lhs: (prefix_expression
                     (prefix_operator)
                     operand: (identifier))
            (binary_operator)
            rhs: (prefix_expression
                     (prefix_operator)
                     operand: (identifier))))
    (if_statement
        condition: (prefix_expression
                       (prefix_operator)
                       operand: (identifier))
        condition: (prefix_expression
                       (prefix_operator)
                       operand: (identifier))
        body: (block))
    (guard_statement
        condition: (prefix_expression
                       (prefix_operator)
                       operand: (identifier))
        body: (block
                  (continue_statement))))


================
Operator references
================

let x = (<=)

foo(op: ^)

----

(source_file
    (constant_declaration
        name: (identifier)
        initializer: (parenthesized_expression
                         (operator_reference)))
    (call_expression
        function: (identifier)
        (argument_list
            (argument
                label: (identifier)
                value: (operator_reference)))))


================
Function references
================

let f = Foo<T.U>.bar(_:baz:)

foo(bar: βαζ(_:quux:waldo:), frobozz: fanucci)

list.map(String.init(_:))

let mumble = corge( grault: _: )

----

(source_file
    (constant_declaration
        name: (identifier)
        initializer: (member_expression
                         base: (type_expression
                                   (generic_type
                                       (generic_argument_list
                                           (simple_type
                                               (type_identifier)
                                               (type_identifier)))))
                         member: (function_reference
                                     name: (identifier)
                                     (function_reference_parameter_list
                                         parameter: (identifier)
                                         parameter: (identifier)))))
    (call_expression
        function: (identifier)
        (argument_list
            (argument
                label: (identifier)
                value: (function_reference
                           name: (identifier)
                           (function_reference_parameter_list
                               parameter: (identifier)
                               parameter: (identifier)
                               parameter: (identifier))))
            (argument
                label: (identifier)
                value: (identifier))))
     (call_expression
         function: (member_expression
                       base: (identifier)
                       member: (identifier))
         (argument_list
             (argument
                 value: (member_expression
                            base: (identifier)
                            member: (function_reference
                                        name: (identifier)
                                        (function_reference_parameter_list
                                            parameter: (identifier)))))))
     (constant_declaration
         name: (identifier)
         initializer: (function_reference
                           name: (identifier)
                           (function_reference_parameter_list
                               parameter: (identifier)
                               parameter: (identifier)))))


================
Type constructors
================

let foos = [Foo]()

let foosByBar = [Foo : Bar]()

let quux = Quux<Waldo, Frobozz>()

let plugh = Plugh<Xyzzy, Megaboz,>()

----

(source_file
    (constant_declaration
        name: (identifier)
        initializer: (call_expression
                         function: (type_expression
                                       (array_type
                                           element: (simple_type
                                                        (type_identifier))))
                         (argument_list)))
    (constant_declaration
        name: (identifier)
        initializer: (call_expression
                         function: (type_expression
                                       (dictionary_type
                                           key: (simple_type
                                                    (type_identifier))
                                           value: (simple_type
                                                      (type_identifier))))
                         (argument_list)))
    (constant_declaration
        name: (identifier)
        initializer:
            (call_expression
                function: (type_expression
                              (generic_type
                                  (generic_argument_list
                                      (simple_type (type_identifier))
                                      (simple_type (type_identifier)))))
                (argument_list)))
    (constant_declaration
        name: (identifier)
        initializer:
            (call_expression
                function: (type_expression
                              (generic_type
                                  (generic_argument_list
                                      (simple_type (type_identifier))
                                      (simple_type (type_identifier)))))
                (argument_list))))


================
Type member access
================

Bar<Baz>.quux

Waldo.Frobozz<T>.self

----

(source_file
    (member_expression
        base: (type_expression
                    (generic_type
                        (generic_argument_list
                            (simple_type (type_identifier)))))
        member: (identifier))
    (member_expression
        base: (member_expression
                    base: (identifier)
                    member: (type_expression
                                (generic_type
                                    (generic_argument_list
                                        (simple_type (type_identifier))))))
        member: (self)))


================
Ternary conditional
================

print(foo ? bar : baz())

func foo() {
    return bar.baz() ? .quux : .waldo
}

let foo = bar ? "a:b" : baz

----

(source_file
    (call_expression
        function: (identifier)
        (argument_list
            (argument
                value: (ternary_conditional_expression
                           condition: (identifier)
                           then: (identifier)
                           else: (call_expression
                                    function: (identifier)
                                    (argument_list))))))
    (function_declaration
        name: (identifier)
        (parameter_list)
        body: (block
        (return_statement
            value: (ternary_conditional_expression
                       condition: (call_expression
                                      function: (member_expression
                                                    base: (identifier)
                                                    member: (identifier))
                                      (argument_list))
                       then: (implicit_member_expression (identifier))
                       else: (implicit_member_expression (identifier))))))
    (constant_declaration
        name: (identifier)
        initializer: (ternary_conditional_expression
                         condition: (identifier)
                         then: (string_literal (string_text))
                         else: (identifier))))


================
Yield expression
================

yield &foo.bar

yield &quux

yield xyzzy

----

(source_file
    (yield_expression
        (prefix_expression
            (prefix_operator)
            operand: (member_expression
                         base: (identifier)
                         member: (identifier))))
    (yield_expression (prefix_expression
                          (prefix_operator)
                          operand: (identifier)))
    (yield_expression (identifier)))


================
If expressions
================

let foo = if bar {
        baz + quux
    } else {
        plugh
    }

foo = if bar {
        baz + quux
    } else {
        plugh
    }

var xyzzy: Xyzzy {
    return if frobozz {
               waldo
           } else {
               fanucci
           }
}

----

(source_file
    (constant_declaration
        name: (identifier)
        initializer: (if_expression
                         condition: (identifier)
                         body: (block
                                   (binary_expression
                                       lhs: (identifier)
                                       (binary_operator)
                                       rhs: (identifier)))
                         (else_statement
                             body: (block (identifier)))))
    (assignment_expression
        location: (identifier)
        value: (if_expression
                   condition: (identifier)
                   body: (block
                             (binary_expression
                                 lhs: (identifier)
                                 (binary_operator)
                                 rhs: (identifier)))
                   (else_statement
                       body: (block (identifier)))))
    (variable_declaration
        name: (identifier)
        type: (simple_type (type_identifier))
        implicit_getter: (block
                             (return_statement
                                 value: (if_expression
                                            condition: (identifier)
                                            body: (block (identifier))
                                            (else_statement
                                                body: (block (identifier))))))))


================
Switch expressions
================

let foo = switch bar {
        case .baz:
            quux
        default:
            plugh
    }

foo = switch bar {
        case .baz:
            quux
        default:
            plugh
    }

var xyzzy: Xyzzy {
    return switch frobozz {
            case .waldo:
                fanucci
            default:
                zipso
        }
}

----

(source_file
    (constant_declaration
        name: (identifier)
        initializer: (switch_expression
                         expression: (identifier)
                         (switch_case
                             (match_descriptor
                                 pattern: (member_pattern
                                              member: (identifier)))
                             (case_body (identifier)))
                         (switch_default
                             (case_body (identifier)))))
    (assignment_expression
        location: (identifier)
        value: (switch_expression
                   expression: (identifier)
                   (switch_case
                       (match_descriptor
                           pattern: (member_pattern
                                        member: (identifier)))
                       (case_body (identifier)))
                   (switch_default
                       (case_body (identifier)))))
    (variable_declaration
        name: (identifier)
        type: (simple_type (type_identifier))
        implicit_getter: (block
                             (return_statement
                                 value: (switch_expression
                                            expression: (identifier)
                                            (switch_case
                                                (match_descriptor pattern:
                                                    (member_pattern
                                                        member: (identifier)))
                                                (case_body (identifier)))
                                            (switch_default
                                                (case_body (identifier))))))))


================
Value parameter packs
================

foo(repeat each bar)

bar[repeat each quux]

let x = (repeat each plugh)

[repeat each frobozz]

[repeat each waldo, repeat each fanucci]

func foo<each T>(_ t: repeat each T) -> (repeat each T) {
    return (repeat each t)
}

----

(source_file
    (call_expression
        function: (identifier)
        (argument_list
            (argument
                value: (pack_expansion_expression
                           (pack_reference_expression (identifier))))))
    (subscript_expression
        base: (identifier)
        (subscriptor
            (argument
                value: (pack_expansion_expression
                           (pack_reference_expression
                               (identifier))))))
    (constant_declaration
        name: (identifier)
        initializer: (parenthesized_expression
                         (pack_expansion_expression
                             (pack_reference_expression (identifier)))))
    (array_literal
        (pack_expansion_expression
            (pack_reference_expression (identifier))))
    (array_literal
        (pack_expansion_expression
            (pack_reference_expression (identifier)))
        (pack_expansion_expression
            (pack_reference_expression (identifier))))
    (function_declaration
        name: (identifier)
        (generic_parameter_list
            (generic_parameter
                (parameter_pack
                    name: (type_identifier))))
        (parameter_list
            (parameter
                external_name: (identifier)
                local_name: (identifier)
                type: (pack_expansion_type
                          pattern_type: (pack_reference
                                            (simple_type (type_identifier))))))
        (function_result
            return_type: (parenthesized_type
                             (pack_expansion_type
                                pattern_type:
                                    (pack_reference
                                        (simple_type (type_identifier))))))
        body: (block
                  (return_statement
                      value: (parenthesized_expression
                                 (pack_expansion_expression
                                     (pack_reference_expression (identifier))))))))


================
Postfix #if expressions
================

foo
#if BAR
    .baz()
#else
    .quux()
#endif


plugh(
    frobozz
        .waldo()
    #if FANUCCI
        .megaboz()
    #endif
)

----

(source_file
    (identifier)
    (compiler_control_statement
        (compiler_condition (identifier))
        (compiler_condition_body
            (call_expression
                function: (implicit_member_expression (identifier))
                (argument_list)))
        (compiler_condition_body
            (call_expression
                function: (implicit_member_expression (identifier))
                (argument_list))))
    (call_expression
        function: (identifier)
        (argument_list
            (argument
                value: (member_expression
                           base: (call_expression
                                       function: (member_expression
                                                     base: (identifier)
                                                     member: (identifier))
                                       (argument_list))
                           member: (postfix_ifconfig
                                       (compiler_condition (identifier))
                                       (call_expression
                                           function: (identifier)
                                           (argument_list))))))))
