================
Protocol declaration
================

protocol P {}

protocol Q : AnyObject, Quux, Xyzzy {

}

protocol R {
    typealias Foo = Bar
}

protocol SinglePassSequence : Sequence where Element : Explosive {}

@_marker protocol Whatsis {}

----

(source_file
    (protocol_declaration
        name: (type_identifier)
        (protocol_body))
    (protocol_declaration
        name: (type_identifier)
        (inheritance_clause
            (simple_type (type_identifier))
            (simple_type (type_identifier))
            (simple_type (type_identifier)))
        (protocol_body))
    (protocol_declaration
        name: (type_identifier)
        (protocol_body
            (typealias
                name: (type_identifier)
                value: (simple_type (type_identifier)))))
    (protocol_declaration
        name: (type_identifier)
        (inheritance_clause
            (simple_type (type_identifier)))
        (generic_where_clause
            (generic_requirement
                type: (simple_type (type_identifier))
                constraint: (simple_type (type_identifier))))
        (protocol_body))
    (protocol_declaration
        (attribute name: (identifier))
        name: (type_identifier)
        (protocol_body)))


================
Protocol bodies; property requirements
================

protocol P {
    static var foo: Int { get }
    optional var bar: String { mutating get };
    var baz: Baz { get set }
}

protocol Q : AnyObject {
    static var xyzzy: Xyzzy { mutating get set }
    var quux: Quux { get nonmutating set }
}

protocol R {
    typealias Foo = Bar

    var plugh: Plugh { get async }
    var frobozz: Frobozz { get async throws }
    var waldo: Waldo { get throws(SquidError) }
}

----

(source_file
    (protocol_declaration
        name: (type_identifier)
        (protocol_body
            (property_requirement
                (protocol_member_modifiers)
                name: (identifier)
                property_type: (simple_type (type_identifier)))
            (property_requirement
                (protocol_member_modifiers)
                name: (identifier)
                property_type: (simple_type (type_identifier)))
            (property_requirement
                name: (identifier)
                property_type: (simple_type (type_identifier)))))
    (protocol_declaration
        name: (type_identifier)
        (inheritance_clause (simple_type (type_identifier)))
        (protocol_body
            (property_requirement
                (protocol_member_modifiers)
                name: (identifier)
                property_type: (simple_type (type_identifier)))
            (property_requirement
                name: (identifier)
                property_type: (simple_type (type_identifier)))))
    (protocol_declaration
        name: (type_identifier)
        (protocol_body
            (typealias
                name: (type_identifier)
                value: (simple_type (type_identifier)))
            (property_requirement
                name: (identifier)
                property_type: (simple_type (type_identifier))
                (async_effect))
            (property_requirement
                name: (identifier)
                property_type: (simple_type (type_identifier))
                (async_effect)
                (throw_effect))
            (property_requirement
                name: (identifier)
                property_type: (simple_type
                                   (type_identifier))
                (throw_effect
                    error_type: (simple_type
                                    (type_identifier)))))))


================
Protocol bodies; method/init requirements
================

protocol R {
    static func makeMeASandwich()
    func immanentize_the_eschaton(quickly: Bool) throws;
    mutating func doThatThing()
    func makeABox(_: Dimension) throws(WoodError) -> Box
}

protocol S {
    typealias Foo = Bar
    init<C : Cheese>(cheese: C, condiments: Condiments) where C.Milk == Cow
    func makeMeASandwich<Bread>() where Bread : WholeGrain
}

----

(source_file
    (protocol_declaration
        name: (type_identifier)
        (protocol_body
            (method_requirement
                (protocol_member_modifiers)
                name: (identifier)
                (parameter_list))
            (method_requirement
                name: (identifier)
                (parameter_list
                    (parameter
                        local_name: (identifier)
                        type: (simple_type (type_identifier))))
                (function_result
                    (throw_effect)))
            (method_requirement
                (protocol_member_modifiers)
                name: (identifier)
                (parameter_list))
            (method_requirement
                name: (identifier)
                (parameter_list
                    (parameter
                        local_name: (identifier)
                        type: (simple_type
                                  (type_identifier))))
                (function_result
                    (throw_effect
                        error_type: (simple_type
                                        (type_identifier)))
                    return_type: (simple_type
                                     (type_identifier))))))
    (protocol_declaration
        name: (type_identifier)
        (protocol_body
            (typealias
                name: (type_identifier)
                value: (simple_type (type_identifier)))
            (init_requirement
                (generic_parameter_list
                    (generic_parameter
                        name: (type_identifier)
                        requirement: (simple_type (type_identifier))))
                (parameter_list
                    (parameter
                        local_name: (identifier)
                        type: (simple_type (type_identifier)))
                    (parameter
                        local_name: (identifier)
                        type: (simple_type (type_identifier))))
                (generic_where_clause
                    (generic_requirement
                        type: (simple_type
                                  (type_identifier)
                                  (type_identifier))
                        constraint: (simple_type (type_identifier)))))
                (method_requirement
                    name: (identifier)
                    (generic_parameter_list
                        (generic_parameter
                            name: (type_identifier)))
                    (parameter_list)
                    (generic_where_clause
                        (generic_requirement
                            type: (simple_type (type_identifier))
                            constraint: (simple_type (type_identifier))))))))


================
Protocol bodies; associatedtype
================

protocol T {
    associatedtype Foo
    associatedtype Bar<T> where T : Hashable;
    associatedtype Baz : Quux = Waldo
}

----

(source_file
    (protocol_declaration
        name: (type_identifier)
        (protocol_body
            (associatedtype
                name: (type_identifier))
            (associatedtype
                name: (type_identifier)
                (generic_parameter_list
                    (generic_parameter
                        name: (type_identifier)))
                (generic_where_clause
                    (generic_requirement
                        type: (simple_type (type_identifier))
                        constraint: (simple_type (type_identifier)))))
            (associatedtype
                name: (type_identifier)
                constraints: (inheritance_clause
                                 (simple_type (type_identifier)))
                default_value: (simple_type (type_identifier))))))


================
Protocol bodies; subscripts
================

protocol P {
    subscript<T : Equatable, U>(key key: T) -> U?
}

----

(source_file
    (protocol_declaration
        name: (type_identifier)
        (protocol_body
            (subscript_requirement
                (generic_parameter_list
                    (generic_parameter
                        name: (type_identifier)
                        requirement: (simple_type (type_identifier)))
                    (generic_parameter
                        name: (type_identifier)))
                (parameter_list
                    (parameter
                        external_name: (identifier)
                        local_name: (identifier)
                        type: (simple_type (type_identifier))))
                return_type: (optional_type
                                 wrapped: (simple_type
                                              (type_identifier)))))))


================
Protocol bodies; operator requirements
================

protocol P {
    static func ^=(lhs: Self, rhs: Self) -> Bool
}

----

(source_file
    (protocol_declaration
        name: (type_identifier)
        (protocol_body
            (operator_requirement
                (protocol_member_modifiers)
                name: (operator_name)
                (parameter_list
                    (parameter
                        local_name: (identifier)
                        type: (simple_type (type_identifier)))
                    (parameter
                        local_name: (identifier)
                        type: (simple_type (type_identifier))))
                (function_result
                    return_type: (simple_type (type_identifier)))))))


================
Protocols with "primary" associated types
================

protocol Foo<Bar> {
    associatedtype Bar
}

protocol Baz<Quux, Plugh> {
    associatedtype Quux
    associatedtype Plugh : Frobozz
}

----

(source_file
    (protocol_declaration
        name: (type_identifier)
        (generic_parameter_list
            (generic_parameter
                name: (type_identifier)))
        (protocol_body
            (associatedtype
                name: (type_identifier))))
    (protocol_declaration
        name: (type_identifier)
        (generic_parameter_list
            (generic_parameter
                name: (type_identifier))
            (generic_parameter
                name: (type_identifier)))
            (protocol_body
                (associatedtype
                    name: (type_identifier))
                (associatedtype
                    name: (type_identifier)
                    constraints: (inheritance_clause
                                     (simple_type (type_identifier)))))))
