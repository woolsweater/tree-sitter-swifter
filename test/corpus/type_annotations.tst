================
Tuple types
================

let x: (Int, quickly: Bool)

func handle() -> (Handle, priority: Priority) {}

func recycle(handle: (Handle, priority: Priority)) -> Bool {

}

----

(source_file
    (constant_declaration
        name: (identifier)
        type: (tuple_type
                  (simple_type (type_identifier))
                  label: (identifier) (simple_type (type_identifier))))
    (function_declaration
        name: (identifier)
        (parameter_list)
        (function_result
            return_type: (tuple_type
                             (simple_type (type_identifier))
                             label: (identifier) (simple_type
                                                     (type_identifier))))
        body: (block))
    (function_declaration
        name: (identifier)
        (parameter_list
            (parameter
                local_name: (identifier)
                type: (tuple_type
                          (simple_type (type_identifier))
                          label: (identifier) (simple_type
                                                  (type_identifier)))))
        (function_result
            return_type: (simple_type (type_identifier)))
        body: (block)))


================
Void type
================

func nothing() -> () {}

var _: ()

func takesVoid(_ void: ()) {}

----

(source_file
    (function_declaration
        name: (identifier)
        (parameter_list)
        (function_result return_type: (void_type))
        body: (block))
    (variable_declaration
        name: (identifier)
        type: (void_type))
    (function_declaration
        name: (identifier)
        (parameter_list
            (parameter
                external_name: (identifier)
                local_name: (identifier)
                type: (void_type)))
        body: (block)))


================
Optionals (incl. IUO)
================

let x: Int???

var foo: Bar.Quux!

func maybeNot(arg: Arg?) -> Value? {}

----

(source_file
    (constant_declaration
        name: (identifier)
        type: (optional_type
                  wrapped: (optional_type
                      wrapped: (optional_type
                          wrapped: (simple_type
                                       (type_identifier))))))
    (variable_declaration
        name: (identifier)
        type: (iuo_type
                  wrapped: (simple_type
                               (type_identifier)
                               (type_identifier))))
    (function_declaration
        name: (identifier)
        (parameter_list
            (parameter
                local_name: (identifier)
                type: (optional_type
                          wrapped: (simple_type (type_identifier)))))
        (function_result
            return_type: (optional_type
                             wrapped: (simple_type (type_identifier))))
        body: (block)))


================
Array type sugar
================

let colors: [Color]

func process<T>(list: [T]) -> [Any] {}

----

(source_file
    (constant_declaration
        name: (identifier)
        type: (array_type
                  element: (simple_type (type_identifier))))
    (function_declaration
        name: (identifier)
        (generic_parameter_list
            (generic_parameter name: (type_identifier)))
        (parameter_list
            (parameter
                local_name: (identifier)
                type: (array_type
                          element: (simple_type (type_identifier)))))
        (function_result
            return_type: (array_type
                             element: (simple_type (type_identifier))))
        body: (block)))


================
Dictionary type sugar
================

var counts: [Foo.Bar : Int]

func reverse<T, U>(map: [T : U]) -> [U : T] {}

----

(source_file
    (variable_declaration
        name: (identifier)
        type: (dictionary_type
                  key: (simple_type
                           (type_identifier)
                           (type_identifier))
            value: (simple_type (type_identifier))))
    (function_declaration
        name: (identifier)
        (generic_parameter_list
            (generic_parameter
                name: (type_identifier))
            (generic_parameter
                name: (type_identifier)))
        (parameter_list
            (parameter
                local_name: (identifier)
                type: (dictionary_type
                          key: (simple_type (type_identifier))
                          value: (simple_type (type_identifier)))))
       (function_result
           return_type: (dictionary_type
                            key: (simple_type (type_identifier))
                            value: (simple_type (type_identifier))))
       body: (block)))


================
Generic arguments
================

let d: Dictionary<Foo, Bar>

func keys<T, U>(map: Map<T, U>) -> Array<T> {}

let t: Foo<@SomeActor () -> Int>

let x: Optional<UnsafePointer<Foo<Bar<Baz>>>>

let ds: Quux<[Plugh : Frobozz]>

let emptyVariadic: V<>

----

(source_file
    (constant_declaration
        name: (identifier)
        type: (simple_type
                  (generic_type
                      (generic_argument_list
                          (simple_type (type_identifier))
                          (simple_type (type_identifier))))))
    (function_declaration
        name: (identifier)
        (generic_parameter_list
            (generic_parameter
                name: (type_identifier))
            (generic_parameter
                name: (type_identifier)))
        (parameter_list
            (parameter
                local_name: (identifier)
                type: (simple_type
                          (generic_type
                              (generic_argument_list
                                  (simple_type (type_identifier))
                                  (simple_type (type_identifier)))))))
        (function_result
            return_type: (simple_type
                             (generic_type
                                 (generic_argument_list
                                     (simple_type (type_identifier))))))
            body: (block))
    (constant_declaration
        name: (identifier)
        type: (simple_type
                  (generic_type
                      (generic_argument_list
                          (function_type
                              (attribute name: (identifier))
                              (parameter_list)
                              (function_result
                                  return_type: (simple_type
                                                   (type_identifier))))))))
    (constant_declaration
        name: (identifier)
        type:
        (simple_type
            (generic_type
                (generic_argument_list
                    (simple_type
                        (generic_type
                            (generic_argument_list
                                (simple_type
                                    (generic_type
                                        (generic_argument_list
                                            (simple_type
                                                (generic_type
                                                    (generic_argument_list
                                                      (simple_type
                                                          (type_identifier)))))))))))))))
    (constant_declaration
        name: (identifier)
        type: (simple_type
                  (generic_type
                      (generic_argument_list
                          (dictionary_type
                              key: (simple_type
                                       (type_identifier))
                              value: (simple_type
                                         (type_identifier)))))))
    (constant_declaration
        name: (identifier)
        type: (simple_type
                  (generic_type (generic_argument_list)))))


================
Function types
================

let x: (Int) -> Void

let curry: (_ one: A) -> (_ two: B) async -> C

func doThatThing(_ action: () -> Void) {}

func makeABox(complete: @escaping (_ success: Bool) -> Void) {}

func compactMap<T>(_ transform: (Element) throws -> T?) rethrows -> [T] {}

let callback: @MainActor (Int) -> Int

let waldo: () throws(Frobozz) -> Magic

let fns: Array<@convention(c) @Sendable () -> Void>

----

(source_file
    (constant_declaration
        name: (identifier)
        type: (function_type
                  (parameter_list
                      (parameter
                          type: (simple_type (type_identifier))))
                  (function_result
                      return_type: (simple_type (type_identifier)))))
    (constant_declaration
        name: (identifier)
        type: (function_type
                  (parameter_list
                      (parameter
                          external_name: (identifier)
                          local_name: (identifier)
                          type: (simple_type (type_identifier))))
                  (function_result
                      return_type:
                          (function_type
                             (parameter_list
                                 (parameter
                                     external_name: (identifier)
                                     local_name: (identifier)
                                     type: (simple_type (type_identifier))))
                             (function_result
                                 (async_effect)
                                 return_type: (simple_type
                                                  (type_identifier)))))))
    (function_declaration
        name: (identifier)
        (parameter_list
            (parameter
                external_name: (identifier)
                local_name: (identifier)
                type: (function_type
                          (parameter_list)
                          (function_result
                              return_type: (simple_type (type_identifier))))))
         body: (block))
    (function_declaration
        name: (identifier)
        (parameter_list
            (parameter
                local_name: (identifier)
                (type_attributes
                    (type_attribute))
                type: (function_type
                          (parameter_list
                              (parameter
                                  external_name: (identifier)
                                  local_name: (identifier)
                                  type: (simple_type (type_identifier))))
                          (function_result
                              return_type: (simple_type (type_identifier))))))
        body: (block))
    (function_declaration
        name: (identifier)
        (generic_parameter_list
            (generic_parameter
                name: (type_identifier)))
        (parameter_list
            (parameter
                external_name: (identifier)
                local_name: (identifier)
                type: (function_type
                          (parameter_list
                              (parameter
                                  type: (simple_type (type_identifier))))
                          (function_result
                              (throw_effect)
                              return_type:
                                  (optional_type
                                      wrapped: (simple_type
                                                   (type_identifier)))))))
        (function_result
            (throw_effect)
            return_type: (array_type
                             element: (simple_type (type_identifier))))
        body: (block))
    (constant_declaration
        name: (identifier)
        type: (function_type
                  (attribute name: (identifier))
                  (parameter_list
                      (parameter
                          type: (simple_type (type_identifier))))
                  (function_result
                      return_type: (simple_type (type_identifier)))))
    (constant_declaration
        name: (identifier)
        type: (function_type
                  (parameter_list)
                  (function_result
                      (throw_effect
                          error_type: (simple_type
                                          (type_identifier)))
                      return_type: (simple_type
                                       (type_identifier)))))
    (constant_declaration
        name: (identifier)
        type: (simple_type
                  (generic_type
                      (generic_argument_list
                          (type_attributes
                              (type_attribute)
                              (type_attribute))
                          (function_type
                              (parameter_list)
                              (function_result
                                  return_type: (simple_type
                                                   (type_identifier)))))))))


================
Opaque types
================

func makeABox() -> some Box {}

class C {
    var foo: some Bar & Baz { _myFoo }
}

func doThatThing() -> (some X)? {}

func foo(bar: some Bar, quux: some Quux) {}

func plugh(_: some ~Waldo) {}

----

(source_file
    (function_declaration
        name: (identifier)
        (parameter_list)
        (function_result
            return_type: (opaque_type (type_identifier)))
        body: (block))
    (class_declaration
        name: (type_identifier)
        (type_body
            (variable_property_declaration
                name: (identifier)
                type: (opaque_type
                          (protocol_composition_type
                              (simple_type (type_identifier))
                              (simple_type (type_identifier))))
                implicit_getter: (block (identifier)))))
    (function_declaration
        name: (identifier)
        (parameter_list)
        (function_result
            return_type: (optional_type
                            wrapped: (parenthesized_type
                                         (opaque_type
                                             (type_identifier)))))
        body: (block))
    (function_declaration
        name: (identifier)
        (parameter_list
            (parameter
                local_name: (identifier)
                type: (opaque_type
                          (type_identifier)))
            (parameter
                local_name: (identifier)
                type: (opaque_type
                          (type_identifier))))
        body: (block))
    (function_declaration
        name: (identifier)
        (parameter_list
            (parameter
                local_name: (identifier)
                type: (opaque_type
                          (protocol_nonconformance
                              (simple_type (type_identifier))))))
        body: (block)))


================
Parenthesized types
================

let x: (Int)

func foo(bar: (Bar)) -> (Baz)? { return nil }

----

(source_file
    (constant_declaration
        name: (identifier)
        type: (parenthesized_type
                  (simple_type (type_identifier))))
    (function_declaration
        name: (identifier)
        (parameter_list
            (parameter
                local_name: (identifier)
                type: (parenthesized_type
                          (simple_type (type_identifier)))))
        (function_result
            return_type: (optional_type
                             wrapped: (parenthesized_type
                                          (simple_type (type_identifier)))))
        body: (block
                  (return_statement value: (nil)))))


================
Type placeholders
================

let list: Array<_> = [1, 2]

let publisher = Just<_>(0)

----

(source_file
    (constant_declaration
        name: (identifier)
        type: (simple_type
                  (generic_type
                      (generic_argument_list
                           (simple_type (type_identifier)))))
        initializer: (array_literal
                         (numeric_literal)
                         (numeric_literal)))
    (constant_declaration
        name: (identifier)
        initializer: (call_expression
                         function: (type_expression
                                       (generic_type
                                           (generic_argument_list
                                               (simple_type
                                                   (type_identifier)))))
                         (argument_list
                             (argument value: (numeric_literal))))))


================
Existential any
================

func foo() -> any P {}

func bar(_: any (P & Q)) {}

let x: (any P).Type = (any P).self

func foo(bar: (any Baz)?) {}

func quux(_: any ~Plugh) {}

----

(source_file
    (function_declaration
        name: (identifier)
        (parameter_list)
        (function_result
            return_type: (existential_type
                             (type_identifier)))
        body: (block))
    (function_declaration
        name: (identifier)
        (parameter_list
            (parameter
                local_name: (identifier)
                type: (existential_type
                          (protocol_composition_type
                              (simple_type (type_identifier))
                              (simple_type (type_identifier))))))
        body: (block))
    (constant_declaration
        name: (identifier)
        type: (existential_type (type_identifier))
        initializer: (member_expression
                         base: (type_expression
                                   (existential_type (type_identifier)))
        member: (self)))
    (function_declaration
        name: (identifier)
        (parameter_list
            (parameter
                local_name: (identifier)
                type: (existential_type (type_identifier))))
        body: (block))
    (function_declaration
        name: (identifier)
        (parameter_list
            (parameter
                local_name: (identifier)
                type: (existential_type
                          (protocol_nonconformance
                              (simple_type (type_identifier))))))
        body: (block)))
