================
Basic regex literals
================

let foo = /xyzzy/

let bar = /\w{1,3}/

baz(/\d+/)

let quux = /(?<identifier>[[:alpha:]]\w*) = (?<hex>[0-9A-F]+)/

----

(source_file
    (constant_declaration
        name: (identifier)
        initializer: (regex_literal))
    (constant_declaration
        name: (identifier)
        initializer: (regex_literal))
    (call_expression
        function: (identifier)
        (argument_list
            (argument
                value: (regex_literal))))
    (constant_declaration
        name: (identifier)
        initializer: (regex_literal)))


================
Extended regex literals
================

let foo = #/usr/lib/modules/([^/]+)/vmlinuz/#

let bar = #/ [+-] /#

----

(source_file
    (constant_declaration
        name: (identifier)
        initializer: (regex_literal))
    (constant_declaration
        name: (identifier)
        initializer: (regex_literal)))


================
Multiline regex literals
================

let foo = #/
  usr/lib/modules/ # Prefix
  (?<subpath> [^/]+)
  /vmlinuz          # The kernel
/#

let bar = #/
  # Match a line of the format e.g "DEBIT  03/03/2022  Totally Legit Shell Corp  $2,000,000.00"
  (?<kind>    \w+)                \s\s+
  (?<date>    \S+)                \s\s+
  (?<account> (?: (?!\s\s) . )+)  \s\s+ # Note that account names may contain spaces.
  (?<amount>  .*)
/#

----

(source_file
    (constant_declaration
        name: (identifier)
        initializer: (regex_literal))
    (constant_declaration
        name: (identifier)
        initializer: (regex_literal)))
