================
For loops
================

for _ in countToTen() {
   makeABox();
}

for (i, foo: item) in items.enumerated() {
    stash(item, at: i);
}

for case let .foo(bar) in baz {}

----

(source_file
    (for_in_statement
        pattern: (identifier)
        sequence: (call_expression
                      function: (identifier)
                      (argument_list))
        body: (block
                  (call_expression
                      function: (identifier)
                      (argument_list))))
    (for_in_statement
        pattern: (tuple_pattern
                     (tuple_element (identifier))
                     (tuple_element
                         label: (identifier)
                         (identifier)))
        sequence: (call_expression
                      function: (member_expression
                                    base: (identifier)
                                    member: (identifier))
                      (argument_list))
        body: (block
                  (call_expression
                      function: (identifier)
                      (argument_list
                          (argument value: (identifier))
                          (argument
                              label: (identifier)
                              value: (identifier))))))
    (for_in_statement
        pattern: (binding_match_pattern
                     binding: (member_pattern
                                  member: (identifier)
                                  (assoc_value_patterns (identifier))))
        sequence: (identifier)
        body: (block)))

================
For loop with where clause
================

for x in list where foo(x) {}

----

(source_file
    (for_in_statement
        pattern: (identifier)
        sequence: (identifier)
        (where_clause
            (call_expression
                function: (identifier)
                (argument_list (argument value: (identifier)))))
        body: (block)))


================
Async for loops
================

for try await line in seq.lines() {}

for await foo in bar {}

----

(source_file
    (for_in_statement
        pattern: (async_sequence_pattern
                     binding: (identifier))
                     sequence: (call_expression
                                   function: (member_expression
                                                 base: (identifier)
                                                 member: (identifier))
                                   (argument_list))
        body: (block))
    (for_in_statement
        pattern: (async_sequence_pattern
                     binding: (identifier))
        sequence: (identifier)
        body: (block)))


================
Pack iteration
================

for element in repeat each element {}

for _ in repeat Foo<each Bar>() {}

for _ in repeat baz(each quux) {}

for (left, right) in repeat (each lhs, each rhs) {}

----

(source_file
    (for_in_statement
        pattern: (identifier)
        sequence: (pack_expansion_expression
            (pack_reference_expression (identifier)))
        body: (block))
    (for_in_statement
        pattern: (identifier)
        sequence: (pack_expansion_expression
            (call_expression
                function: (type_expression
                              (generic_type
                                  (generic_argument_list
                                      (pack_reference
                                          (simple_type (type_identifier))))))
                (argument_list)))
        body: (block))
    (for_in_statement
        pattern: (identifier)
        sequence: (pack_expansion_expression
            (call_expression
                function: (identifier)
                (argument_list
                    (argument
                        value: (pack_reference_expression
                                   (identifier))))))
        body: (block))
    (for_in_statement
        pattern: (tuple_pattern
            (tuple_element (identifier))
            (tuple_element (identifier)))
        sequence: (pack_expansion_expression
            (tuple_expression
                (pack_reference_expression (identifier))
                (pack_reference_expression (identifier))))
        body: (block)))


================
While statements
================

while foo.bar {
    let x = lookup();
    print(x);
}

while false {}

while let item = iter.next() {
    print(item);
}

while case .foo(let bar) = baz, bar > 10 {}

----

(source_file
    (while_statement
        condition: (member_expression
                       base: (identifier)
                       member: (identifier))
        body: (block
                  (constant_declaration
                      name: (identifier)
                      initializer: (call_expression
                                       function: (identifier)
                                       (argument_list)))
                  (call_expression
                       function: (identifier)
                       (argument_list (argument value: (identifier))))))
    (while_statement
        condition: (boolean_literal)
        body: (block))
    (while_statement
        condition: (optional_binding_condition
                       name: (identifier)
                       (initializer
                       (call_expression
                           function: (member_expression
                                         base: (identifier)
                                         member: (identifier))
                           (argument_list))))
        body: (block
                  (call_expression
                      function: (identifier)
                      (argument_list
                          (argument value: (identifier))))))
    (while_statement
        condition: (case_condition
                       pattern: (member_pattern
                                    member: (identifier)
                                    (assoc_value_patterns
                                        (binding_match_pattern
                                            binding: (identifier))))
                       value: (identifier))
        condition: (binary_expression
                       lhs: (identifier)
                       (binary_operator)
                       rhs: (numeric_literal))
        body: (block)))


================
Repeat-while statements
================

repeat {
    print(i);
} while i

----

(source_file
    (repeat_while_statement
        body: (block
            (call_expression
                function: (identifier)
                (argument_list
                    (argument value: (identifier)))))
        condition: (identifier)))


================
Labelled loops
================

foo: for i in list {}

bar: repeat {

} while something

baz: while somethingElse {}

`class`: repeat {
} while foo

----

(source_file
    (labelled_statement
        (statement_label (identifier))
        (for_in_statement
            pattern: (identifier)
            sequence: (identifier)
            body: (block)))
    (labelled_statement
        (statement_label (identifier))
        (repeat_while_statement
            body: (block)
            condition: (identifier)))
    (labelled_statement
        (statement_label (identifier))
        (while_statement
            condition: (identifier)
            body: (block)))
    (labelled_statement
        (statement_label (escaped_identifier))
        (repeat_while_statement
            body: (block)
            condition: (identifier))))
