================
Line comments
================

// This is a comment

/// Also a comment, which will be
///
/// continued but the parser doesn't know that.

//// Still a // comment here //

----

(source_file
    (line_comment)
    (line_comment)
    (line_comment)
    (line_comment)
    (line_comment))


================
Block comments
================

/* This is a comment */

/**
 And a comment here
 - note: This is actually a doc comment!
 */

/**
 * Also valid as a comment
 *
 * // This line comment does not count
 *
 * - remark: So many asterisks!
 */

/**
 /* This comment has *nesting* //
    /*
     and that's /okay/ */
  */
 */

----

(source_file
    (block_comment)
    (block_comment)
    (block_comment)
    (block_comment))


================
Comments and automatic semicolons
================

struct Foo {
    var bar: Bar

    /** This should not be inside the property decl. */
    func baz() {}
}

struct Foo {
    var bar: Bar

    /// This should not be inside the property decl.
    func baz() {}
}

struct Foo {
    var bar: Bar

    // Adjacent comments
    /// should count as one for
    /*  purposes of */
    /// automatic semicolon
    /**
     calculation
    */
    func baz() {}
}


let x = a.b
    // Comments should not break up a member expression
    .c
    /**
     ipsum

     dolor sit
     */
    .d
    /// amet
    ///
    /// consecutur
    .e
    /* adipiscing */
    .f

foo(bar: bar) /* Or a trailing closure */ {
  return quux
}

----

(source_file
    (struct_declaration
        name: (type_identifier)
        (type_body
            (variable_property_declaration
                name: (identifier)
                type: (simple_type (type_identifier)))
            (block_comment)
            (method_declaration
                name: (identifier)
                (parameter_list)
                body: (block))))
    (struct_declaration
        name: (type_identifier)
        (type_body
            (variable_property_declaration
                name: (identifier)
                type: (simple_type (type_identifier)))
            (line_comment)
            (method_declaration
                name: (identifier)
                (parameter_list)
                body: (block))))
    (struct_declaration
        name: (type_identifier)
        (type_body
            (variable_property_declaration
                name: (identifier)
                type: (simple_type (type_identifier)))
            (line_comment)
            (line_comment)
            (block_comment)
            (line_comment)
            (block_comment)
            (method_declaration
                name: (identifier)
                (parameter_list)
                body: (block))))
    (constant_declaration
        name: (identifier)
        initializer: (member_expression
                         base: (member_expression
                                   base: (member_expression
                                             base: (member_expression
                                                       base:
                                                       (member_expression
                                                           base: (identifier)
                                                           member: (identifier))
                                                       (line_comment)
                                                       member: (identifier))
                                             (block_comment)
                                             member: (identifier))
                                   (line_comment)
                                   (line_comment)
                                   (line_comment)
                                   member: (identifier))
                         (block_comment)
                         member: (identifier)))
    (call_expression
        function: (identifier)
        (argument_list
            (argument
                label: (identifier)
                value: (identifier)))
        (block_comment)
        (trailing_closures
            (closure_expression
                (body
                    (return_statement value: (identifier)))))))
