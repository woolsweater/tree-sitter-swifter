================
Import statement
================

import Foundation

import _SecretModule;

@testable import TestModule123

@preconcurrency import Quux

import some_c_library

import Foo.Bar.Baz

import `foo/bar/baz/quux`

----

(source_file
    (import_statement
        (import_path module_name: (identifier)))
    (import_statement
        (import_path module_name: (identifier)))
    (import_statement
        (attribute name: (identifier))
        (import_path module_name: (identifier)))
    (import_statement
        (attribute name: (identifier))
        (import_path module_name: (identifier)))
    (import_statement
        (import_path module_name: (identifier)))
    (import_statement
        (import_path
            module_name: (identifier)
            submodule_name: (identifier)
            submodule_name: (identifier)))
    (import_statement
        (import_path
            module_name: (escaped_identifier))))


================
Import kinds
================

import class Foundation.FileManager
import actor Foo.Bar
@_exported import struct Baz.Quux.Plugh

----

(source_file
    (import_statement
        (import_kind)
        (import_path
            module_name: (identifier)
            submodule_name: (identifier)))
    (import_statement
        (import_kind)
        (import_path
            module_name: (identifier)
            submodule_name: (identifier)))
    (import_statement
        (attribute name: (identifier))
        (import_kind)
        (import_path
            module_name: (identifier)
            submodule_name: (identifier)
            submodule_name: (identifier))))


================
Import access modifiers
================

public import Foo

@usableFromInline
package import Bar

internal import Baz

fileprivate import Quux.Plugh

private
import Waldo.Frobozz

----

(source_file
    (import_statement
        (access_level_modifier)
        (import_path
            module_name: (identifier)))
    (import_statement
        (attribute name: (identifier))
        (access_level_modifier)
        (import_path
            module_name: (identifier)))
    (import_statement
        (access_level_modifier)
        (import_path
            module_name: (identifier)))
    (import_statement
        (access_level_modifier)
        (import_path
            module_name: (identifier)
            submodule_name: (identifier)))
    (import_statement
        (access_level_modifier)
        (import_path
            module_name: (identifier)
            submodule_name: (identifier))))


================
Control statements
================

break

break outerLoop

return 0xefaaa9

continue

fallthrough

return x;

throw SomeError()

----

(source_file
    (break_statement)
    (break_statement label: (identifier))
    (return_statement value: (numeric_literal))
    (continue_statement)
    (fallthrough_statement)
    (return_statement value: (identifier))
    (throw_statement
        value: (call_expression
                   function: (identifier)
                   (argument_list))))


================
Return statements
================

func foo() {
    return
}

func bar() -> (Bar) -> Void {
    return { (bar) in
    
    }
}

func quux() -> () -> Void {
    return
        {
        
        }
}

func plugh() -> String {
    if x {
        return "Hello, world"
    }

    return
        "Goodnight, moon"
}

func xyzzy() -> Int {
    return

    10 + 42
}

func waldo() -> (Bool, Int) {
    return (true, 42)
}

func frobozz() -> (Bool, Int) {
    return

        (false, 51)
}

func mumble() -> Bool {
    if corge {
        return !grault
    } else  {
        return
            <%fred
    }
}

func megaboz() -> [Bool] {
    return
      [
          false,
          true,
      ]
}

func thud() -> (Int, Int) -> Int {
    return +
}

----

(source_file
    (function_declaration
        name: (identifier)
        (parameter_list)
        body: (block (return_statement)))
    (function_declaration
        name: (identifier)
        (parameter_list)
        (function_result
            return_type: (function_type
                             (parameter_list
                                 (parameter
                                     type: (simple_type (type_identifier))))
                             (function_result
                                 return_type: (simple_type (type_identifier)))))
        body: (block
                  (return_statement
                      value: (closure_expression
                                 (parameter_list
                                     (parameter name: (identifier)))))))
    (function_declaration
        name: (identifier)
        (parameter_list)
        (function_result
            return_type: (function_type
                             (parameter_list)
                             (function_result
                                 return_type: (simple_type
                                                  (type_identifier)))))
         body: (block
                   (return_statement
                       value: (closure_expression))))
    (function_declaration
        name: (identifier)
        (parameter_list)
        (function_result
            return_type: (simple_type (type_identifier)))
        body: (block
                  (if_statement
                      condition: (identifier)
                      body: (block
                                (return_statement
                                    value: (string_literal (string_text)))))
                  (return_statement
                      value: (string_literal (string_text)))))
    (function_declaration
        name: (identifier)
        (parameter_list)
        (function_result
            return_type: (simple_type (type_identifier)))
        body: (block
                  (return_statement
                        value: (binary_expression
                                   lhs: (numeric_literal)
                                   (binary_operator)
                                   rhs: (numeric_literal)))))
    (function_declaration
        name: (identifier)
        (parameter_list)
        (function_result
            return_type: (tuple_type
                             (simple_type (type_identifier))
                             (simple_type (type_identifier))))
        body: (block
                  (return_statement
                      value: (tuple_expression
                                 (boolean_literal)
                                 (numeric_literal)))))
    (function_declaration
        name: (identifier)
        (parameter_list)
        (function_result
            return_type: (tuple_type
                             (simple_type (type_identifier))
                             (simple_type (type_identifier))))
        body: (block
                  (return_statement
                      value: (tuple_expression
                                 (boolean_literal)
                                 (numeric_literal)))))
    (function_declaration
        name: (identifier)
        (parameter_list)
        (function_result
            return_type: (simple_type (type_identifier)))
        body: (block
                  (if_statement
                      condition: (identifier)
                      body: (block
                                (return_statement
                                    value: (prefix_expression
                                               (prefix_operator)
                                               operand: (identifier))))
                      (else_statement
                          body: (block
                                    (return_statement
                                        value: (prefix_expression
                                                   (prefix_operator)
                                                   operand: (identifier))))))))
    (function_declaration
        name: (identifier)
        (parameter_list)
        (function_result
            return_type: (array_type
                             element: (simple_type
                                          (type_identifier))))
        body: (block
                  (return_statement
                      value: (array_literal
                                 (boolean_literal)
                                 (boolean_literal)))))
    (function_declaration
        name: (identifier)
        (parameter_list)
        (function_result
            return_type: (function_type
               (parameter_list
                   (parameter
                       type: (simple_type
                                 (type_identifier)))
                   (parameter
                       type: (simple_type
                                 (type_identifier))))
            (function_result
                return_type: (simple_type
                                 (type_identifier)))))
        body: (block
                  (return_statement
                      value: (operator_reference)))))


================
Guard statements
================

guard flag else {
    return nil
}

guard (somethingWentWrong) else {
    fatalError()
}

guard let foo = bar.baz() else {
    return
}

guard let foo else { return }

----

(source_file
    (guard_statement
        condition: (identifier)
        body: (block
            (return_statement value: (nil))))
    (guard_statement
        condition: (parenthesized_expression
                       (identifier))
        body: (block
            (call_expression
                function: (identifier)
                (argument_list))))
    (guard_statement
        condition: (optional_binding_condition
            name: (identifier)
            (initializer
                (call_expression
                    function: (member_expression
                                  base: (identifier)
                                  member: (identifier))
                    (argument_list))))
            body: (block
                      (return_statement)))
    (guard_statement
        condition: (optional_shadowing_condition
                       name: (identifier))
        body: (block
                  (return_statement))))


================
If statements
================

if foo, bar {
   print(baz)
}

if (false) {
    doThatThing()
}
else if (wunderBar) {
    doSomethingElse()
}
else {
    makeABox()
}

if let foo = bar {
   makeABox(foo)
}

if let foo {
    doThatThing(foo)
}

----

(source_file
    (if_statement
        condition: (identifier)
        condition: (identifier)
        body: (block
            (call_expression
                function: (identifier)
                (argument_list
                    (argument value: (identifier))))))
    (if_statement
        condition: (parenthesized_expression
                       (boolean_literal))
        body: (block
            (call_expression
                function: (identifier)
                (argument_list)))
        (else_if_statement
            condition: (parenthesized_expression
                           (identifier))
            body: (block
                (call_expression
                    function: (identifier)
                    (argument_list))))
        (else_statement
            body: (block
                (call_expression
                    function: (identifier)
                    (argument_list)))))
    (if_statement
        condition: (optional_binding_condition
                       name: (identifier)
                       (initializer (identifier)))
        body: (block
                (call_expression
                    function: (identifier)
                    (argument_list
                        (argument value: (identifier))))))
    (if_statement
        condition: (optional_shadowing_condition
                       name: (identifier))
        body: (block
                  (call_expression
                      function: (identifier)
                      (argument_list
                          (argument
                              value: (identifier)))))))


================
Switch statements
================

switch value {
    @unknown default:
        break
}

switch `visibility` {
    case `private`:
        return false
    default:
        return true
}

switch foo {
    case bar, baz:
        break
    case quux, plugh, frobozz,:
        break
}

switch foo {
    case .bar:
        return

    case .baz:
        return

    default:
        return
}

----

(source_file
    (switch_statement
        expression: (identifier)
        (switch_default (case_body (break_statement))))
    (switch_statement
        expression: (escaped_identifier)
        (switch_case
            (match_descriptor
                pattern: (escaped_identifier))
            (case_body
                (return_statement value: (boolean_literal))))
            (switch_default
                (case_body
                    (return_statement value: (boolean_literal)))))
    (switch_statement
        expression: (identifier)
        (switch_case
            (match_descriptor
                pattern: (identifier))
            (match_descriptor
                pattern: (identifier))
            (case_body (break_statement)))
        (switch_case
            (match_descriptor
                pattern: (identifier))
            (match_descriptor
                pattern: (identifier))
            (match_descriptor
                pattern: (identifier))
            (case_body (break_statement))))
    (switch_statement
        expression: (identifier)
        (switch_case
            (match_descriptor
                pattern: (member_pattern
                             member: (identifier)))
            (case_body (return_statement)))
        (switch_case
            (match_descriptor
                pattern: (member_pattern
                             member: (identifier)))
            (case_body (return_statement)))
        (switch_default
            (case_body (return_statement)))))


================
Switch case where clauses
================

switch foo {
    case bar where baz,
         quux:
        break
    case xyzzy where waldo,
         frobozz where fanucci:
        break;
    case ipso where furglaneti:
        break
}

----

(source_file
    (switch_statement
        expression: (identifier)
        (switch_case
            (match_descriptor
                pattern: (identifier)
                (where_clause (identifier)))
            (match_descriptor
                pattern: (identifier))
            (case_body (break_statement)))
        (switch_case
            (match_descriptor
                pattern: (identifier)
                (where_clause (identifier)))
            (match_descriptor
                pattern: (identifier)
                (where_clause (identifier)))
            (case_body (break_statement)))
        (switch_case
            (match_descriptor
                pattern: (identifier)
                (where_clause (identifier)))
            (case_body (break_statement)))))


================
Consuming pattern statements
================

switch consume foo {
    default: break
}

if case let .foo(bar) = consume baz {}

guard case let .foo(bar) = consume baz else { return }

----

(source_file
    (switch_statement
        expression: (ownership_expression operand: (identifier))
        (switch_default
            (case_body (break_statement))))
    (if_statement
        condition: (case_condition
                        pattern: (binding_match_pattern
                            binding: (member_pattern
                                         member: (identifier)
                                         (assoc_value_patterns (identifier))))
                           value: (ownership_expression
                                      operand: (identifier)))
        body: (block))
    (guard_statement
        condition: (case_condition
                        pattern: (binding_match_pattern
                            binding: (member_pattern
                                         member: (identifier)
                                         (assoc_value_patterns (identifier))))
                           value: (ownership_expression
                                      operand: (identifier)))
        body: (block (return_statement))))


================
Defer statement
================

defer { immanentize_the_eschaton() }

func foo() {
    let lock = Lock()
    defer { lock.unlock() }

    return
}

----

(source_file
    (defer_statement
        body: (block
                  (call_expression
                      function: (identifier)
                      (argument_list))))
    (function_declaration
        name: (identifier)
        (parameter_list)
        body: (block
            (constant_declaration
                name: (identifier)
                initializer: (call_expression
                                 function: (identifier)
                                 (argument_list)))
            (defer_statement
                body: (block
                          (call_expression
                              function: (member_expression
                                            base: (identifier)
                                            member: (identifier))
                              (argument_list))))
            (return_statement))))


================
Labelled statements
================

quux: if nothing {}

baz: switch value {
    default:
        break
}

----

(source_file
    (labelled_statement
        (statement_label (identifier))
        (if_statement
            condition: (identifier)
            body: (block)))
    (labelled_statement
        (statement_label (identifier))
        (switch_statement
            expression: (identifier)
            (switch_default (case_body (break_statement))))))

================
Do statements
================

do {
    try something()
}

do {
   try somethingElse()
} catch let error where foo() {
    attemptRecovery(error)
} catch is FooError, is BarError {
    return
} catch let .foo(x) where x == ohNo {
    return
} catch Plugh.quux, Frobozz.waldo {
    return
} catch {
    fatalError("Something went wrong")
}

----

(source_file
    (do_statement
        body: (block
            (try_expression
                (call_expression
                    function: (identifier)
                    (argument_list)))))
    (do_statement
        body: (block
            (try_expression
                (call_expression
                    function: (identifier)
                    (argument_list))))
        (catch_clause
            (catch_pattern
                pattern: (binding_match_pattern
                             binding: (identifier))
                (where_clause
                    (call_expression
                        function: (identifier)
                        (argument_list))))
            body: (block
                (call_expression
                    function: (identifier)
                    (argument_list
                        (argument value: (identifier))))))
        (catch_clause
            (catch_pattern
                pattern: (is_pattern (simple_type (type_identifier))))
            (catch_pattern
                pattern: (is_pattern (simple_type (type_identifier))))
            body: (block
                      (return_statement)))
        (catch_clause
            (catch_pattern
                pattern: (binding_match_pattern
                             binding: (member_pattern
                                          member: (identifier)
                                          (assoc_value_patterns
                                              (identifier))))
                (where_clause
                    (binary_expression
                         lhs: (identifier)
                         (binary_operator)
                         rhs: (identifier))))
             body: (block (return_statement)))
        (catch_clause
            (catch_pattern
                pattern: (member_pattern
                             base: (type_identifier)
                             member: (identifier)))
            (catch_pattern
                pattern: (member_pattern
                             base: (type_identifier)
                             member: (identifier)))
            body: (block (return_statement)))
        (catch_clause
            body: (block
                (call_expression
                    function: (identifier)
                    (argument_list
                        (argument
                            value: (string_literal (string_text)))))))))


================
Do statement with throws clause
================

do throws(CatError) {

}
catch {

}

----

(source_file
    (do_statement
        (throw_effect
            (simple_type (type_identifier)))
        (block)
        (catch_clause
            (block))))


================
Discard statement
================

struct Foo {
    deinit {
        discard self
    }
}

----

(source_file
    (struct_declaration
        name: (type_identifier)
        (type_body
            (deinit_declaration
                body: (block
                          (discard_statement operand: (self)))))))


================
Expression statements (semicolons)
================

let x = x.y;

try foo();

counts[word, default: 0] += 1;

----

(source_file
    (constant_declaration
        name: (identifier)
        initializer: (member_expression
                         base: (identifier)
                         member: (identifier)))
    (try_expression
        (call_expression
            function: (identifier)
            (argument_list)))
    (binary_expression
        lhs: (subscript_expression
                 base: (identifier)
                     (subscriptor
                         (argument value: (identifier))
                         (argument
                             label: (identifier)
                             value: (numeric_literal))))
        (binary_operator)
        rhs: (numeric_literal)))


================
Empty statement
================

;
; ;

----

(source_file (empty_statement) (empty_statement) (empty_statement))


================
shbang
================

#!/usr/bin/env swift -sanitize=threads

print("Hello, world!")

----

(source_file
    (shbang
        (interpreter_path)
        (interpreter_arg)
        (interpreter_arg))
    (call_expression
        function: (identifier)
        (argument_list
            (argument
                value: (string_literal (string_text))))))
