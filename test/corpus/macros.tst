================
Macro declarations
================

@freestanding(expression) macro prohibitBinaryOperators<T>(
  _ value: T,
  operators: [String]
) -> T =
    #externalMacro(module: "ExampleMacros", type: "ProhibitBinaryOperators")

@freestanding(expression)
macro addBlocker<T>(_ value: T) -> T =
  #prohibitBinaryOperators(value, operators: ["+"])

@attached(extension, conformances: Foo, names: named(requirement))
macro Foo() = #externalMacro(module: "Bar", type: "Quux")

----

(source_file
    (macro_declaration
        (attribute
            name: (identifier)
            (argument_list
                (argument value: (identifier))))
       name: (identifier)
       (generic_parameter_list
           (generic_parameter name: (type_identifier)))
       (parameter_list
           (parameter
               external_name: (identifier)
               local_name: (identifier)
               type: (simple_type (type_identifier)))
           (parameter
               local_name: (identifier)
               type: (array_type
                         element: (simple_type (type_identifier)))))
       (function_result
           return_type: (simple_type
                            (type_identifier)))
       definition:
           (macro_definition
               (macro_expansion_expression
                   name: (identifier)
                   (argument_list
                       (argument label: (identifier)
                                 value: (string_literal (string_text)))
                       (argument label: (identifier)
                                 value: (string_literal (string_text)))))))
    (macro_declaration
        (attribute name: (identifier)
                   (argument_list
                       (argument value: (identifier))))
        name: (identifier)
        (generic_parameter_list
            (generic_parameter name: (type_identifier)))
        (parameter_list
            (parameter external_name: (identifier)
                       local_name: (identifier)
                       type: (simple_type (type_identifier))))
        (function_result
            return_type: (simple_type (type_identifier)))
        definition:
            (macro_definition
                (macro_expansion_expression
                    name: (identifier)
                    (argument_list
                        (argument value: (identifier))
                        (argument label: (identifier)
                                  value: (array_literal
                                             (string_literal
                                                 (string_text))))))))
    (macro_declaration
        (attribute
            name: (identifier)
            (argument_list
                (argument value: (identifier))
                (argument
                    label: (identifier)
                    value: (identifier))
                (argument
                    label: (identifier)
                    value: (call_expression
                               function: (identifier)
                               (argument_list
                                   (argument
                                       value: (identifier)))))))
        name: (identifier)
        (parameter_list)
        definition: (macro_definition
                        (macro_expansion_expression
                            name: (identifier)
                            (argument_list
                                (argument
                                    label: (identifier)
                                    value: (string_literal (string_text)))
                                (argument
                                    label: (identifier)
                                    value: (string_literal (string_text))))))))


================
Macro expressions
================

#addBlocker(#stringify(1 + 2))

#xyzzy

----

(source_file
    (macro_expansion_expression
        name: (identifier)
       (argument_list
           (argument
               value: (macro_expansion_expression
                          name: (identifier)
                          (argument_list
                              (argument
                                  value: (binary_expression
                                             lhs: (numeric_literal)
                                             (binary_operator)
                                             rhs: (numeric_literal))))))))
    (macro_expansion_expression
        name: (identifier)))
