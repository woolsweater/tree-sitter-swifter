================
Warning and error
================

#warning("")

#warning("Core meltdown imminent")

#error("")

#error("I'm sorry, I can't do that")

#warning("""
    Thank you for noticing this notice.
    """)

#error("""
    Your noticing has been noted.
    """)

----

(source_file
    (compiler_diagnostic_statement (string_literal))
    (compiler_diagnostic_statement
        (string_literal (string_text)))
    (compiler_diagnostic_statement (string_literal))
    (compiler_diagnostic_statement
        (string_literal (string_text)))
    (compiler_diagnostic_statement
        (string_literal (string_text)))
    (compiler_diagnostic_statement
        (string_literal (string_text))))


================
Compiler expressions
================

#column
#dsohandle
#file
#fileID
#filePath
#function
#line

let column = #column
failTest(at: #line, from: #file)

----

(source_file
    (compiler_expression)
    (compiler_expression)
    (compiler_expression)
    (compiler_expression)
    (compiler_expression)
    (compiler_expression)
    (compiler_expression)
    (constant_declaration
        name: (identifier)
        initializer: (compiler_expression))
    (call_expression
        function: (identifier)
        (argument_list
            (argument
                label: (identifier)
                value: (compiler_expression))
            (argument
                label: (identifier)
                value: (compiler_expression)))))


================
Xcode "literals"
================

#colorLiteral(red: 0.5, green: 0.3, blue: 0.1, alpha: 1.0)

#fileLiteral(resourceName: "data/foo.txt")

#imageLiteral(resourceName: "icons/bar.png")

----

(source_file
    (compiler_expression
        (argument_list
            (argument
                label: (identifier)
                value: (numeric_literal))
            (argument
                label: (identifier)
                value: (numeric_literal))
            (argument
                label: (identifier)
                value: (numeric_literal))
            (argument
                label: (identifier)
                value: (numeric_literal))))
    (compiler_expression
        (argument_list
            (argument
                label: (identifier)
                value: (string_literal (string_text)))))
    (compiler_expression
        (argument_list
            (argument
                label: (identifier)
                value: (string_literal (string_text))))))


================
Availability conditions
================

if #available(iOS 15, *) {

}
else if #unavailable(tvOS 12, *) {

}

----

(source_file
    (if_statement
        (compiler_expression
            (availability_condition
                (platform_specifier (platform_name) (version_number))
                (platform_specifier (wildcard))))
        (block)
        (else_if_statement
            (compiler_expression
                (availability_condition
                    (platform_specifier (platform_name) (version_number))
                    (platform_specifier (wildcard))))
            (block))))


================
Compiler control statements
================

#if FOO && BAR
let constant = 100
#elseif !BAR && true
let constant = 12
#else
let constant = 3
#endif

----

(source_file
    (compiler_control_statement
        (compiler_condition (identifier) (identifier))
        (compiler_condition_body
            (constant_declaration
                name: (identifier)
                initializer: (numeric_literal)))
        (compiler_condition
            (identifier)
            (boolean_literal))
        (compiler_condition_body
            (constant_declaration
                name: (identifier)
                initializer: (numeric_literal)))
        (compiler_condition_body
            (constant_declaration
                name: (identifier)
                initializer: (numeric_literal)))))


================
Function-like compiler conditions
================

#if os(macOS) && arch(x86_64)
let foo = foo
#elseif swift(>=5.2) || !compiler(<6)
let foo = bar
#elseif canImport(Frobozz) && targetEnvironment(simulator)
let foo = baz
#elseif hasFeature(MagicBalloons)
let foo = bookkeeper
#elseif hasAttribute(trebledFromps)
let foo = fanucci
#else
let foo = quux
#endif

----

(source_file
    (compiler_control_statement
        (compiler_condition
            (os_compiler_condition)
            (arch_compiler_condition))
        (compiler_condition_body
            (constant_declaration
                name: (identifier)
                initializer: (identifier)))
        (compiler_condition
            (language_compiler_condition
                (version_number))
            (language_compiler_condition
                (version_number)))
        (compiler_condition_body
            (constant_declaration
                name: (identifier)
                initializer: (identifier)))
        (compiler_condition
            (import_compiler_condition (module_name))
            (environment_compiler_condition))
        (compiler_condition_body
            (constant_declaration
                name: (identifier)
                initializer: (identifier)))
        (compiler_condition
            (feature_compiler_condition
                feature_name: (identifier)))
        (compiler_condition_body
            (constant_declaration
                name: (identifier)
                initializer: (identifier)))
        (compiler_condition
            (attribute_compiler_condition (identifier)))
        (compiler_condition_body
            (constant_declaration
                name: (identifier)
                initializer: (identifier)))
        (compiler_condition_body
            (constant_declaration
                name: (identifier)
                initializer: (identifier)))))


================
Compiler directives inside types
================

class C {
    #if FOO
    static let bar = "quux"
    #else
    static let bar = "waldo"
    #endif
}

enum E {
    #if FOO
    case a
    #endif
}

----

(source_file
    (class_declaration
        name: (type_identifier)
        (type_body
            (compiler_control_statement
                (compiler_condition
                    (identifier))
                (compiler_condition_body
                    (constant_property_declaration
                        (member_modifier)
                        name: (identifier)
                        initializer: (string_literal (string_text))))
                (compiler_condition_body
                    (constant_property_declaration
                        (member_modifier)
                        name: (identifier)
                        initializer: (string_literal (string_text)))))))
    (enum_declaration
        name: (type_identifier)
        (enum_body
        (compiler_control_statement
            (compiler_condition
                (identifier))
            (compiler_condition_body
                (case_declaration
                    name: (identifier)))))))


================
Line control statement
================

struct Foo {
    #sourceLocation()
}

#sourceLocation(file: "Foo.swift", line: 123)

----

(source_file
    (struct_declaration
        name: (type_identifier)
        (type_body (line_control_statement)))
    (line_control_statement
        path: (string_literal (string_text))
        line: (numeric_literal)))
