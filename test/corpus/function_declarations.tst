================
Simple function definition
================

func fooTheBar() {}

func _makeMeASandwich() {

}

func immanentize_the_eschaton3()
{
}

func `switch`() {}

func 🧨 () {}

----

(source_file
    (function_declaration
        name: (identifier)
        (parameter_list)
        body: (block))
    (function_declaration
        name: (identifier)
        (parameter_list)
        body: (block))
    (function_declaration
        name: (identifier)
        (parameter_list)
        body: (block))
    (function_declaration
        name: (escaped_identifier)
        (parameter_list)
        body: (block))
    (function_declaration
        name: (identifier)
        (parameter_list)
        body: (block)))


================
Raw identifiers
================

func `square returns x * x`() {}

func `100`() {}

----

(source_file
    (function_declaration
        name: (escaped_identifier)
        (parameter_list)
        body: (block))
    (function_declaration
        name: (escaped_identifier)
        (parameter_list)
        body: (block)))


================
Access modifiers
================

public func foo() {}

package func bar() {}

internal func baz() {}

fileprivate func quux() {}

private func plugh() {}

----

(source_file
    (function_declaration
        (access_level_modifier)
        name: (identifier)
        (parameter_list)
        body: (block))
    (function_declaration
        (access_level_modifier)
        name: (identifier)
        (parameter_list)
        body: (block))
    (function_declaration
        (access_level_modifier)
        name: (identifier)
        (parameter_list)
        body: (block))
    (function_declaration
        (access_level_modifier)
        name: (identifier)
        (parameter_list)
        body: (block))
    (function_declaration
        (access_level_modifier)
        name: (identifier)
        (parameter_list)
        body: (block)))


================
Function parameters
================

func makeMeASandwich(with cheese: [Deli.Cheese]) {}

func immanentize_the_eschaton(quickly: Bool) {}

func process(mode: Mode, values: Value...) {}

func process(mode: Mode..., values: Value...) {}

func doThatThing<T>(_ `default`: T) {}

func foo(
    bar: Bar,
    baz: Baz,
) {}

----

(source_file
    (function_declaration
        name: (identifier)
        (parameter_list
            (parameter
                external_name: (identifier)
                local_name: (identifier)
                type: (array_type
                          element: (simple_type
                                      (type_identifier)
                                      (type_identifier)))))
        body: (block))
    (function_declaration
        name: (identifier)
        (parameter_list
            (parameter
                local_name: (identifier)
                type: (simple_type (type_identifier))))
        body: (block))
    (function_declaration
        name: (identifier)
        (parameter_list
            (parameter
                local_name: (identifier)
                type: (simple_type (type_identifier)))
            (parameter
                local_name: (identifier)
                type: (variadic_type
                          (simple_type (type_identifier)))))
        body: (block))
    (function_declaration
        name: (identifier)
        (parameter_list
            (parameter
                local_name: (identifier)
                type: (variadic_type
                          (simple_type (type_identifier))))
            (parameter
                local_name: (identifier)
                type: (variadic_type
                          (simple_type (type_identifier)))))
        body: (block))
    (function_declaration
        name: (identifier)
        (generic_parameter_list
            (generic_parameter name: (type_identifier)))
        (parameter_list
            (parameter
                external_name: (identifier)
                local_name: (escaped_identifier)
                type: (simple_type (type_identifier))))
        body: (block))
    (function_declaration
        name: (identifier)
        (parameter_list
            (parameter
                local_name: (identifier)
                type: (simple_type
                          (type_identifier)))
            (parameter
                local_name: (identifier)
                type: (simple_type
                          (type_identifier))))
        body: (block)))


================
Function returns
================

func makeMeASandwich() throws -> Sandwich {}

func fooTheBar(_ transform: X & Y & Z) rethrows {}

func immanentize_the_eschaton() -> World.Boom {}

func foo<B : Bar>() where B.Baz == (quux: Quux, xyzzy: Xyzzy) {}

----

(source_file
    (function_declaration
        name: (identifier)
        (parameter_list)
        (function_result
            (throw_effect)
            return_type: (simple_type (type_identifier)))
        body: (block))
    (function_declaration
        name: (identifier)
        (parameter_list
            (parameter
                external_name: (identifier)
                local_name: (identifier)
                type: (protocol_composition_type
                          (simple_type (type_identifier))
                          (protocol_composition_type
                              (simple_type (type_identifier))
                              (simple_type (type_identifier))))))
        (function_result
            (throw_effect))
        body: (block))
    (function_declaration
        name: (identifier)
        (parameter_list)
        (function_result
            return_type: (simple_type (type_identifier) (type_identifier)))
        body: (block))
    (function_declaration
        name: (identifier)
        (generic_parameter_list
            (generic_parameter
                name: (type_identifier)
                requirement: (simple_type (type_identifier))))
        (parameter_list)
        (generic_where_clause
            (generic_requirement
                type: (simple_type
                          (type_identifier)
                          (type_identifier))
                constraint: (tuple_type
                                label: (identifier) (simple_type
                                                        (type_identifier))
                                label: (identifier) (simple_type
                                                        (type_identifier)))))
        body: (block)))


================
Typed throws
================

func foo() throws(FooError) -> Foo {}

----

(source_file
    (function_declaration
        name: (identifier)
        (parameter_list)
        (function_result
            (throw_effect
                error_type: (simple_type (type_identifier)))
            return_type: (simple_type (type_identifier)))
        body: (block)))


================
Async functions
================

func makeDinner() async -> Dinner {}

func catchTheTrain() async throws {}

func downloadImage() async throws -> Image {}

----

(source_file
    (function_declaration
        name: (identifier)
        (parameter_list)
        (function_result
            (async_effect)
            return_type: (simple_type (type_identifier)))
        body: (block))
    (function_declaration
        name: (identifier)
        (parameter_list)
        (function_result
            (async_effect)
            (throw_effect))
        body: (block))
    (function_declaration
        name: (identifier)
        (parameter_list)
        (function_result
            (async_effect)
            (throw_effect)
            return_type: (simple_type (type_identifier)))
        body: (block)))


================
Function annotations
================

@discardableResult func foo() -> Bool {

}

@usableFromInline
@inlinable
func cantSeeMe() {}

@_doBeDoBeDo @objc
func beDoBeDo()
{}

@available(*, unavailable)
func makeMeASandwich() {}

@available(*, noasync, message: "Lorem ipsum dolor sit amet")
func foo() {}

@backDeployed(before: SwiftStdlib 5.11)
func bar() {}

----

(source_file
    (function_declaration
        (attribute name: (identifier))
        name: (identifier)
        (parameter_list)
        (function_result
            return_type: (simple_type (type_identifier)))
        body: (block))
    (function_declaration
        (attribute name: (identifier))
        (attribute name: (identifier))
        name: (identifier)
        (parameter_list)
        body: (block))
    (function_declaration
        (attribute name: (identifier))
        (objc_name_attribute)
        name: (identifier)
        (parameter_list)
        body: (block))
    (function_declaration
        (availability_attribute
            (platform_specifier (wildcard))
            (availability_version_detail))
        name: (identifier)
        (parameter_list)
        body: (block))
    (function_declaration
        (availability_attribute
            (platform_specifier (wildcard))
            (availability_version_detail)
            (availability_explanation
                (string_literal (string_text))))
        name: (identifier)
        (parameter_list)
        body: (block))
    (function_declaration
        (backdeploy_attribute
            (platform_specifier
                (platform_name)
                (version_number)))
        name: (identifier)
        (parameter_list)
        body: (block)))


================
Function parameter annotations/modifiers
================

func fooTheBar(quux: inout Quux) {

}

func immanentize_the_eschaton(then: @escaping Callback) {}

func doThatThing(with: @escaping @autoclosure @convention(c) Callback)
{}

func makeMeASandwich(_ fn: @Sendable () -> Void) {}

func deposit(to account: isolated Account) {}

----

(source_file
    (function_declaration
        name: (identifier)
        (parameter_list
            (parameter
                local_name: (identifier)
                type: (simple_type (type_identifier))))
        body: (block))
    (function_declaration
        name: (identifier)
        (parameter_list
            (parameter
                local_name: (identifier)
                (type_attributes
                    (type_attribute))
                type: (simple_type (type_identifier))))
        body: (block))
    (function_declaration
        name: (identifier)
        (parameter_list
            (parameter
                local_name: (identifier)
                (type_attributes
                    (type_attribute)
                    (type_attribute)
                    (type_attribute))
                type: (simple_type (type_identifier))))
        body: (block))
    (function_declaration
        name: (identifier)
        (parameter_list
            (parameter
                external_name: (identifier)
                local_name: (identifier)
                (type_attributes
                    (type_attribute))
                type: (function_type
                          (parameter_list)
                          (function_result
                              return_type: (simple_type (type_identifier))))))
        body: (block))
    (function_declaration
        name: (identifier)
        (parameter_list
            (parameter
                external_name: (identifier)
                local_name: (identifier)
                (isolation_modifier)
                type: (simple_type (type_identifier))))
        body: (block)))


================
Generics
================

func fooTheBar<T>(t: inout T) {}

func makeABox<Material : Wood>() {}

func fooTheBar<T, U : V, W>(t: T) -> W where U.X == T.Z<W> {}

----

(source_file
    (function_declaration
        name: (identifier)
        (generic_parameter_list
            (generic_parameter name: (type_identifier)))
        (parameter_list
            (parameter
                local_name: (identifier)
                type: (simple_type (type_identifier))))
        body: (block))
    (function_declaration
        name: (identifier)
        (generic_parameter_list
            (generic_parameter
                name: (type_identifier)
                requirement: (simple_type (type_identifier))))
        (parameter_list)
        body: (block))
    (function_declaration
        name: (identifier)
        (generic_parameter_list
            (generic_parameter
                name: (type_identifier))
            (generic_parameter
                name: (type_identifier)
                requirement: (simple_type (type_identifier)))
            (generic_parameter
                name: (type_identifier)))
        (parameter_list
            (parameter
                local_name: (identifier)
                type: (simple_type (type_identifier))))
        (function_result
            return_type: (simple_type (type_identifier)))
        (generic_where_clause
            (generic_requirement
                type: (simple_type (type_identifier) (type_identifier))
                constraint: (simple_type
                                (type_identifier)
                                (generic_type
                                    (generic_argument_list
                                        (simple_type (type_identifier)))))))
        body: (block)))


================
Function parameter default values
================

func foo(bar: Bar, baz: Baz = .standard) {}

func makeABox(quickly: Bool = true, completion: (Bool) -> Void = { _ in }) {}

----

(source_file
    (function_declaration
        name: (identifier)
        (parameter_list
            (parameter
                local_name: (identifier)
                type: (simple_type (type_identifier)))
            (parameter
                local_name: (identifier)
                type: (simple_type (type_identifier))
                (default_value (implicit_member_expression (identifier)))))
        body: (block))
    (function_declaration
        name: (identifier)
        (parameter_list
            (parameter
                local_name: (identifier)
                type: (simple_type (type_identifier))
                (default_value (boolean_literal)))
            (parameter
                local_name: (identifier)
                type: (function_type
                          (parameter_list
                              (parameter
                                  type: (simple_type (type_identifier))))
                          (function_result
                              return_type: (simple_type (type_identifier))))
                (default_value
                    (closure_expression
                        (parameter_list
                            (parameter name: (identifier)))))))
        body: (block)))


================
Operator functions
================

func == (lhs: Foo, rhs: Foo) -> Bool {
    return false
}

func <^> <T : Hashable> (lhs: T, rhs: T) {}

postfix func -|- (n: inout Int) { n *= 2 }

prefix func ¬ (value: Bool) -> Bool { !value }

struct Foo {
    prefix static func ^ (value: Foo) -> Foo { self }
    static postfix func ¬ (value: Foo) -> Bool { false }
}

struct Foo : Comparable {
    static func < (lhs: Self, rhs: Self) -> Bool {
        false
    }
}

----

(source_file
    (function_declaration
        name: (operator_name)
        (parameter_list
            (parameter
                local_name: (identifier)
                type: (simple_type (type_identifier)))
            (parameter
                local_name: (identifier)
                type: (simple_type (type_identifier))))
        (function_result
            return_type: (simple_type (type_identifier)))
        body: (block
                  (return_statement value: (boolean_literal))))
    (function_declaration
        name: (operator_name)
        (generic_parameter_list
            (generic_parameter
                name: (type_identifier)
                requirement: (simple_type (type_identifier))))
        (parameter_list
            (parameter
                local_name: (identifier)
                type: (simple_type (type_identifier)))
            (parameter
                local_name: (identifier)
                type: (simple_type (type_identifier))))
        body: (block))
    (function_declaration
        name: (operator_name)
        (parameter_list
            (parameter
                local_name: (identifier)
                type: (simple_type (type_identifier))))
        body: (block
                  (binary_expression
                      lhs: (identifier)
                      (binary_operator)
                      rhs: (numeric_literal))))
    (function_declaration
        name: (operator_name)
        (parameter_list
            (parameter
                local_name: (identifier)
                type: (simple_type (type_identifier))))
        (function_result
            return_type: (simple_type (type_identifier)))
        body: (block
                  (prefix_expression
                      (prefix_operator)
                      operand: (identifier))))
    (struct_declaration
        name: (type_identifier)
        (type_body
            (method_declaration
                (member_modifier)
                name: (operator_name)
                (parameter_list
                    (parameter
                        local_name: (identifier)
                        type: (simple_type (type_identifier))))
                (function_result
                    return_type: (simple_type (type_identifier)))
                body: (block (self)))
            (method_declaration
                (member_modifier)
                name: (operator_name)
                (parameter_list
                    (parameter
                        local_name: (identifier)
                        type: (simple_type (type_identifier))))
                (function_result
                    return_type: (simple_type (type_identifier)))
                body: (block (boolean_literal)))))
    (struct_declaration
        name: (type_identifier)
        (inheritance_clause
            (simple_type (type_identifier)))
        (type_body
            (method_declaration
                (member_modifier)
                name: (operator_name)
                (parameter_list
                    (parameter
                        local_name: (identifier)
                        type: (simple_type (type_identifier)))
                    (parameter
                        local_name: (identifier)
                        type: (simple_type (type_identifier))))
                (function_result
                    return_type: (simple_type (type_identifier)))
                body: (block (boolean_literal))))))


================
Property wrappers on parameters
================

func foo(@Bar value: Value) {}

func baz(@Quux @Xyzzy plugh frobozz: Frobozz) {}

----

(source_file
    (function_declaration
        name: (identifier)
        (parameter_list
            (parameter
                (attribute name: (identifier))
                local_name: (identifier)
                type: (simple_type (type_identifier))))
        body: (block))
    (function_declaration
        name: (identifier)
        (parameter_list
            (parameter
                (attribute name: (identifier))
                (attribute name: (identifier))
                external_name: (identifier)
                local_name: (identifier)
                type: (simple_type (type_identifier))))
        body: (block)))


================
Operator declarations
================

infix operator <^> : MultiplicationPrecedence

postfix operator **

prefix operator -•^

prefix operator ¬

----

(source_file
    (operator_declaration
        (operator_name)
        (precedence_group))
    (operator_declaration
        (operator_name))
    (operator_declaration
        (operator_name))
    (operator_declaration
        (operator_name)))


================
ARC (parameter ownership) modifiers
================

func frobozz(_: borrowing Waldo) {}
func immanentize(_: consuming Eschaton) {}

func frobozz(_: nonconsuming Waldo) {}
func immanentize(_: consuming Eschaton) {}

struct S {
    mutating __consuming func foo() {}
    public consuming func bar() {}
}

func baz(_: __shared Quux) {}
func xyzzy(_: __owned Plugh) {}

----

(source_file
    (function_declaration
        name: (identifier)
        (parameter_list
            (parameter
                local_name: (identifier)
                (arc_modifier)
                type: (simple_type (type_identifier))))
        body: (block))
    (function_declaration
        name: (identifier)
        (parameter_list
            (parameter
                local_name: (identifier)
                (arc_modifier)
                type: (simple_type (type_identifier))))
        body: (block))
    (function_declaration
        name: (identifier)
        (parameter_list
            (parameter
                local_name: (identifier)
                (arc_modifier)
                type: (simple_type (type_identifier))))
        body: (block))
    (function_declaration
        name: (identifier)
        (parameter_list
            (parameter
                local_name: (identifier)
                (arc_modifier)
                type: (simple_type (type_identifier))))
        body: (block))
    (struct_declaration
        name: (type_identifier)
        (type_body
            (method_declaration
                (member_modifier)
                (arc_modifier)
                name: (identifier)
                (parameter_list)
                body: (block))
            (method_declaration
                (access_level_modifier)
                (arc_modifier)
                name: (identifier)
                (parameter_list)
                body: (block))))
    (function_declaration
        name: (identifier)
        (parameter_list
            (parameter
                local_name: (identifier)
                (arc_modifier)
                type: (simple_type (type_identifier))))
        body: (block))
    (function_declaration
        name: (identifier)
        (parameter_list
            (parameter
                local_name: (identifier)
                (arc_modifier)
                type: (simple_type (type_identifier))))
        body: (block)))


================
'sending' keyword
================

func foo(_: sending Bar, _: sending @escaping () -> Baz) {}

let quux: (sending Plugh) -> Void

func xyzzy() -> sending Frobozz {}

let mumble: () -> sending Wobble

func corge(_ fred: consuming sending some ~Copyable) {}

----

(source_file
    (function_declaration
        name: (identifier)
        (parameter_list
            (parameter
                local_name: (identifier)
                (isolation_modifier)
                type: (simple_type (type_identifier)))
            (parameter
                local_name: (identifier)
                (isolation_modifier)
                (type_attributes
                    (type_attribute))
                type: (function_type
                          (parameter_list)
                          (function_result
                              return_type: (simple_type
                                               (type_identifier))))))
        body: (block))
    (constant_declaration
        name: (identifier)
        type: (function_type
                  (parameter_list
                      (parameter
                          (isolation_modifier)
                          type: (simple_type (type_identifier))))
                  (function_result
                      return_type: (simple_type (type_identifier)))))
    (function_declaration
        name: (identifier)
        (parameter_list)
        (function_result
            (isolation_modifier)
            return_type: (simple_type (type_identifier)))
        body: (block))
    (constant_declaration
        name: (identifier)
        type: (function_type
                  (parameter_list)
                  (function_result
                      (isolation_modifier)
                      return_type: (simple_type (type_identifier)))))
    (function_declaration
        name: (identifier)
        (parameter_list
            (parameter
                external_name: (identifier)
                local_name: (identifier)
                (arc_modifier)
                (isolation_modifier)
                type: (opaque_type
                          (protocol_nonconformance
                              (simple_type (type_identifier))))))
        body: (block)))


================
Parameter packs
================

func foo<each T, each U>() -> (repeat each T) {}

func bar<each T>(_: repeat each T) -> (Int, repeat Array<each T>) {}

func baz<each T, each U>(
  t: repeat each T,
  u: repeat each U
) -> (repeat (each T) -> (repeat each U)) {}

func quux<each T : Sequence>(_: repeat each T) -> (repeat (each T).Element) {}

func plugh<each S: Sequence, T>(
    _: repeat each S
) where repeat (each S).Element == T {}

func frobozz<each S: Sequence, each T>(
    _: repeat each S
) where repeat (each S).Element == Array<each T> {}

----

(source_file
    (function_declaration
        name: (identifier)
        (generic_parameter_list
            (generic_parameter
                (parameter_pack
                    name: (type_identifier)))
            (generic_parameter
                (parameter_pack
                    name: (type_identifier))))
        (parameter_list)
        (function_result
            return_type: (parenthesized_type
                             (pack_expansion_type
                                 pattern_type: (pack_reference
                                                   (simple_type
                                                       (type_identifier))))))
        body: (block))
    (function_declaration
        name: (identifier)
        (generic_parameter_list
            (generic_parameter
                (parameter_pack
                    name: (type_identifier))))
        (parameter_list
            (parameter
                local_name: (identifier)
                type: (pack_expansion_type
                          pattern_type: (pack_reference
                                            (simple_type (type_identifier))))))
        (function_result
            return_type:
                (tuple_type
                    (simple_type (type_identifier))
                    (pack_expansion_type
                        pattern_type:
                            (simple_type
                                (generic_type
                                    (generic_argument_list
                                        (pack_reference
                                            (simple_type
                                                (type_identifier)))))))))
        body: (block))
    (function_declaration
        name: (identifier)
        (generic_parameter_list
            (generic_parameter
                (parameter_pack
                    name: (type_identifier)))
            (generic_parameter
                (parameter_pack
                    name: (type_identifier))))
        (parameter_list
            (parameter
                local_name: (identifier)
                type: (pack_expansion_type
                          pattern_type: (pack_reference
                                            (simple_type (type_identifier)))))
            (parameter
                local_name: (identifier)
                type: (pack_expansion_type
                          pattern_type: (pack_reference
                                            (simple_type (type_identifier))))))
        (function_result
            return_type:
                (parenthesized_type
                    (pack_expansion_type
                        pattern_type:
                            (function_type
                                (parameter_list
                                    (parameter
                                        type: (pack_reference
                                                  (simple_type
                                                      (type_identifier)))))
                                (function_result return_type:
                                    (parenthesized_type
                                        (pack_expansion_type pattern_type:
                                            (pack_reference
                                                (simple_type
                                                    (type_identifier))))))))))
        body: (block))
    (function_declaration
        name: (identifier)
        (generic_parameter_list
            (generic_parameter
                (parameter_pack
                    name: (type_identifier))
                requirement: (simple_type
                                 (type_identifier))))
        (parameter_list
            (parameter
                local_name: (identifier)
                type: (pack_expansion_type
                          pattern_type: (pack_reference
                                            (simple_type (type_identifier))))))
        (function_result
            return_type: (parenthesized_type
                             (pack_expansion_type
                                 pattern_type: (member_type_pack
                                                   (simple_type
                                                       (type_identifier))
                                                   (type_identifier)))))
        body: (block))
    (function_declaration
        name: (identifier)
        (generic_parameter_list
            (generic_parameter
                (parameter_pack
                    name: (type_identifier))
            requirement: (simple_type
                             (type_identifier)))
            (generic_parameter
                name: (type_identifier)))
        (parameter_list
            (parameter
                local_name: (identifier)
                type: (pack_expansion_type
                          pattern_type: (pack_reference
                                            (simple_type (type_identifier))))))
        (generic_where_clause
            (generic_requirement
                type: (pack_expansion_type
                          pattern_type: (member_type_pack
                                            (simple_type
                                                (type_identifier))
                                            (type_identifier)))
                constraint: (simple_type
                                (type_identifier))))
        body: (block))
    (function_declaration
        name: (identifier)
        (generic_parameter_list
            (generic_parameter
                (parameter_pack
                    name: (type_identifier))
                requirement: (simple_type
                                 (type_identifier)))
            (generic_parameter
                (parameter_pack
                    name: (type_identifier))))
        (parameter_list
            (parameter
                local_name: (identifier)
                type: (pack_expansion_type
                          pattern_type: (pack_reference
                                            (simple_type (type_identifier))))))
        (generic_where_clause
            (generic_requirement
                type: (pack_expansion_type
                          pattern_type: (member_type_pack
                                            (simple_type
                                                (type_identifier))
                                            (type_identifier)))
                constraint: (simple_type
                                (generic_type
                                    (generic_argument_list
                                        (pack_reference
                                            (simple_type
                                                (type_identifier))))))))
        body: (block)))

================
@isolated(any) attribute
================

func foo(bar: @isolated(any) (Bar) -> ()) {}

let baz: @isolated(any) () -> Quux

----

(source_file
    (function_declaration
        name: (identifier)
        (parameter_list
            (parameter
                local_name: (identifier)
                type: (function_type
                    (isolation_attribute)
                    (parameter_list
                        (parameter
                            type: (simple_type (type_identifier))))
                    (function_result
                        return_type: (void_type)))))
        body: (block))
    (constant_declaration
        name: (identifier)
        type: (function_type
                  (isolation_attribute)
                  (parameter_list)
                  (function_result
                      return_type: (simple_type (type_identifier))))))

