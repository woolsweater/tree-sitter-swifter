================
Struct declaration
================

struct S {}

struct _Secret
{
    static func _dontTouch() {}
}

struct Foo123 {
}

----

(source_file
    (struct_declaration
        name: (type_identifier) (type_body))
    (struct_declaration
        name: (type_identifier)
        (type_body
            (method_declaration
                (member_modifier)
                name: (identifier)
                (parameter_list)
                body: (block))))
    (struct_declaration
        name: (type_identifier)
        (type_body)))


================
Struct with conformances
================

struct S : Foo, Bar {}

struct Quux : Xyzzy.Waldo {

}

struct S : P & Q {}

----

(source_file
    (struct_declaration
        name: (type_identifier)
        (inheritance_clause
            (simple_type (type_identifier))
            (simple_type (type_identifier)))
        (type_body))
    (struct_declaration
        name: (type_identifier)
        (inheritance_clause
            (simple_type
                (type_identifier)
                (type_identifier)))
        (type_body))
    (struct_declaration
        name: (type_identifier)
        (inheritance_clause
            (protocol_composition_type
                (simple_type (type_identifier))
                (simple_type (type_identifier))))
        (type_body)))


================
Retroactive protocol attribute (SE-0364)
================

extension Date : @retroactive Identifiable {}

----

(source_file
    (extension_declaration
        extended_type: (simple_type (type_identifier))
        (inheritance_clause
            (type_attributes
                (type_attribute))
            (simple_type (type_identifier)))
        (type_body)))


================
Protocol nonconformance
================

struct Foo : ~Copyable, Bar {}

struct Bar<Baz : ~Copyable> {}

struct Quux<Plugh : ~Copyable & Frobozz> : ~Copyable & Mumble {}

----

(source_file
    (struct_declaration
        name: (type_identifier)
        (inheritance_clause
            (protocol_nonconformance
                (simple_type (type_identifier)))
            (simple_type (type_identifier)))
        (type_body))
    (struct_declaration
        name: (type_identifier)
        (generic_parameter_list
            (generic_parameter
                name: (type_identifier)
                requirement: (protocol_nonconformance
                                 (simple_type (type_identifier)))))
        (type_body))
    (struct_declaration
        name: (type_identifier)
        (generic_parameter_list
            (generic_parameter
                name: (type_identifier)
                requirement: (protocol_composition_type
                                 (protocol_nonconformance
                                     (simple_type (type_identifier)))
                                 (simple_type (type_identifier)))))
        (inheritance_clause
            (protocol_composition_type
                (protocol_nonconformance
                    (simple_type (type_identifier)))
                (simple_type (type_identifier))))
        (type_body)))


================
Struct with generics
================

struct Foo<T : Hashable> {}

struct Bar<Baz> where Baz : Quux {}

----

(source_file
    (struct_declaration
        name: (type_identifier)
        (generic_parameter_list
            (generic_parameter
                name: (type_identifier)
                requirement: (simple_type (type_identifier))))
        (type_body))
    (struct_declaration
        name: (type_identifier)
        (generic_parameter_list
            (generic_parameter
                name: (type_identifier)))
        (generic_where_clause
            (generic_requirement
                type: (simple_type (type_identifier))
                constraint: (simple_type (type_identifier))))
        (type_body)))


================
Class declaration
================

class C {}

class ViewController: UIViewController {
}

class _C : A, B, C.X, D, E, F, G
{
}

final class Quux : Xyzzy {

}

open class Superclass {}

----

(source_file
    (class_declaration
        name: (type_identifier)
        (type_body))
    (class_declaration
        name: (type_identifier)
        (inheritance_clause
            (simple_type (type_identifier)))
        (type_body))
    (class_declaration
        name: (type_identifier)
        (inheritance_clause
            (simple_type (type_identifier))
            (simple_type (type_identifier))
            (simple_type
                (type_identifier)
                (type_identifier))
            (simple_type (type_identifier))
            (simple_type (type_identifier))
            (simple_type (type_identifier))
            (simple_type (type_identifier)))
        (type_body))
    (class_declaration
        (reference_decl_modifier)
        name: (type_identifier)
        (inheritance_clause
            (simple_type (type_identifier)))
        (type_body))
    (class_declaration
        (reference_decl_modifier)
        name: (type_identifier)
        (type_body)))


================
Class declaration with generics
================

class Bar<
A,
B : Helper,
C : Another & Another,
D
> {}

class C<T> : D where T : D.E {}

----

(source_file
    (class_declaration
        name: (type_identifier)
        (generic_parameter_list
            (generic_parameter
                name: (type_identifier))
            (generic_parameter
                name: (type_identifier)
                requirement: (simple_type (type_identifier)))
            (generic_parameter
                name: (type_identifier)
                requirement: (protocol_composition_type
                                 (simple_type (type_identifier))
                                 (simple_type (type_identifier))))
            (generic_parameter
                name: (type_identifier)))
        (type_body))
    (class_declaration
        name: (type_identifier)
        (generic_parameter_list
            (generic_parameter
                name: (type_identifier)))
        (inheritance_clause
            (simple_type (type_identifier)))
        (generic_where_clause
            (generic_requirement
                type: (simple_type (type_identifier))
                constraint: (simple_type
                                (type_identifier)
                                (type_identifier))))
        (type_body)))


================
Actor declaration
================

actor A {}

actor A : B, C.D, E, F {}

distributed actor B {}

----

(source_file
    (actor_declaration
        name: (type_identifier)
        (type_body))
    (actor_declaration
        name: (type_identifier)
        (inheritance_clause
            (simple_type (type_identifier))
            (simple_type (type_identifier) (type_identifier))
            (simple_type (type_identifier))
            (simple_type (type_identifier)))
        (type_body))
    (actor_declaration
        (reference_decl_modifier)
        name: (type_identifier)
        (type_body)))


================
Enum declaration
================

enum E {}

enum Tree {
    indirect case node(Tree, payload: Any, Tree)
    case leaf
}

indirect enum Recursive {
    case cdr(Recursive)

    var last: Recursive {
        fatalError()
    }
}

enum Flag {
    case off, on, transient(ttl: TimeInterval)

    func toggle() {}
}

enum Keyword {
     case `switch`
}

----

(source_file
    (enum_declaration
        name: (type_identifier)
        (enum_body))
    (enum_declaration
        name: (type_identifier)
        (enum_body
            (case_declaration
                name: (identifier)
                (associated_values
                    (simple_type (type_identifier))
                    label: (identifier) (simple_type (type_identifier))
                    (simple_type (type_identifier))))
            (case_declaration name: (identifier))))
    (enum_declaration
        name: (type_identifier)
        (enum_body
            (case_declaration
                name: (identifier)
                (associated_values
                    (simple_type (type_identifier))))
            (variable_property_declaration
                name: (identifier)
                type: (simple_type (type_identifier))
                implicit_getter:
                    (block
                        (call_expression
                            function: (identifier)
                            (argument_list))))))
    (enum_declaration
        name: (type_identifier)
        (enum_body
            (case_declaration
                name: (identifier)
                name: (identifier)
                name: (identifier)
                      (associated_values
                          label: (identifier)
                          (simple_type (type_identifier))))
             (method_declaration
                name: (identifier)
                (parameter_list)
                body: (block))))
    (enum_declaration
        name: (type_identifier)
        (enum_body
            (case_declaration
                name: (escaped_identifier)))))


================
Enum raw values
================

enum E : String {
    case foo = "Foo", bar = "Bar"
    case baz = """
               Baz
               """
    case quux = "Quux"
}

enum F : Int {
    case one = 1
    case oneMillion = 1_000_000
}

enum G : Double {
    case pi = 3.14159
}

enum BoolTwo : Bool {
    case true = true
    case false = false
}

----

(source_file
    (enum_declaration
        name: (type_identifier)
        (inheritance_clause
            (simple_type (type_identifier)))
        (enum_body
            (case_declaration
                name: (identifier)
                (raw_value 
                    (string_literal (string_text)))
                name: (identifier)
                (raw_value 
                    (string_literal (string_text))))
            (case_declaration
                name: (identifier)
                (raw_value 
                    (string_literal (string_text))))
            (case_declaration
                name: (identifier)
                (raw_value 
                    (string_literal (string_text))))))
    (enum_declaration
        name: (type_identifier)
        (inheritance_clause
            (simple_type (type_identifier)))
        (enum_body
            (case_declaration
                name: (identifier)
                (raw_value (numeric_literal)))
            (case_declaration
                name: (identifier)
                (raw_value (numeric_literal)))))
    (enum_declaration
        name: (type_identifier)
        (inheritance_clause
            (simple_type (type_identifier)))
        (enum_body
            (case_declaration
                name: (identifier)
                (raw_value (numeric_literal)))))
    (enum_declaration
        name: (type_identifier)
        (inheritance_clause
            (simple_type (type_identifier)))
        (enum_body
            (case_declaration
                name: (identifier)
                (raw_value (boolean_literal)))
            (case_declaration
                name: (identifier)
                (raw_value (boolean_literal))))))


================
Enum with conformances
================

enum E : Foo, Bar {}

enum Tree : Baz, Quux {
   indirect case node(Tree, Tree)
   case leaf(Any)
}

indirect enum Recursive: Xyzzy {
    case cdr
}

----

(source_file
    (enum_declaration
        name: (type_identifier)
        (inheritance_clause
            (simple_type (type_identifier))
            (simple_type (type_identifier)))
        (enum_body))
    (enum_declaration
        name: (type_identifier)
        (inheritance_clause
            (simple_type (type_identifier))
            (simple_type (type_identifier)))
        (enum_body
            (case_declaration
                name: (identifier)
                (associated_values
                    (simple_type (type_identifier))
                    (simple_type (type_identifier))))
            (case_declaration
                name: (identifier)
                (associated_values
                    (simple_type (type_identifier))))))
    (enum_declaration
        name: (type_identifier)
        (inheritance_clause
            (simple_type (type_identifier)))
        (enum_body
            (case_declaration name: (identifier)))))


================
Enum with generics
================

enum Result<Success, Failure : Error> {}

enum Result<Success, Failure> where Failure : Error & RawRepresentable {}

----

(source_file
    (enum_declaration
        name: (type_identifier)
        (generic_parameter_list
            (generic_parameter
                name: (type_identifier))
            (generic_parameter
                name: (type_identifier)
                requirement: (simple_type (type_identifier))))
        (enum_body))
    (enum_declaration
        name: (type_identifier)
        (generic_parameter_list
            (generic_parameter
                name: (type_identifier))
            (generic_parameter
                name: (type_identifier)))
        (generic_where_clause
            (generic_requirement
                type: (simple_type (type_identifier))
                constraint: (protocol_composition_type
                                (simple_type (type_identifier))
                                (simple_type (type_identifier)))))
        (enum_body)))


================
Nested types
================

class Foo {
    protocol Bar {}
}

enum Baz {
    struct Quux {
        class Plugh {}
    }
}

enum Waldo {
    enum Frobozz {}
}

----

(source_file
    (class_declaration
        name: (type_identifier)
        (type_body
            (protocol_declaration
                name: (type_identifier)
                (protocol_body))))
    (enum_declaration
        name: (type_identifier)
        (enum_body
            (struct_declaration
                name: (type_identifier)
                (type_body
                    (class_declaration
                        name: (type_identifier)
                        (type_body))))))
    (enum_declaration
        name: (type_identifier)
        (enum_body
            (enum_declaration
                name: (type_identifier)
                (enum_body)))))


================
Raw identifiers
================

struct `Foo Bar` {
    var `baz quux`: Int
}

extension UIImage {
    static var `10.circle`: UIImage
}

----

(source_file
    (struct_declaration
        name: (type_identifier)
        (type_body
            (variable_property_declaration
                name: (escaped_identifier)
                type: (simple_type (type_identifier)))))
    (extension_declaration
        extended_type: (simple_type (type_identifier))
        (type_body
            (variable_property_declaration
                (member_modifier)
                name: (escaped_identifier)
                type: (simple_type (type_identifier))))))


================
Annotations
================

@objc class Foo: NSObject {}
@objc(XYZBar) class Bar: NSObject {}

@main
struct MyApp: Application {}

@frozen @moana enum E {}

@supercalifragilisticexpialidocious
@veritassi @excelsior
struct Dingbats {

}

@thisMustBeThePlace
protocol Mallard {}

@`default` class Default {}

----

(source_file
    (class_declaration
        (objc_name_attribute)
        name: (type_identifier)
        (inheritance_clause
            (simple_type (type_identifier)))
        (type_body))
    (class_declaration
        (objc_name_attribute
            class_name: (identifier))
        name: (type_identifier)
        (inheritance_clause
            (simple_type (type_identifier)))
        (type_body))
    (struct_declaration
        (attribute name: (identifier))
        name: (type_identifier)
        (inheritance_clause
            (simple_type (type_identifier)))
        (type_body))
    (enum_declaration
        (attribute name: (identifier))
        (attribute
            name: (identifier))
        name: (type_identifier)
        (enum_body))
    (struct_declaration
        (attribute
            name: (identifier))
        (attribute
            name: (identifier))
        (attribute
            name: (identifier))
        name: (type_identifier)
        (type_body))
    (protocol_declaration
        (attribute
            name: (identifier))
        name: (type_identifier)
        (protocol_body))
    (class_declaration
        (attribute
            name: (escaped_identifier))
        name: (type_identifier)
        (type_body)))


================
Annotations with arguments
================

@objc class C : NSObject {
    @objc(doThatThing:)
    func do(thing: NSObject) {}
}

struct Foo {
    @inline(__always) func makeABox() {}
}

@available(iOS 7.0, *, obsoleted: 6.5, message: "Don't use this", renamed: "Foo")
class Bar {}

----

(source_file
    (class_declaration
        (objc_name_attribute)
        name: (type_identifier)
        (inheritance_clause
            (simple_type (type_identifier)))
        (type_body
            (method_declaration
                (objc_name_attribute
                    (objc_method_name
                        label: (identifier)))
                name: (identifier)
                (parameter_list
                    (parameter
                        local_name: (identifier)
                        type: (simple_type (type_identifier))))
                body: (block))))
    (struct_declaration
        name: (type_identifier)
        (type_body
            (method_declaration
                (attribute
                    name: (identifier)
                    (argument_list (argument value: (identifier))))
                name: (identifier)
                (parameter_list)
                body: (block))))
    (class_declaration
        (availability_attribute
            (platform_specifier (platform_name) (version_number))
            (platform_specifier (wildcard))
            (availability_version_detail (version_number))
            (availability_explanation
                (string_literal (string_text)))
            (availability_explanation
                (string_literal (string_text))))
        name: (type_identifier)
        (type_body)))


================
Attributes in inheritance lists
================

struct Foo : @unchecked Sendable {}

----

(source_file
    (struct_declaration
        name: (type_identifier)
        (inheritance_clause
            (type_attributes
                (type_attribute))
            (simple_type (type_identifier)))
        (type_body)))


================
Typealias
================

typealias Foo = Bar.Baz.Quux

typealias PacketDescriptor<T> = (size: Int, encoding: Encoding, body: T)

typealias Coordinator = Delegate & DataSource & Controller

typealias Foo<T> = Bar<T> where T : Hashable

----

(source_file
    (typealias
        name: (type_identifier)
        value: (simple_type
                   (type_identifier)
                   (type_identifier)
                   (type_identifier)))
    (typealias
        name: (type_identifier)
        (generic_parameter_list
            (generic_parameter
                name: (type_identifier)))
        value: (tuple_type
                   label: (identifier) (simple_type (type_identifier))
                   label: (identifier) (simple_type (type_identifier))
                   label: (identifier) (simple_type (type_identifier))))
    (typealias
        name: (type_identifier)
        value: (protocol_composition_type
                   (simple_type (type_identifier))
                   (protocol_composition_type
                       (simple_type (type_identifier))
                       (simple_type (type_identifier)))))
    (typealias
        name: (type_identifier)
        (generic_parameter_list
            (generic_parameter
                name: (type_identifier)))
        value: (simple_type
                   (generic_type
                       (generic_argument_list
                           (simple_type (type_identifier)))))
        (generic_where_clause
            (generic_requirement
                type: (simple_type (type_identifier))
                constraint: (simple_type (type_identifier))))))


================
Multiline generic arguments
================

typealias X = F<
  Int,
  Bool
>

----

(source_file
    (typealias
        (type_identifier)
        (simple_type
            (generic_type
                (generic_argument_list
                    (simple_type (type_identifier))
                    (simple_type (type_identifier)))))))


================
Extensions
================

extension Foo.Bar : Equatable {
    enum Flags {}

    func doThatThing() {}
}

extension Result where Success == Void {}

@available(SwiftStdlib 100.8, *)
extension Foo {}

----

(source_file
    (extension_declaration
        extended_type: (simple_type
                           (type_identifier)
                           (type_identifier))
        (inheritance_clause
            (simple_type (type_identifier)))
        (type_body
            (enum_declaration
                name: (type_identifier)
                (enum_body))
            (method_declaration
                name: (identifier)
                (parameter_list)
                body: (block))))
    (extension_declaration
        extended_type: (simple_type (type_identifier))
        (generic_where_clause
            (generic_requirement
                type: (simple_type (type_identifier))
                constraint: (simple_type (type_identifier))))
        (type_body))
    (extension_declaration
        (availability_attribute
            (platform_specifier
                (platform_name)
                (version_number))
            (platform_specifier (wildcard)))
        extended_type: (simple_type
                           (type_identifier))
        (type_body)))


================
Extensions on bound generic types
================

extension [Foo] {}

extension Bar? {}

extension Quux<Waldo, Plugh> {}

----

(source_file
    (extension_declaration
        (array_type
            (simple_type (type_identifier)))
            (type_body))
    (extension_declaration
        (optional_type
            (simple_type (type_identifier)))
            (type_body))
    (extension_declaration
        (simple_type
            (generic_type
                (generic_argument_list
                    (simple_type (type_identifier))
                    (simple_type (type_identifier)))))
        (type_body)))


================
Member declaration modifiers
================

class C
{
    dynamic class var i: Int

    unowned var other: C
    unowned(safe) var another: C
    unowned(unsafe) var yetAnother: C

    weak var delegate: Delegate

    override func makeABox() {}
}

struct S {
    lazy static var dingus: Dingus

    mutating func foo() {}
}

actor A {
    nonisolated let foo: Foo

    nonisolated func makeABox() {}

    distributed func sendGoodWishes() {}
}

----

(source_file
    (class_declaration
        name: (type_identifier)
        (type_body
            (variable_property_declaration
                (member_modifier)
                (member_modifier)
                name: (identifier)
                type: (simple_type (type_identifier)))
            (variable_property_declaration
                (ownership_modifier)
                name: (identifier)
                type: (simple_type (type_identifier)))
            (variable_property_declaration
                (ownership_modifier)
                name: (identifier)
                type: (simple_type (type_identifier)))
            (variable_property_declaration
                (ownership_modifier)
                name: (identifier)
                type: (simple_type (type_identifier)))
            (variable_property_declaration
                (ownership_modifier)
                name: (identifier)
                type: (simple_type (type_identifier)))
            (method_declaration
                (member_modifier)
                name: (identifier)
                (parameter_list)
                body: (block))))
    (struct_declaration
        name: (type_identifier)
        (type_body
            (variable_property_declaration
                (member_modifier)
                (member_modifier)
                name: (identifier)
                type: (simple_type (type_identifier)))
            (method_declaration
                (member_modifier)
                name: (identifier)
                (parameter_list)
                body: (block))))
    (actor_declaration
        name: (type_identifier)
        (type_body
            (constant_property_declaration
                (member_modifier)
                name: (identifier)
                type: (simple_type (type_identifier)))
            (method_declaration
                (member_modifier)
                name: (identifier)
                (parameter_list)
                body: (block))
            (method_declaration
                (member_modifier)
                name: (identifier)
                (parameter_list)
                body: (block)))))


================
Access modifiers
================

public struct S {
    private var foo: Foo
}

internal class C {}

fileprivate enum E {}

private extension C {
    func doThatThing() {}
}

internal typealias Foo = Bar

public protocol P {}

package struct Quux {}

----

(source_file
    (struct_declaration
        (access_level_modifier)
        name: (type_identifier)
        (type_body
            (variable_property_declaration
                (access_level_modifier)
                name: (identifier)
                type: (simple_type (type_identifier)))))
    (class_declaration
        (access_level_modifier)
        name: (type_identifier)
        (type_body))
    (enum_declaration
        (access_level_modifier)
        name: (type_identifier)
        (enum_body))
    (extension_declaration
        (access_level_modifier)
        extended_type: (simple_type (type_identifier))
        (type_body
            (method_declaration
                name: (identifier)
                (parameter_list)
                body: (block))))
    (typealias
        (access_level_modifier)
        name: (type_identifier)
        value: (simple_type (type_identifier)))
    (protocol_declaration
        (access_level_modifier)
        name: (type_identifier)
        (protocol_body))
    (struct_declaration
        (access_level_modifier)
        name: (type_identifier)
        (type_body)))


================
Member access modifiers
================

class C {
    private(set) var foo: Foo? = nil
}

enum E {
    fileprivate func makeABox() {}
}

struct S {
    public internal(set) var x = 10
}

----

(source_file
    (class_declaration
        name: (type_identifier)
        (type_body
            (variable_property_declaration
                (access_level_modifier)
                name: (identifier)
                type: (optional_type
                          wrapped: (simple_type (type_identifier)))
                initializer: (nil))))
    (enum_declaration
        name: (type_identifier)
        (enum_body
            (method_declaration
                (access_level_modifier)
                name: (identifier)
                (parameter_list)
                body: (block))))
    (struct_declaration
        name: (type_identifier)
        (type_body
            (variable_property_declaration
                (access_level_modifier)
                (access_level_modifier)
                name: (identifier)
                initializer: (numeric_literal)))))


================
Class inheritance modifier nesting
================

final class C : B {
    final class var name: String { "C" }

    final class D {}
}

----

(source_file
    (class_declaration
        (reference_decl_modifier)
        name: (type_identifier)
        (inheritance_clause
            (simple_type (type_identifier)))
        (type_body
            (variable_property_declaration
                (member_modifier)
                (member_modifier)
                name: (identifier)
                type: (simple_type (type_identifier))
                implicit_getter: (block
                                     (string_literal (string_text))))
            (class_declaration
                (reference_decl_modifier)
                name: (type_identifier)
                (type_body)))))


================
Initializer declarations
================

class C {
    required init() throws {}

    convenience override init(foo: Foo) {
        self.foo = foo
    }

    dynamic required init?(coder: NSCoder) {}
}

enum E : String {
    init?(rawValue: String) {
        return nil
    }
}

struct Cow {
    init<T>(t: T) where T : Hashable {}
}

----

(source_file
    (class_declaration
        name: (type_identifier)
        (type_body
            (init_declaration
                (init_modifier)
                (parameter_list)
                body: (block))
            (init_declaration
                (init_modifier)
                (init_modifier)
                (parameter_list
                    (parameter
                        local_name: (identifier)
                        type: (simple_type (type_identifier))))
                body: (block
                          (assignment_expression
                              location: (member_expression
                                            base: (self)
                                            member: (identifier))
                              value: (identifier))))
            (init_declaration
                (init_modifier)
                (init_modifier)
                (parameter_list
                    (parameter
                        local_name: (identifier)
                        type: (simple_type (type_identifier))))
                body: (block))))
    (enum_declaration
        name: (type_identifier)
        (inheritance_clause
            (simple_type (type_identifier)))
        (enum_body
            (init_declaration
                (parameter_list
                    (parameter
                        local_name: (identifier)
                        type: (simple_type (type_identifier))))
                body: (block
                          (return_statement value: (nil))))))
    (struct_declaration
        name: (type_identifier)
        (type_body
            (init_declaration
                (generic_parameter_list
                    (generic_parameter
                        name: (type_identifier)))
                (parameter_list
                    (parameter
                        local_name: (identifier)
                        type: (simple_type (type_identifier))))
                (generic_where_clause
                    (generic_requirement
                        type: (simple_type (type_identifier))
                        constraint: (simple_type (type_identifier))))
                body: (block)))))


================
Deinit
================

class C {
    deinit {
        self.buffer.destroy()
    }
}

----

(source_file
    (class_declaration
        name: (type_identifier)
        (type_body
            (deinit_declaration
                body: (block
                (call_expression
                    function: (member_expression
                                  base: (member_expression
                                              base: (self)
                                              member: (identifier))
                                  member: (identifier))
                    (argument_list)))))))


================
Subscript declarations
================

struct S {
    static subscript(foo: Foo) -> Bar {
        return self.bar(for: foo)
    }

    subscript<T>(name: String) -> T? where T : Codable {
        mutating get { }
        nonmutating set (newValue) { }
    }
}

----

(source_file
    (struct_declaration
        name: (type_identifier)
        (type_body
            (subscript_declaration
                (member_modifier)
                (parameter_list
                    (parameter
                        local_name: (identifier)
                        type: (simple_type (type_identifier))))
                return_type: (simple_type (type_identifier))
                implicit_getter: (block
                    (return_statement
                        value: (call_expression
                                   function: (member_expression
                                                 base: (self)
                                                 member: (identifier))
                                   (argument_list
                                       (argument
                                           label: (identifier)
                                           value: (identifier)))))))
            (subscript_declaration
                (generic_parameter_list
                    (generic_parameter name: (type_identifier)))
                (parameter_list
                    (parameter
                        local_name: (identifier)
                        type: (simple_type (type_identifier))))
                return_type: (optional_type
                                 wrapped: (simple_type (type_identifier)))
                (generic_where_clause
                    (generic_requirement
                        type: (simple_type (type_identifier))
                        constraint: (simple_type (type_identifier))))
                (accessor_block
                    (getter_clause (block))
                    (setter_clause
                        setter_name: (identifier)
                        (block)))))))


================
Operator declarations
================

struct Foo {
    static func == (lhs: Foo, rhs: Foo) -> Bool {
        return false
    }
}

----

(source_file
    (struct_declaration
        name: (type_identifier)
        (type_body
            (method_declaration
                (member_modifier)
                name: (operator_name)
                (parameter_list
                    (parameter
                        local_name: (identifier)
                        type: (simple_type (type_identifier)))
                    (parameter
                        local_name: (identifier)
                        type: (simple_type (type_identifier))))
                (function_result
                    return_type: (simple_type (type_identifier)))
                body: (block
                          (return_statement value: (boolean_literal)))))))


================
Parameter packs
================

struct Foo<each B : Bar, each C> : Bar {
    typealias Baz = (repeat each B.Baz)

    var quux: (repeat each B.Plugh)
    var frobozz: (repeat each C) -> Waldo
}

----

(source_file
    (struct_declaration
        name: (type_identifier)
        (generic_parameter_list
            (generic_parameter
                (parameter_pack
                    name: (type_identifier))
                requirement: (simple_type
                                 (type_identifier)))
            (generic_parameter
                (parameter_pack
                    name: (type_identifier))))
        (inheritance_clause
            (simple_type (type_identifier)))
        (type_body
            (typealias
                name: (type_identifier)
                value: (parenthesized_type
                           (pack_expansion_type
                               pattern_type: (pack_reference
                                                 (simple_type
                                                    (type_identifier)
                                                    (type_identifier))))))
            (variable_property_declaration
                name: (identifier)
                type: (parenthesized_type
                          (pack_expansion_type
                              pattern_type: (pack_reference
                                                (simple_type
                                                    (type_identifier)
                                                    (type_identifier))))))
            (variable_property_declaration
                name: (identifier)
                type: (function_type
                          (parameter_list
                              (parameter
                                  type: (pack_expansion_type
                                            pattern_type:
                                                (pack_reference
                                                    (simple_type
                                                        (type_identifier))))))
                          (function_result
                              return_type: (simple_type
                                               (type_identifier))))))))
