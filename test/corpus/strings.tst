================
Single-line strings
================

"Hello, world!"

"Escaped \"quotes\" are fun"

"Other \'in\teresti\ng\' escapes \r als\0 allowed \u{1f600}!"

"https://cdn.example.com/files/06CCE391-E12A-4257-8EA4-7D98F9BCDAA2"

"/*** This should not be parsed as a comment */"

----

(source_file
    (string_literal (string_text))
    (string_literal
        (string_text)
        (string_escape)
        (string_text)
        (string_escape)
        (string_text))
    (string_literal
        (string_text)
        (string_escape)
        (string_text)
        (string_escape)
        (string_text)
        (string_escape)
        (string_text)
        (string_escape)
        (string_text)
        (string_escape)
        (string_text)
        (string_escape)
        (string_text)
        (string_escape)
        (string_text))
    (string_literal (string_text))
    (string_literal (string_text)))


================
Multi-line strings
================

"""
Hello, world!
"""

"""
"Quotes" do not need to be escaped here.
"""

"""
    And other \' escapes are \r \n valid here \too \0
    of course \u{1f600}
    """

"""
Newlines can also be escaped \
if needed.
"""

----

(source_file
    (string_literal (string_text))
    (string_literal (string_text))
    (string_literal
        (string_text)
        (string_escape)
        (string_text)
        (string_escape)
        (string_text)
        (string_escape)
        (string_text)
        (string_escape)
        (string_text)
        (string_escape)
        (string_text)
        (string_escape)
        (string_text))
    (string_literal (string_text)))


================
String interpolation
================

"\(foo) \(bar.baz())"

"\(date, timeStyle: .short, dateStyle: .full)"

"""
\(foo) \(bar.baz())

\(date, timeStyle: .short, dateStyle: preferredStyle())
"""

"\(foo, bar,)"

"\(flag ? "foo" : "bar \(baz(quux: "plugh"))")"

"""
"\(foo)"\(bar: bar)
"""

"Nested \(#"raw"#) string"

"""
                    a b   c  d    e   f   g  h       hg fe  dc ba
Multiply nested raw \(#"1 \#(##"2 \##(#"3 \#(###"4"###)"#)"##)"#) string
"""

"""
Nesting both plain and raw
a b   c d    e   f   g h   i  j   k l     m    n   o p   q  r   s t tsrq ponm   lkji hgfe  dcba 
\("P1 \(##"1 \##("P2 \(#"2 \#("P3 \(###"3 \###("P4 \(#"4 \#("P5 \("x")")"#)")"###)")"#)")"##)")
"""

----

(source_file
    (string_literal
        (string_interpolation
            expression: (identifier))
        (string_text)
        (string_interpolation
            expression:  (call_expression
                            function: (member_expression
                                          base: (identifier)
                                          member: (identifier))
                            (argument_list))))
    (string_literal
        (string_interpolation
            expression: (identifier)
            label: (identifier)
            expression: (implicit_member_expression (identifier))
            label: (identifier)
            expression: (implicit_member_expression (identifier))))
    (string_literal
        (string_interpolation
            expression: (identifier))
        (string_text)
        (string_interpolation
            expression: (call_expression
                            function: (member_expression
                                          base: (identifier)
                                          member: (identifier))
                            (argument_list)))
        (string_text)
        (string_interpolation
            expression: (identifier)
            label: (identifier)
            expression: (implicit_member_expression (identifier))
            label: (identifier)
            expression: (call_expression
                            function: (identifier)
                            (argument_list)))
        (string_text))
    (string_literal
        (string_interpolation
            expression: (identifier)
            expression: (identifier)))
    (string_literal
        (string_interpolation
            expression: (ternary_conditional_expression
                            condition: (identifier)
                            then: (string_literal (string_text))
                            else:
                            (string_literal
                                (string_text)
                                (string_interpolation
                                    expression: (call_expression
                                        function: (identifier)
                                        (argument_list
                                            (argument
                                                label: (identifier)
                                                value:
                                                (string_literal
                                                    (string_text))))))))))
    (string_literal
        (string_text)
        (string_interpolation
            expression: (identifier))
        (string_text)
        (string_interpolation
            label: (identifier)
            expression: (identifier))
        (string_text))
    (string_literal
        (string_text)
        (string_interpolation
            expression: (raw_string_literal
                            (raw_string_open_delim)
                            (string_text)
                            (raw_string_close_delim)))
        (string_text))
    (string_literal
        (string_text)
        (string_interpolation
            expression:
            (raw_string_literal
                (raw_string_open_delim)
                (string_text)
                (raw_string_interpolation
                    (raw_string_interpolation_open_delim)
                    expression:
                        (raw_string_literal
                            (raw_string_open_delim)
                            (string_text)
                            (raw_string_interpolation
                                (raw_string_interpolation_open_delim)
                                expression:
                                    (raw_string_literal
                                        (raw_string_open_delim)
                                        (string_text)
                                        (raw_string_interpolation
                                            (raw_string_interpolation_open_delim)
                                            expression:
                                                (raw_string_literal
                                                    (raw_string_open_delim)
                                                    (string_text)
                                                    (raw_string_close_delim)))
                                        (raw_string_close_delim)))
                        (raw_string_close_delim)))
            (raw_string_close_delim)))
        (string_text))
    (string_literal
        (string_text)
        (string_interpolation
            expression:
            (string_literal
                (string_text)
                (string_interpolation
                    expression:
                    (raw_string_literal
                        (raw_string_open_delim)
                        (string_text)
                        (raw_string_interpolation
                            (raw_string_interpolation_open_delim)
                            expression:
                            (string_literal
                                (string_text)
                                (string_interpolation
                                    expression:
                                    (raw_string_literal
                                        (raw_string_open_delim)
                                        (string_text)
                                        (raw_string_interpolation
                                            (raw_string_interpolation_open_delim)
                                            expression:
                                            (string_literal
                                                (string_text)
                                                (string_interpolation
                                                    expression:
                                                    (raw_string_literal
                                                        (raw_string_open_delim)
                                                        (string_text)
                                                        (raw_string_interpolation
                                                            (raw_string_interpolation_open_delim)
                                                            expression:
                                                            (string_literal
                                                                (string_text)
                                                                (string_interpolation
                                                                    expression:
                                                                    (raw_string_literal
                                                                        (raw_string_open_delim)
                                                                        (string_text)
                                                                        (raw_string_interpolation
                                                                            (raw_string_interpolation_open_delim)
                                                                            expression:
                                                                            (string_literal
                                                                                (string_text)
                                                                                (string_interpolation
                                                                                    expression:
                                                                                    (string_literal
                                                                                        (string_text)))))
                                                                        (raw_string_close_delim)))))
                                                        (raw_string_close_delim)))))
                                        (raw_string_close_delim)))))
                        (raw_string_close_delim)))))
            (string_text)))


================
Raw strings (single line)
================

###""###

#"""#

#""""""#

######"\"######

#"{ "foo" : "bar" }"#

##"\##'In\##teresti\##ng\##' escapes \##r als\##0 allowed \##u{1f600}!"##

##"Bu\t \'s\#0me\times\" they \#r \not act\u{a1} escapes"##

----

(source_file
    (raw_string_literal
        (raw_string_open_delim)
        (raw_string_close_delim))
    (raw_string_literal
        (raw_string_open_delim)
        (string_text)
        (raw_string_close_delim))
    (raw_string_literal
        (raw_string_open_delim)
        (string_text) (string_text) (string_text) (string_text)
        (raw_string_close_delim))
    (raw_string_literal
        (raw_string_open_delim)
        (string_text)
        (raw_string_close_delim))
    (raw_string_literal
        (raw_string_open_delim)
        (string_text) (string_text) (string_text) (string_text)
        (string_text)
        (raw_string_close_delim))
    (raw_string_literal
        (raw_string_open_delim)
        (raw_string_escape)
        (string_text)
        (raw_string_escape)
        (string_text)
        (raw_string_escape)
        (string_text)
        (raw_string_escape)
        (string_text)
        (raw_string_escape)
        (string_text)
        (raw_string_escape)
        (string_text)
        (raw_string_escape)
        (string_text)
        (raw_string_close_delim))
    (raw_string_literal
        (raw_string_open_delim)
        (string_text) (string_text) (string_text) (string_text)
        (string_text) (string_text) (string_text) (string_text)
        (string_text) (string_text)
        (raw_string_close_delim)))


================
Raw strings (multiline)
================

#"""
"""#

##"""
\\{\n
  "xyzzy"\n
  \\}
"""##

###"""
    Interesting \###' escapes are \###r \###n valid here \###too \###0
    of course \###u{1f600}
    """###

##"""
Bu\t \'s\#0me\times\" they \#r \not act\u{a1} escapes
"""##

----

(source_file
    (raw_string_literal
        (raw_string_open_delim)
        (raw_string_close_delim))
    (raw_string_literal
        (raw_string_open_delim)
        (string_text) (string_text) (string_text) (string_text)
        (string_text) (string_text) (string_text) (string_text)
        (raw_string_close_delim))
    (raw_string_literal
        (raw_string_open_delim)
        (string_text)
        (raw_string_escape)
        (string_text)
        (raw_string_escape)
        (string_text)
        (raw_string_escape)
        (string_text)
        (raw_string_escape)
        (string_text)
        (raw_string_escape)
        (string_text)
        (raw_string_escape)
        (string_text)
        (raw_string_close_delim))
    (raw_string_literal
        (raw_string_open_delim)
        (string_text) (string_text) (string_text) (string_text)
        (string_text) (string_text) (string_text) (string_text)
        (string_text) (string_text)
        (raw_string_close_delim)))


================
Raw string interpolation (single line)
================

#"\#(foo) \#(bar.baz())"#

##"\##(date, timeStyle: .short, dateStyle: .full)"##

###"these \#(however) \##(are: not, .interpolations)"###

#"\#(flag ? "foo" : "bar \(baz(quux: "plugh"))")"#

#"         a  b   c  d   e  f   fe dc ba           "#
#"Nested 1 \#(#"2 \#(#"3 \#(#"4"#)"#)"#) raw string"#

----

(source_file
    (raw_string_literal
        (raw_string_open_delim)
        (raw_string_interpolation
            (raw_string_interpolation_open_delim)
            expression: (identifier))
        (string_text)
        (raw_string_interpolation
            (raw_string_interpolation_open_delim)
            expression:  (call_expression
                            function: (member_expression
                                          base: (identifier)
                                          member: (identifier))
                            (argument_list)))
        (raw_string_close_delim))
    (raw_string_literal
        (raw_string_open_delim)
        (raw_string_interpolation
            (raw_string_interpolation_open_delim)
            expression: (identifier)
            label: (identifier)
            expression: (implicit_member_expression (identifier))
            label: (identifier)
            expression: (implicit_member_expression (identifier)))
        (raw_string_close_delim))
    (raw_string_literal
        (raw_string_open_delim)
        (string_text) (string_text) (string_text)
        (raw_string_close_delim))
    (raw_string_literal
        (raw_string_open_delim)
        (raw_string_interpolation
            (raw_string_interpolation_open_delim)
            expression: (ternary_conditional_expression
                            condition: (identifier)
                            then: (string_literal (string_text))
                            else: (string_literal
                                      (string_text)
                                      (string_interpolation
                                          expression: (call_expression
                                              function: (identifier)
                                              (argument_list
                                                  (argument
                                                      label: (identifier)
                                                      value:
                                                      (string_literal
                                                          (string_text)))))))))
        (raw_string_close_delim))
    (raw_string_literal
        (raw_string_open_delim)
        (string_text)
        (raw_string_close_delim))
    (raw_string_literal
        (raw_string_open_delim)
        (string_text)
        (raw_string_interpolation
            (raw_string_interpolation_open_delim)
            expression:
                (raw_string_literal
                    (raw_string_open_delim)
                    (string_text)
                    (raw_string_interpolation
                        (raw_string_interpolation_open_delim)
                        expression:
                            (raw_string_literal
                                (raw_string_open_delim)
                                (string_text)
                                (raw_string_interpolation
                                    (raw_string_interpolation_open_delim)
                                    expression:
                                        (raw_string_literal
                                            (raw_string_open_delim)
                                            (string_text)
                                            (raw_string_close_delim)))
                                (raw_string_close_delim)))
                    (raw_string_close_delim)))
        (string_text)
        (raw_string_close_delim)))


================
Raw string interpolation (multiline)
================

#####"""
\#####(foo) \#####(bar.baz())

\#####(date, timeStyle: .short, dateStyle: preferredStyle())
"""#####

##"""
\#(foo) \#(bar.baz())

\#(date, timeStyle: .short, dateStyle: preferredStyle())
"""##

#"""
\#(flag ? "foo" : "bar \(baz(quux: "plugh"))")
"""#

#"""
Nested 1 \#(
  ##"2 \##(
    ###"3 \###(
      #"4"#
    )"###
  )"##
) raw string
"""#


----

(source_file
    (raw_string_literal
        (raw_string_open_delim)
        (raw_string_interpolation
            (raw_string_interpolation_open_delim)
            expression: (identifier))
        (string_text)
        (raw_string_interpolation
            (raw_string_interpolation_open_delim)
            expression: (call_expression
                            function: (member_expression
                                          base: (identifier)
                                          member: (identifier))
                            (argument_list)))
        (string_text)
        (raw_string_interpolation
            (raw_string_interpolation_open_delim)
            expression: (identifier)
            label: (identifier)
            expression: (implicit_member_expression
                            (identifier))
            label: (identifier)
            expression: (call_expression
                            function: (identifier)
                            (argument_list)))
        (string_text)
        (raw_string_close_delim))
    (raw_string_literal
        (raw_string_open_delim)
        (string_text) (string_text) (string_text)
        (raw_string_close_delim))
    (raw_string_literal
        (raw_string_open_delim)
        (raw_string_interpolation
            (raw_string_interpolation_open_delim)
            expression: (ternary_conditional_expression
                            condition: (identifier)
                            then: (string_literal (string_text))
                            else: (string_literal
                                      (string_text)
                                      (string_interpolation
                                          expression: (call_expression
                                              function: (identifier)
                                              (argument_list
                                                  (argument
                                                      label: (identifier)
                                                      value:
                                                      (string_literal
                                                          (string_text)))))))))
        (string_text)
        (raw_string_close_delim))
    (raw_string_literal
        (raw_string_open_delim)
        (string_text)
        (raw_string_interpolation
            (raw_string_interpolation_open_delim)
            expression:
                (raw_string_literal
                    (raw_string_open_delim)
                    (string_text)
                    (raw_string_interpolation
                        (raw_string_interpolation_open_delim)
                        expression:
                            (raw_string_literal
                                (raw_string_open_delim)
                                (string_text)
                                (raw_string_interpolation
                                    (raw_string_interpolation_open_delim)
                                    expression:
                                        (raw_string_literal
                                            (raw_string_open_delim)
                                            (string_text)
                                            (raw_string_close_delim)))
                                (raw_string_close_delim)))
                    (raw_string_close_delim)))
        (string_text)
        (raw_string_close_delim)))
