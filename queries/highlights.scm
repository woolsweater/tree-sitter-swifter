;;;--- Punctuation

;;TODO: Order sensitivity; these can probably move down,
;; but not to the "expressions" section.
(access_level_modifier
 "("? @punctuation
 ")"? @punctuation)

(argument_list "," @punctuation)
(argument_list
 ["(" ")"] @punctuation)

(implicit_member_expression
 "." @punctuation)

(parenthesized_expression
 "(" @punctuation
 ")" @punctuation)

;;TODO: This is a workaround for failure to parse `type_expression`s as the
;; `function`s here.
(call_expression
 function: (array_literal
            "[" @punctuation.type
            (_) @type
            "]" @punctuation.type))

(call_expression
 function: (dictionary_literal
            "[" @punctuation.type
            (dictionary_element
              key: (_) @type
              ":" @punctuation.type
              value: (_) @type)
            "]" @punctuation.type))

(array_literal
 "[" @punctuation.special
 "]" @punctuation.special)

(closure_expression
 "{" @punctuation.scope
 "}" @punctuation.scope)

(dictionary_literal
 "[" @punctuation.special
 ":" @punctuation.special
 "]" @punctuation.special)

(member_expression
 "." @punctuation)

(ternary_conditional_expression
 "?" @punctuation.special
 ":" @punctuation.special)

(array_type
 "[" @punctuation.type
 "]" @punctuation.type)

(dictionary_type
 "[" @punctuation.type
 ":" @punctuation.type
 "]" @punctuation.type)

(generic_type
 (generic_argument_list
  "<" @punctuation.type
  ","? @punctuation.type
  ">" @punctuation.type))

(protocol_composition_type "&" @punctuation.type)
(protocol_nonconformance "~" @punctuation.type)

(simple_type "."? @punctuation.type)

(tuple_type
 "(" @punctuation.type
 ","? @punctuation.type
 ")" @punctuation.type)

(function_type
 (parameter_list
  "(" @punctuation.type
  ","? @punctuation.type
  ")" @punctuation.type))

(async_effect) @keyword.type
(throw_effect
 ["rethrows"
  "throws"] @keyword.type)

(function_type
 (function_result "->" @punctuation.type))

(optional_type
 "?" @punctuation.type)

(iuo_type
 "!" @punctuation.type)

(range_expression
 ["..."
  "..<"] @punctuation.special)

(unbounded_range_expression) @punctuation.special

(parenthesized_type
 "(" @punctuation.type
 ")" @punctuation.type)

(subscriptor
 "[" @punctuation
 "]" @punctuation)

;;;--- /Punctuation

[
 "actor"
 "any"
 "as"
 "as?"
 "as!"
 "associatedtype"
 "async"
 "await"
 "case"
 "catch"
 "class"
 "continue"
 "default"
 "defer"
 "didSet"
 "do"
 "else"
 "else if"
 "enum"
 "extension"
 (fallthrough_statement)
 "for"
 "func"
 "get"
 "guard"
 "if"
 "import"
 "in"
 "indirect"
 "inout"
 "is"
 "let"
 "macro"
 "_modify"
 "mutating"
 "nonmutating"
 "optional"
 "protocol"
 "_read"
 "repeat"
 "rethrows"
 "return"
 (self)
 "set"
 "static"
 "struct"
 "subscript"
 (super)
 "switch"
 "throw"
 "throws"
 "try"
 "typealias"
 "unsafeAddress"
 "unsafeMutableAddress"
 "var"
 "where"
 "while"
 "willSet"
 "yield"
] @keyword

(compiler_diagnostic_statement
 ["#error" "#warning"] @keyword.compiler
 ["(" ")"] @keyword.compiler)

[
 "#available"
 "#colorLiteral"
 "#column"
 "#file"
 "#fileID"
 "#fileLiteral"
 "#filePath"
 "#function"
 "#imageLiteral"
 "#line"
 "#unavailable"
 "#sourceLocation"
 ] @keyword.compiler

(compiler_control_statement
 "#if" @keyword.compiler
 "#elseif"? @keyword.compiler
 "#else"? @keyword.compiler
 "#endif" @keyword.compiler)

(postfix_ifconfig
 "#if" @keyword.compiler
 "#elseif"? @keyword.compiler
 "#else"? @keyword.compiler
 "#endif" @keyword.compiler)

(compiler_condition
 (identifier)? @constant.builtin
 ["||" "&&"]? @operator
 (identifier)* @constant.builtin)

(compiler_condition "!" @operator.special)

(line_control_statement
 ["(" "," ":" ")"] @keyword.compiler
 ["file" "line"] @keyword.compiler)

(selector_expression
 "#selector(" @keyword.compiler
 ")" @keyword.compiler)

[
 (arc_modifier)
 (access_level_modifier)
 (init_modifier)
 (isolation_modifier)
 (member_modifier)
 (ownership_modifier)
 (reference_decl_modifier)
 "@unknown"
] @keyword

(type_attribute) @annotation.builtin

(attribute
 name: (_) @name
 (.match? @name "^(?:d(?:ynamic(?:MemberLookup|Replacement|Callable)|is(?:favoredOverload|cardableResult)|eprecated)|_(?:implementationOnly|spi(?:Only)?|weakLinked|exported|private|marker)|no(?:no(?:verride|bjc)|Allocation|Locks)|NS(?:ApplicationMain|Copying|Managed)|a(?:lwaysEmitIntoClient|vailable)|pr(?:opertyWrapper|econcurrency)|i(?:nlin(?:able|e)|mplements)|o(?:bjc(?:Members)?|ptimize)|t(?:ransparent|estable)|IB(?:Action|Outlet)|UIApplicationMain|usableFromInline|ma(?:rker|in)|resultBuilder|globalActor|MainActor|frozen|cdecl)$")
) @annotation.builtin

(attribute
 "@" @annotation
 name: (_) @annotation
 ("(" @annotation ")" @annotation)?)

(objc_name_attribute) @annotation.builtin

(availability_attribute
 (platform_specifier) @keyword.annotation
 ","? @punctuation)
(availability_version_detail
 ["deprecated"
  "noasync"
  "unavailable"] @keyword.annotation)
(availability_explanation
 ["message:"
  "renamed:"] @keyword.annotation)
(availability_attribute) @annotation.builtin

(backdeploy_attribute
 "before:" @keyword.annotation
 (platform_specifier) @keyword.annotation
 ","? @punctuation)
(backdeploy_attribute) @annotation.builtin

(import_statement
 (import_path
  module_name: (identifier) @constant
  submodule_name: (identifier)? @constant))

[
 (array_type)
 (dictionary_type)
 (function_type)
 (iuo_type)
 (opaque_type)
 (optional_type)
 (protocol_composition_type)
 (simple_type)
 (type_identifier)
 (tuple_type)
 (void_type)
] @type

(break_statement "break" @keyword
                 (identifier)? @label)
(continue_statement "continue" @keyword
                    (identifier)? @label)

;;;--- Comments

((line_comment) @contents
 (.match? @contents "^///")) @doc

((block_comment) @contents
 (.match? @contents "^/\\*\\*")) @doc

[(line_comment)
 (block_comment)] @comment

((line_comment) @contents
 (.match? @contents "MARK: ?-")) @comment.special

(shbang) @comment.special

;;;--- Patterns

(as_pattern
 [(escaped_identifier)
  (identifier)] @variable)

(binding_match_pattern
 [(escaped_identifier)
  (identifier)] @variable)

(call_pattern
 function: [(escaped_identifier)
            (identifier)] @variable)

(member_pattern
 base: (_)? @type
 "." @punctuation
 member: (_) @property)

(assoc_value_patterns
 label: [(_) ":"]? @label)

(assoc_value_patterns
 [(escaped_identifier)
  (identifier)] @variable
  ","? @punctuation)

(optional_pattern
 [(escaped_identifier)
  (identifier)]? @variable
 "?" @operator)

(range_pattern
 [(escaped_identifier)
  (identifier)]? @variable
 ["..."
  "..<"] @punctuation.special)

(tuple_pattern
 (tuple_pattern_element
  label: [(_) ":"]? @label
  [(escaped_identifier)
   (identifier)]? @variable)
 "," @punctuation)

;;;--- Literals/constants and variable decls

(numeric_literal) @number

[
 (synthesized_identifier)
 (implicit_closure_argument)
] @variable.synthesized

[(boolean_literal)
 (nil)] @constant.builtin

(closure_expression
 (parameter_list
  (parameter
   name: (identifier) @variable.parameter
   ":"? @variable.parameter)
  ","? @punctuation))

[(string_escape)
 (raw_string_escape)] @escape

(constant_declaration
 name: [(escaped_identifier)
        (identifier)] @variable)
(variable_declaration
 "lazy"? @keyword
 name: [(escaped_identifier)
        (identifier)] @variable)

(argument
 value: (prefix_expression
         (prefix_operator) @operator.special
         (.eq? @operator.special "&")))

(argument
 value: [(escaped_identifier)
         (identifier)] @variable)

(call_expression
 function: [(member_expression)
            (implicit_member_expression)]
 (argument_list
  (argument
   label: (_) @method.call
   ":" @method.call)))

(argument
 label: (_) @function.call
 ":" @function.call)

(call_expression
 function: (identifier) @function.builtin
 (.match? @function.builtin "assert(ionFailure)?"))
(call_expression
 function: (identifier) @function.builtin
 (.eq? @function.builtin "fatalError"))
(call_expression
 function: (identifier) @function.builtin
 (.eq? @function.builtin "precondition(Failure)?"))
(call_expression
 function: (identifier) @constructor
 (.match? @constructor "^[A-Z]"))

(call_expression
 function: [(member_expression
             member: (_) @method.call)
            (implicit_member_expression) @method.call])

(call_expression
 function: [(escaped_identifier)
            (identifier)] @function.call)

(yield_expression
 (prefix_expression
  (prefix_operator) @operator.special
  (.eq? @operator.special "&")))

(ownership_expression
 ["consume"
  "copy"] @keyword)

;;;--- Type decls

(type_identifier) @type

(tuple_type
 label: (_) @property.definition)

(parameter_pack "each" @keyword.type)
(pack_expansion_type "repeat" @keyword.type "each"? @keyword.type)
(pack_reference "each" @keyword.type)

;;-- Type member decls

(case_declaration
 name: [(identifier) (escaped_identifier)] @property.definition)

(associated_values
 label: (_) @property.definition)

(property_requirement
 ;;TODO: Including the label here suddenly produces an error when testing... :/
 ;;name:
 [(identifier)
  (escaped_identifier)] @property.definition
 "get" @keyword
 "nonmutating"? @keyword
 "set"? @keyword
 )
(method_requirement
 name: (identifier) @method
 (function_result
  "->" @method)?)
(operator_requirement
 name: (operator_name) @method
 (function_result
  "->" @method)?)
(init_requirement
 "init" @function
 "?"? @function
 "!"? @operator.special)

(init_declaration
 "init" @function
 "?"? @function
 "!"? @operator.special)

(deinit_declaration
 "deinit" @function)

(function_declaration
 ["infix" "postfix" "prefix"]? @keyword
 name: [(escaped_identifier)
        (identifier)
        (operator_name)] @function
 (function_result
  "->" @function)?
)

(macro_declaration
 name: [(escaped_identifier)
        (identifier)] @function
 (function_result
  "->" @function)?
  (macro_definition
   "=" @function)
)

(method_declaration
 ["infix" "postfix" "prefix"]? @keyword
 name: [(escaped_identifier)
        (identifier)
        (operator_name)] @function
 (function_result
  "->" @function)?
)

(generic_parameter_list
 "<" @function
 ">" @function)

;; Color the externally-visible name, whichever
;; label it is.
(parameter
 external_name: [(escaped_identifier)
                 (identifier)] @function
 local_name: ((_) @default
              ":" @function))

(parameter
 local_name: [(escaped_identifier)
              (identifier)] @function
 ":" @function)
;;

(parameter_list "(" @function ")" @function)

(variable_property_declaration
 name: [(escaped_identifier)
        (identifier)] @property.definition
 type: (_)? @type)

(constant_property_declaration
 name: [(escaped_identifier)
        (identifier)] @property.definition
 type: (_)? @type)

;;;--- Other decls

(operator_declaration
 ["infix" "postfix" "prefix"]? @keyword
 "operator" @keyword)

[(operator_name)
 (operator_reference)] @operator

(precedencegroup_declaration
 "precedencegroup" @keyword
 name: (_) @type)

(precedencegroup_assignment
 "assignment" @keyword)

(precedencegroup_associativity
 "associativity" @keyword
 ["left" "right" "none"] @constant.builtin)

(precedencegroup_relation
 ["higherThan" "lowerThan"] @keyword
 group: (_) @type)

;;;--- Expressions

(pack_expansion_expression "repeat" @keyword)
(pack_reference_expression "each" @keyword)

((postfix_operator) @operator.special
 (.eq? @operator.special "!"))

((prefix_operator) @operator.special
 (.eq? @operator.special "!"))

(prefix_expression
 (prefix_operator) @number
 (.match? @number "^[+-]$")
 (numeric_literal))

[(binary_operator)
 (postfix_operator)
 (prefix_operator)] @operator

(statement_label) @label

(key_path_expression
 "\\" @variable.synthesized)

;;TODO: Highlighting here is a little weird: base type is colored as a type
;; _unless_ there's a generic arg list.
;; ??? Also trying to include `(numeric_literal)?
;;@number' produces an "impossible pattern" warning when testing.
(key_path_base) @variable.synthesized

(key_path_segment
 "." @punctuation
 subscript: (numeric_literal)? @number
 ["?" "!"]? @variable.synthesized
) @variable.synthesized

(key_path_string_expression
 "#keyPath" @keyword.compiler
 ["(" ")"] @keyword.compiler)

;; Lower precedence than an `identifier` in a `parameter`
((identifier) @name
 (.eq? @name "_")) @constant.builtin

(implicit_member_expression
 [(escaped_identifier)
  (identifier)] @property)

(member_expression
 member: [(escaped_identifier)
          (identifier)] @property)

(regex_literal) @string

(macro_expansion_expression
 "#" @keyword.compiler
 name: [(escaped_identifier)
        (identifier)] @keyword.compiler
 ["(" ")"]? @keyword.compiler)

(try_expression
 "?"? @operator)

(try_expression
 "!"? @operator.special)

(tuple_expression
 label: (_) @label)

(type_expression) @type

;;;--- Miscellany

;;TODO: This probably needs to be more targeted?
([(escaped_identifier)
  (identifier)] @name
 (.match? @name "^`?[A-Z].*[a-z].*")) @type

;;;--- Strings

(string_interpolation
 "\\(" @escape
 label: (_)? @label
 label: ":"? @label
 ")" @escape .)

(raw_string_interpolation
 (raw_string_interpolation_open_delim) @escape
 label: (_)? @label
 label: ":"? @label
 ")" @escape .)

(string_text) @string

(string_literal
 . "\"" @string
 "\"" @string .)

(string_literal
 . "\"\"\"" @string
 "\"\"\"" @string .)

(raw_string_literal
 . "#" @string
 . (raw_string_open_delim) @string
 (raw_string_close_delim) @string)
