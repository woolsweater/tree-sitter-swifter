;;;--- Declarations

[
 (class_declaration)
 (closure_expression)
 (enum_declaration)
 (extension_declaration)
 (function_declaration)
 (protocol_declaration)
 (struct_declaration)
 (init_declaration)
 (deinit_declaration)
 (subscript_declaration)
 ;; (operator_declaration)
] @local.scope

(constant_declaration
 name: (identifier) @local.definition)
(constant_property_declaration
 name: (identifier) @local.definition)
(variable_declaration
 name: (identifier) @local.definition)
(variable_property_declaration
 name: (identifier) @local.definition)

;;;--- Statements
;;TODO: Scope + definitions
