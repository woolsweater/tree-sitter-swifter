// -*- tree-sitter-grammar -*-

module.exports = grammar({
    name: "swift",

    word: $ => $.identifier,

    conflicts: $ => [
        // 'final class' can start a class decl or a property/method decl
        [$.reference_decl_modifier, $.member_modifier],
        // 'override' can be either of these
        [$.init_modifier, $.member_modifier],
        // Need to find the 'in' of the closure before deciding
        [$._primary_expression, $.closure_parameter],

        // Repeated literal closures inside literal closures are hard to reduce
        [$.call_expression, $.closure_expression],

        [$._function_type_parameter, $.parenthesized_type],
        [$._function_type_parameter, $._primary_expression],
        [$._function_type_parameter, $._tuple_type_element],

        [$._primary_expression, $.simple_type],
        [$.type_expression, $.simple_type],
        [$._primary_expression, $.type_expression, $.simple_type],

        [$.pack_reference_expression, $.simple_type],

        [$.member_type_pack, $.pack_reference],

        [$._expression, $.capture_item],

        [$.void_literal, $.void_type],
    ],

    precedences: $ => [
        [
            'closure_parameter_list',
            'void_literal',
        ],
        [
            'void_literal',
            'call_expression',
        ],
        [
            'type_expression',
            'call_expression',
        ],
        [
            'range_expression',
            '_primary_expression',
            '_literal',
        ],
        [
            'range_expression',
            '_primary_expression',
            'type_expression',
        ],
        [
            'parameter_list',
            'void_type',
        ],
        [
            '_full_func_type_param_list',
            'void_type',
        ],
        [
            'postfix_expression',
            'member_expression',
            'subscript_expression',
            'call_expression',
            'prefix_expression',
            '_property_binding',
            'await_expression',
            'assignment_expression',
        ],
        [
            'binary_expression',
            'subscript_expression',
        ],
    ],

    externals: $ => [
        $._automatic_semicolon,

        $._string_text,
        $._multiline_string_text,

        $._raw_oneline_string_open_delim,
        $._raw_multiline_string_open_delim,

        $._raw_oneline_string_escape,
        $._raw_oneline_interpolation_open_delim,
        $._raw_oneline_string_text,

        $._raw_multiline_string_escape,
        $._raw_multiline_interpolation_open_delim,
        $._raw_multiline_string_text,

        $._raw_oneline_string_close_delim,
        $._raw_multiline_string_close_delim,

        $._block_comment_contents,

        $.binary_operator,
        $.operator_name,
        $.prefix_operator,
        $.postfix_operator,

        ///
        // Zero-width rules that just look ahead to disambiguate

        $._valid_function_ref_params,
        $._variable_property_observer_block,
        // This must be in the external scanner so that it can be prioritized
        // over `binary_operator` in an expression like `foo as Bar<Baz>`
        $._generic_arg_open_delim,
        $._multiple_trailing_closures,
        $._return_value,

        // On an error the external scanner will be invoked with _all_ tokens
        // marked in `valid_tokens`. This is just a sentinel to detect that
        // situation.
        $.error_recovery,
    ],

    inline: $ => [
        $._any_identifier,
        $._composable_type,
        $._constant_decl_common,
        $._function_method_common,
        $._function_type_parameter_list,
        $._initializer,
        $._label,
        $._member_member,
        $._operator_fixity,
        $._parameter_type,
        $._property_binding,
        $._property_initializer,
        $._protocol_member_decl,
        $._range_bound,
        $._range_pattern_bound,
        $._r_expression,
        $._top_level_modifier,
        $._type_identifier,
        $._type_member_declaration,
        $._variable_property_common,
    ],

    extras: $ => [
        /\s/,
        $.line_comment,
        $.block_comment,
    ],

    rules: {
        source_file: $ => seq(
            optional($.shbang),
            repeat($._statement)
        ),

        shbang: $ => seq(
            '#!',
            $.interpreter_path,
            repeat($.interpreter_arg),
            '\n',
        ),

        interpreter_path: $ => prec.right(token(seq(
            optional('/'),
            repeat1_with_separator(
                /[^/\s]+/,
                '/'
            ),
        ))),

        interpreter_arg: $ => /-*[-_\w\d=]+/,

        _statement: $ => choice(
            $._compiler_directive,
            $._control_statement,
            $._declaration,
            $.defer_statement,
            $.discard_statement,
            $.do_statement,
            $.empty_statement,
            $._expression_statement,
            $.for_in_statement,
            $.if_statement,
            $.import_statement,
            $.guard_statement,
            $.labelled_statement,
            $.repeat_while_statement,
            $.switch_statement,
            $.while_statement,
        ),

        _compiler_directive: $ => choice(
            alias($._top_level_compiler_control_statement,
                  $.compiler_control_statement),
            $.compiler_diagnostic_statement,
            $.line_control_statement,
        ),

        _top_level_compiler_control_statement: $ => compiler_control_rule(
            $, $._statement,
        ),

        compiler_condition: $ => repeat1_with_separator(
            $._compiler_condition_element,
            choice('&&', '||')
        ),

        _compiler_condition_element: $ => choice(
            $.boolean_literal,
            $.identifier,
            seq('!', $._compiler_condition_element),
            seq('(', $._compiler_condition_element, ')'),
            $._functionlike_compiler_condition,
        ),

        _functionlike_compiler_condition: $ => choice(
            $.arch_compiler_condition,
            $.attribute_compiler_condition,
            $.environment_compiler_condition,
            $.feature_compiler_condition,
            $.import_compiler_condition,
            $.language_compiler_condition,
            $.os_compiler_condition,
        ),

        arch_compiler_condition: $ => seq(
            'arch',
            '(',
            field('value', choice('i386', 'x86_64', 'arm', 'arm64')),
            ')',
        ),

        attribute_compiler_condition: $ => seq(
            'hasAttribute',
            '(',
            $.identifier,
            ')',
        ),

        environment_compiler_condition: $ => seq(
            'targetEnvironment',
            '(',
            field('value', choice('simulator', 'macCatalyst')),
            ')',
        ),

        feature_compiler_condition: $ => seq(
            'hasFeature',
            '(',
            field('feature_name', $.identifier),
            ')'
        ),

        import_compiler_condition: $ => seq(
            'canImport',
            '(',
            alias($.identifier, $.module_name),
            ')',
        ),

        language_compiler_condition: $ => seq(
            field('component', choice('swift', 'compiler')),
            '(',
            field('operator', choice('>=', '<')),
            $.version_number,
            ')'
        ),

        os_compiler_condition: $ => seq(
            'os',
            '(',
            field('name', choice('macOS', 'iOS', 'watchOS', 'tvOS', 'Linux')),
            ')',
        ),

        compiler_diagnostic_statement: $ => seq(
            choice(
                '#error',
                '#warning',
            ),
            '(',
            $.string_literal,
            ')',
            $._semicolon,
        ),

        _control_statement: $ => choice(
            $.break_statement,
            $.continue_statement,
            $.fallthrough_statement,
            $.return_statement,
            $.throw_statement,
        ),

        break_statement: $ => prec.right(seq(
            'break',
            optional(field('label', $._any_identifier)),
            $._semicolon,
        )),

        continue_statement: $ => prec.right(seq(
            'continue',
            optional(field('label', $._any_identifier)),
            $._semicolon,
        )),

        fallthrough_statement: $ => prec.right(seq(
            'fallthrough',
            $._semicolon,
        )),

        return_statement: $ => prec.right(seq(
            'return',
            optional(seq(
                $._return_value,
                field('value', $._r_expression),
            )),
            $._semicolon,
        )),

        throw_statement: $ => prec.right(seq(
            'throw',
            field('value', $._expression),
            $._semicolon,
        )),

        for_in_statement: $ => seq(
            'for',
            field('pattern', choice(
                $.async_sequence_pattern,
                $.identifier,
                seq('case', $._matching_pattern),
                alias($._for_in_tuple_pattern, $.tuple_pattern),
            )),
            'in',
            field('sequence',
                  choice(
                      $._expression,
                      $.pack_reference_expression,
                  )),
            optional($.where_clause),
            field('body', $.block),
        ),

        _for_in_tuple_pattern: $ => seq(
            '(',
            repeat2_with_separator(
                alias($._for_in_tuple_pattern_element, $.tuple_element),
                ','
            ),
            ')',
        ),

        _for_in_tuple_pattern_element: $ => seq(
            optional(field('label', $._label)),
            $._any_identifier,
        ),

        async_sequence_pattern: $ => seq(
            optional('try'),
            'await',
            field('binding', $.identifier),
        ),

        import_statement: $ => prec.right(seq(
            optional($._attributes),
            optional($.access_level_modifier),
            'import',
            //TODO: Make item imports their own node? The 'module_name' label in
            // `import_path` doesn't really fit them
            optional($.import_kind),
            $.import_path,
            $._semicolon,
        )),

        import_kind: $ => choice(
            'actor',
            'class',
            'enum',
            'func',
            'let',
            'protocol',
            'struct',
            'typealias',
            'var',
        ),

        import_path: $ => seq(
            field('module_name', $._any_identifier),
            optional(seq(
                '.',
                repeat1_with_separator(
                    field('submodule_name', $._any_identifier),
                '.'
            ))),
        ),

        if_statement: $ => seq(
            'if',
            field('condition', $._condition_list),
            field('body', $.block),
            repeat($.else_if_statement),
            optional($.else_statement),
        ),

        else_if_statement: $ => seq(
            'else if',
            field('condition', $._condition_list),
            field('body', $.block),
        ),

        else_statement: $ => seq(
            'else',
            field('body', $.block)
        ),

        guard_statement: $ => seq(
            'guard',
            field('condition', $._condition_list),
            'else',
            field('body', $.block),
        ),

        labelled_statement: $ => seq(
            $.statement_label,
            choice(
                $.for_in_statement,
                $.if_statement,
                $.repeat_while_statement,
                $.switch_statement,
                $.while_statement,
            ),
        ),

        repeat_while_statement: $ => prec.right(seq(
            'repeat',
            field('body', $.block),
            'while',
            // Repeat-while only allows a single expression
            field('condition', $._expression),
            $._semicolon,
        )),

        switch_statement: $ => seq(
            'switch',
            field('expression', $._expression),
            '{',
            either_or_both(
                repeat($.switch_case),
                $.switch_default
            ),
            '}',
        ),

        switch_case: $ => prec.right(seq(
            'case',
            comma_list_allow_trailing($.match_descriptor),
            ':',
            $.case_body,
        )),

        match_descriptor: $ => seq(
            field('pattern', $._matching_pattern),
            optional($.where_clause)
        ),

        case_body: $ => prec.right(repeat1($._statement)),

        switch_default: $ => seq(
            optional($._attributes),
            optional('@unknown'),
            'default',
            ':',
            $.case_body,
        ),

        while_statement: $ => seq(
            'while',
            field('condition', $._condition_list),
            field('body', $.block),
        ),

        _declaration: $ => choice(
            $.actor_declaration,
            $.class_declaration,
            $.constant_declaration,
            $.enum_declaration,
            $.extension_declaration,
            $.function_declaration,
            $.macro_declaration,
            $.operator_declaration,
            $.precedencegroup_declaration,
            $.protocol_declaration,
            $.struct_declaration,
            $.typealias,
            $.variable_declaration,
        ),

        // Lenient: formally `deinit` only applies to classes and noncopyable
        // types, and `case_declaration` only to enums
        _type_member_declaration: $ => choice(
            $.case_declaration,
            $.class_declaration,
            $._type_member_compiler_directive,
            $.constant_property_declaration,
            $.deinit_declaration,
            $.enum_declaration,
            $.init_declaration,
            $.method_declaration,
            $.protocol_declaration,
            $.struct_declaration,
            $.subscript_declaration,
            $.typealias,
            $.variable_property_declaration,
        ),

        _type_member_compiler_directive: $ => choice(
            alias($._type_member_compiler_control_statement,
                  $.compiler_control_statement),
            $.compiler_diagnostic_statement,
            $.line_control_statement,
        ),

        _type_member_compiler_control_statement: $ => compiler_control_rule(
            $, $._type_member_declaration,
        ),

        line_control_statement: $ => seq(
            '#sourceLocation',
            '(',
            optional(seq(
                'file', ':',
                field('path', $.string_literal),
                ',',
                'line', ':',
                field('line', $.numeric_literal),
            )),
            ')',
            $._semicolon,
        ),

        defer_statement: $ => seq('defer', field('body', $.block)),

        discard_statement: $ => seq(
            'discard',
            field('operand', $.self),
        ),

        do_statement: $ => seq(
            'do',
            optional($.throw_effect),
            field('body', $.block),
            repeat($.catch_clause),
        ),

        catch_clause: $ => seq(
            'catch',
            optional(
                repeat1_with_separator(
                    alias($.match_descriptor, $.catch_pattern),
                    ','
                )
            ),
            field('body', $.block),
        ),

        empty_statement: $ => ';',

        //TODO: #if inside expressions (SE-0308)
        _expression: $ => choice(
            $.array_literal,
            $.await_expression,
            $.binary_expression,
            $.call_expression,
            $.cast_expression,
            $.compiler_expression,
            $.dictionary_literal,
            $.member_expression,
            $.ownership_expression,
            $.pack_expansion_expression,
            $.postfix_expression,
            $.prefix_expression,
            $._primary_expression,
            $.range_expression,
            $.regex_literal,
            $.subscript_expression,
            $.ternary_conditional_expression,
            $.try_expression,
            $.yield_expression,
        ),

        // Superset of expressions that are legal or unambiguous only in certain
        // RHS-like positions.
        _r_expression: $ => choice(
            $.assignment_expression,
            $.closure_expression,
            $._expression,
            $.function_reference,
            alias($.if_statement, $.if_expression),
            $.operator_reference,
            $.pack_reference_expression,
            alias($.switch_statement, $.switch_expression),
            $.unbounded_range_expression,
        ),

        assignment_expression: $ => prec.left('assignment_expression', seq(
            field('location', $._expression),
            '=',
            field('value', $._r_expression),
        )),

        await_expression: $ => prec.right('await_expression', seq(
            'await',
            $._expression,
        )),

        binary_expression: $ => prec.right('binary_expression', seq(
            field('lhs', $._expression),
            $.binary_operator,
            field('rhs', $._expression),
        )),

        call_expression: $ => prec.right('call_expression', seq(
            field('function',
                  choice(
                      $.closure_expression,
                      $._expression,
                      prec.dynamic(1, $.type_expression),
                      //TODO:
                      // $.operator_reference,
                  )),
            either_or_both(
                $.argument_list,
                $.trailing_closures
            ),
        )),

        argument_list: $ => seq(
            // Don't let the paren be used for a tuple
            token(prec(1, '(')),
            optional(comma_list_allow_trailing($.argument)),
            ')'
        ),

        argument: $ => seq(
            optional(field('label', $._label)),
            field('value', $._r_expression),
        ),

        trailing_closures: $ => prec.right(seq(
            $.closure_expression,
            optional(seq(
                $._multiple_trailing_closures,
                repeat1($.trailing_closure),
            )),
        )),

        trailing_closure: $ => seq(
            alias($.statement_label, $.label),
            $.closure_expression,
        ),

        cast_expression: $ => prec.right(seq(
            field('operand', $._expression),
            choice('is', 'as', 'as!', 'as?'),
            field('type', $._type),
        )),

        closure_expression: $ => seq(
            // Give `block` precedence for the opening brace
            token(prec(-1, '{')),
            optional($._closure_header),
            optional(alias($._closure_body, $.body)),
            '}',
        ),

        _closure_body: $ => repeat1(choice(
            $._statement,
            $.closure_expression,
        )),

        _closure_header: $ => seq(
            optional($.attribute),
            either_or_both(
                $.capture_list,
                alias($.closure_parameter_list, $.parameter_list),
            ),
            optional($.function_result),
            'in',
        ),

        capture_list: $ => seq(
            '[',
            comma_list_allow_trailing($.capture_item),
            ']',
        ),

        capture_item: $ => seq(
            optional($.ownership_modifier),
            field('name', $._primary_expression),
            optional($._initializer),
        ),

        closure_parameter_list: $ => choice(
            $._bare_closure_parameter_list,
            $._paren_closure_parameter_list,
        ),

        _bare_closure_parameter_list: $ => repeat1_with_separator(
            alias($._bare_closure_parameter, $.parameter),
            ','
        ),

        _bare_closure_parameter: $ => field('name', $._any_identifier),

        _paren_closure_parameter_list: $ => seq(
            '(',
            optional(comma_list_allow_trailing(
                alias($.closure_parameter, $.parameter)
            )),
            ')'
        ),

        closure_parameter: $ => seq(
            // Property wrappers (SE-0293)
            repeat($.attribute),
            optional('_'),
            field('name', choice(
                $._any_identifier,
                $.synthesized_identifier,
            )),
            optional(seq(
                ':',
                field('type', $._parameter_type),
            ))
        ),

        compiler_expression: $ => choice(
            $.availability_condition,
            '#column',
            '#dsohandle',
            '#file',
            '#fileID',
            '#filePath',
            '#function',
            '#line',
            $._xcode_literal
        ),

        availability_condition: $ => seq(
            choice('#available', '#unavailable'),
            '(',
            repeat1_with_separator($.platform_specifier, ','),
            ')',
        ),

        _xcode_literal: $ => seq(
            choice(
                '#colorLiteral',
                '#fileLiteral',
                '#imageLiteral',
            ),
            $.argument_list,
        ),

        ownership_expression: $ => seq(
            field('operator',
                  choice(
                      'consume',
                      'copy',
                  )),
            field('operand',
                  choice(
                      $.identifier,
                      $.self,
                  )
             )
        ),

        implicit_closure_argument: $ => /\$\d+/,

        key_path_expression: $ => prec.right(seq(
            '\\',
            optional($.key_path_base),
            repeat1($.key_path_segment),
        )),

        key_path_base: $ => prec.right(seq(
            $.type_expression,
            optional(field('subscript', $.subscriptor)),
        )),

        key_path_segment: $ => prec.right(seq(
            '.',
            choice(
                seq(
                    choice(
                        '?',
                        $._key_path_segment_ident,
                    ),
                    repeat(field('subscript', $._key_path_segment_subscript)),
                ),
                repeat1(field('subscript', $._key_path_segment_subscript)),
            ),
        )),

        _key_path_segment_ident: $ => seq(
            $._any_identifier,
            token.immediate(optional(choice('?', '!'))),
        ),

        _key_path_segment_subscript: $ => seq(
            $.subscriptor,
            token.immediate(optional(choice('?', '!'))),
        ),

        key_path_string_expression: $ => seq(
            '#keyPath',
            '(',
            optional(seq(
                field('root', $.type_expression),
                '.',
            )),
            field('value', $._any_identifier),
            ')',
        ),

        //TODO: Subsume `colorLiteral` and others into this
        macro_expansion_expression: $ => prec.right(seq(
            '#',
            field('name', $._any_identifier),
            optional($.generic_argument_list),
            optional($.argument_list),
            optional($.trailing_closures),
        )),

        member_expression: $ => prec.left('member_expression', seq(
            field('base', choice(
                $._expression,
                $.type_expression,
            )),
            field('member',
                choice(
                    $.postfix_ifconfig,
                    $._member_member,
                )),
        )),

        postfix_ifconfig: $ => seq(
            '#if',
            $.compiler_condition,
            $._member_member,
            repeat(seq(
                '#elseif',
                $.compiler_condition,
                $._member_member,
            )),
            optional(seq(
                '#else',
                $._member_member
            )),
            '#endif',
        ),

        _member_member: $ => seq(
            '.',
            choice(
                $._expression,
                $.function_reference,
                $.operator_reference,
                $.type_expression,
            ),
        ),

        operator_reference: $ => alias($.operator_name, 'operator_reference'),

        parenthesized_expression: $ => seq(
            '(',
            $._r_expression,
            ')'
        ),

        pack_expansion_expression: $ => prec.right(seq(
            'repeat',
            choice(
                $.pack_reference_expression,
                $._expression,
            ),
        )),

        pack_reference_expression: $ => seq(
            'each',
            $._any_identifier,
        ),

        postfix_expression: $ => prec.left('postfix_expression', seq(
            field('operand', $._expression),
            $.postfix_operator,
        )),

        prefix_expression: $ => prec.left('prefix_expression', seq(
            $.prefix_operator,
            field('operand', $._expression),
        )),

        _primary_expression: $ => prec('_primary_expression', choice(
            // Note: enum cases are not distinguishable by syntax alone.
            // They'll be parsed as either `member_expression` or
            // `implicit_member_expression`. If there are associated values, a
            // `call_expression` will be the `member`.
            $.escaped_identifier,
            $.identifier,
            $.implicit_member_expression,
            $.implicit_closure_argument,
            $.key_path_expression,
            $.key_path_string_expression,
            $._literal,
            $.macro_expansion_expression,
            $.parenthesized_expression,
            $.selector_expression,
            $.self,
            $.super,
            $.synthesized_identifier,
            $.tuple_expression,
        )),

        _any_identifier: $ => choice(
            $.escaped_identifier,
            $.identifier,
        ),

        _type_identifier: $ => alias($._any_identifier, $.type_identifier),

        array_literal: $ => choice(
            seq('[', ']'),
            seq('[',
                comma_list_allow_trailing($._r_expression),
                ']',
               ),
        ),

        dictionary_literal: $ => choice(
            seq('[', ':', ']'),
            seq('[',
                comma_list_allow_trailing($.dictionary_element),
                ']'
               ),
        ),

        dictionary_element: $ => seq(
            field('key', $._expression),
            ':',
            field('value', $._r_expression),
        ),

        escaped_identifier: $ => seq(
            '`',
            alias($._raw_identifier_contents, 'name'),
            '`'
        ),

        _raw_identifier_contents: $ => {
            // Disallow whitespace except: U+0020, U+200E, U+200F
            const chars = /[^`\\\u0000-\u001F\u007F\u0085\u2028-\u2029]/;
            return token(repeat1(chars));
        },

        function_reference: $ => seq(
            field('name', $._any_identifier),
            $._valid_function_ref_params,
            $.function_reference_parameter_list,
        ),

        implicit_member_expression: $ => seq(
            '.',
            $._any_identifier,
        ),

        _literal: $ => prec('_literal', choice(
            $.boolean_literal,
            $.nil,
            $.numeric_literal,
            $.raw_string_literal,
            $.string_literal,
            $.void_literal,
        )),

        selector_expression: $ => seq(
            '#selector(',
            $._selector_method,
            ')'
        ),

        // Distinct from `function_reference` because of the optional
        // parameter list (technique for partial context sensitivity suggested
        // in https://github.com/tree-sitter/tree-sitter/issues/697).
        _selector_method: $ => seq(
            repeat(seq(
                $.type_expression,
                '.',
            )),
            field('name', $._any_identifier),
            optional($.function_reference_parameter_list),
            optional(field('explicit_type', seq(
                'as',
                $.function_type,
            ))),
        ),

        function_reference_parameter_list: $ => seq(
            '(',
            repeat1(field('parameter', $._label)),
            ')'
        ),

        self: $ => choice('Self', 'self'),

        super: $ => 'super',

        // Names like the "projection" of a property wrapper, that are created
        // only by the compiler.
        synthesized_identifier: $ => seq(
            '$', alias($._any_identifier, 'synthesized_name'),
        ),

        tuple_expression: $ => seq(
            '(',
            comma_list_2_allow_trailing($._tuple_element),
            ')'
        ),

        _tuple_element: $ => seq(
            optional(field('label', $._label)),
            $._r_expression,
        ),

        boolean_literal: $ => choice('true', 'false'),

        nil: $ => 'nil',

        numeric_literal: $ => choice(
            // Binary
            /-?0b[01][_01]*/,
            // Decimal
            /[+-]?\d[_\d]*(\.\d[_\d]*)?([eE][+-]?\d[_\d]*)?/,
            // Hexadecimal
            /[+-]?0x[\da-fA-F][_\da-fA-F]*?(\.[_\da-fA-F]+)?([pP][+-]?\d[_\d]*)?/,
            // Octal
            /-?0o[0-7][_0-7]*/,
        ),

        raw_string_literal: $ => choice(
            $._raw_oneline_string_literal,
            $._raw_multiline_string_literal,
        ),

        _raw_oneline_string_literal: $ => seq(
            '#',
            alias($._raw_oneline_string_open_delim,
                  $.raw_string_open_delim),
            repeat(choice(
                alias($._raw_oneline_string_escape, $.raw_string_escape),
                alias($._raw_string_interpolation, $.raw_string_interpolation),
                alias($._raw_oneline_string_text, $.string_text),
            )),
            alias($._raw_oneline_string_close_delim,
                  $.raw_string_close_delim),
        ),

        _raw_string_interpolation: $ => seq(
            alias($._raw_oneline_interpolation_open_delim,
                  $.raw_string_interpolation_open_delim),
            repeat1_with_separator($._interpolated_item, ','),
            ')'
        ),

        _raw_multiline_string_literal: $ => seq(
            '#',
            alias($._raw_multiline_string_open_delim,
                  $.raw_string_open_delim),
            repeat(choice(
                alias($._raw_multiline_string_escape, $.raw_string_escape),
                alias($._raw_multiline_string_interpolation,
                      $.raw_string_interpolation),
                alias($._raw_multiline_string_text, $.string_text),
            )),
            alias($._raw_multiline_string_close_delim,
                  $.raw_string_close_delim),
        ),

        _raw_multiline_string_interpolation: $ => seq(
            alias($._raw_multiline_interpolation_open_delim,
                  $.raw_string_interpolation_open_delim),
            repeat1_with_separator($._interpolated_item, ','),
            ')'
        ),

        string_literal: $ => choice(
            $._oneline_string_literal,
            $._multiline_string_literal,
        ),

        _oneline_string_literal: $ => seq(
            '"',
            repeat(choice(
                $.string_escape,
                $.string_interpolation,
                alias($._string_text, $.string_text),
            )),
            token.immediate('"'),
        ),

        string_escape: $ => choice(
            /\\[0\\tnr"']/,
            token(seq('\\u{', /[0-9a-fA-F]{1,8}/, '}')),
        ),

        string_interpolation: $ => seq(
            '\\(',
            comma_list_allow_trailing($._interpolated_item),
            ')'
        ),

        _interpolated_item: $ => seq(
            optional(field('label', $._label)),
            field('expression', $._expression),
        ),

        _multiline_string_literal: $ => seq(
            '"""',
            token.immediate('\n'),
            repeat(choice(
                $.string_escape,
                $.string_interpolation,
                alias($._multiline_string_text, $.string_text),
            )),
            '"""',
        ),

        void_literal: $ => prec('void_literal', seq('(', ')')),

        range_expression: $ => prec('range_expression',
            either_or_both_with_separator(
                //TODO: Disallow half-open partial range with only lower bound
                field('lower_bound', $._range_bound),
                choice('...', '..<'),
                field('upper_bound', $._range_bound),
            )
        ),

        _range_bound: $ => choice(
            $._any_identifier,
            $.implicit_closure_argument,
            $.numeric_literal,
            $.parenthesized_expression,
            $.self,
            $.string_literal,
            $.super,
        ),

        regex_literal: $ => choice(
            $._extended_regex_literal,
            $._multiline_regex_literal,
            $._oneline_regex_literal,
        ),

        _extended_regex_literal: $ => /#\/((\/[^#])|[^\n])+\/#/,

        _multiline_regex_literal: $ => seq(
            /#\/\n/,
            /(\/[^#]|[^/])*?\n\/#/,
        ),

        _oneline_regex_literal: $ => seq(
            '/',
            /[^ \t]?[^/]*[^ \t/]/,
            '/',
        ),

        subscript_expression: $ => prec.left('subscript_expression', seq(
            //TODO: Type expressions allowed?
            field('base', $._expression),
            optional(token.immediate('?')),
            $.subscriptor,
        )),

        subscriptor: $ => seq(
            '[',
            optional(comma_list_allow_trailing($.argument)),
            ']',
        ),

        ternary_conditional_expression: $ => prec.right(-1, seq(
            field('condition', $._expression),
            '?',
            field('then', $._expression),
            ':',
            field('else', $._expression),
        )),

        try_expression: $ => prec.right(seq(
            'try',
            optional(choice(
                token.immediate('?'),
                token.immediate('!'),
            )),
            $._expression,
        )),

        // A type reference used in an expression rather than as an annotation
        type_expression: $ => prec('type_expression', choice(
            $.array_type,
            $.dictionary_type,
            seq('(', $.existential_type, ')'),
            $.generic_type,
            // TODO: Optional type
            // TODO: Tuple type
            $._type_identifier
        )),

        unbounded_range_expression: $ => '...',

        yield_expression: $ => prec.right(seq(
            'yield', $._expression,
        )),

        _expression_statement: $ => prec.right(seq(
            choice(
                // Exclude `assignment_expression` from `guard` conditions and
                // similar places, because it can have an `if` or `switch` on
                // its RHS.
                $.assignment_expression,
                $._expression,
            ),
            $._semicolon,
        )),

        //MARK:- Declarations

        actor_declaration: $ => ref_declaration_rule($, 'actor'),

        class_declaration: $ => ref_declaration_rule($, 'class'),

        constant_declaration: $ => top_level_decl_rule(
            $, $._constant_decl_common
        ),

        constant_property_declaration: $ => type_member_decl_rule(
            $, $._constant_decl_common
        ),

        _constant_decl_common: $ => prec.right(seq(
            optional('async'),
            'let',
            $._property_binding,
            $._semicolon,
        )),

        _initializer: $ => seq(
            '=',
            field('value', choice(
                $.closure_expression,
                // *Not* `_r_expression` because this is used for conditions
                $._expression,
                $.operator_reference,
            )),
        ),

        deinit_declaration: $ => seq(
            optional($._attributes),
            'deinit',
            field('body', $.block),
        ),

        enum_declaration: $ => seq(
            optional($._attributes),
            optional(either_or_both_unordered(
                'indirect',
                $.access_level_modifier
            )),
            'enum',
            field('name', $._type_identifier),
            optional($.generic_parameter_list),
            optional($.inheritance_clause),
            optional($.generic_where_clause),
            $.enum_body,
        ),

        enum_body: $ => seq(
            '{',
            repeat($._type_member_declaration),
            '}'
        ),

        case_declaration: $ => prec.right(seq(
            optional('indirect'),
            'case',
            repeat1_with_separator(
                seq(
                    field('name', $._any_identifier),
                    optional(choice(
                        $.associated_values,
                        $.raw_value,
                    )),
                ),
                ','
            ),
            $._semicolon,
        )),

        associated_values: $ => seq(
            '(',
            repeat1_with_separator($._tuple_type_element, ','),
            ')'
        ),

        raw_value: $ => seq(
            '=',
            choice(
                $.string_literal,
                $.numeric_literal,
                $.boolean_literal,
            )
        ),

        extension_declaration: $ => seq(
            optional($._attributes),
            optional($.access_level_modifier),
            'extension',
            field('extended_type', $._type),
            optional($.inheritance_clause),
            optional($.generic_where_clause),
            $.type_body,
        ),

        function_declaration: $ => top_level_decl_rule(
            $,
            $._function_method_common,
            $._operator_fixity
        ),

        init_declaration: $ => seq(
            optional($._attributes),
            either_or_both_unordered(
                $.access_level_modifier,
                repeat($.init_modifier),
            ),
            'init',
            optional(choice('?', '!')),
            optional($.generic_parameter_list),
            $.parameter_list,
            optional(choice('throws', 'rethrows')),
            optional($.generic_where_clause),
            field('body', $.block),
        ),

        init_modifier: $ => choice(
            'convenience',
            'dynamic',
            'override',
            'required',
        ),

        method_declaration: $ => type_member_decl_rule(
            $,
            $._function_method_common,
            $._operator_fixity
        ),

        _function_method_common: $ => seq(
            'func',
            field('name', choice(
                $._any_identifier,
                $.operator_name,
            )),
            optional($.generic_parameter_list),
            $.parameter_list,
            optional($.function_result),
            optional($.generic_where_clause),
            field('body', $.block)
        ),

        parameter_list: $ => prec('parameter_list', seq(
            '(',
            optional(
                comma_list_allow_trailing($.parameter),
            ),
            ')',
        )),

        parameter: $ => seq(
            repeat($.attribute),
            optional(field('external_name', $._any_identifier)),
            field('local_name', $._label),
            $._parameter_type,
            optional($.default_value),
        ),

        _parameter_type: $ => any_subsequence(
            any_subsequence(
                $.arc_modifier,
                'inout',
                $.isolation_modifier,
            ),
            $.type_attributes,
            field('type', choice(
                $._type,
                $.variadic_type,
            ))
        ),

        default_value: $ => seq(
            '=',
            choice(
                $.closure_expression,
                $._expression,
                $.operator_reference,
            ),
        ),

        variadic_type: $ => seq(
            $._type, token.immediate('...')
        ),

        function_result: $ => either_or_both(
            $._result_effects,
            seq(
                '->',
                optional($.isolation_modifier),
                field('return_type', $._type),
            )
        ),

        _result_effects: $ => either_or_both(
            $.async_effect,
            $.throw_effect,
        ),

        async_effect: $ => 'async',

        throw_effect: $ => seq(
            choice(
                'throws',
                'rethrows',
            ),
            optional(
                seq(
                    '(', field('error_type', $.simple_type), ')'
                )
            )
        ),

        macro_declaration: $ => top_level_decl_rule(
            $,
            seq(
                'macro',
                field('name', $._any_identifier),
                optional($.generic_parameter_list),
                $.parameter_list,
                optional($.function_result),
                optional($.generic_where_clause),
                field('definition', $.macro_definition),
            )
        ),

        macro_definition: $ => seq(
            '=',
            $.macro_expansion_expression,
        ),

        operator_declaration: $ => prec.right(seq(
            $._operator_fixity,
            'operator',
            $.operator_name,
            optional(seq(
                ':',
                alias($.identifier, $.precedence_group),
            )),
            $._semicolon,
        )),

        _operator_fixity: $ => choice('infix', 'postfix', 'prefix'),

        precedencegroup_declaration: $ => seq(
            'precedencegroup',
            field('name', $._any_identifier),
            '{',
            repeat($._precedencegroup_attribute),
            '}',
        ),

        _precedencegroup_attribute: $ => choice(
            $.precedencegroup_assignment,
            $.precedencegroup_associativity,
            $.precedencegroup_relation,
        ),

        precedencegroup_assignment: $ => seq(
            'assignment', ':', $.boolean_literal,
        ),

        precedencegroup_associativity: $ => seq(
            'associativity',
            ':',
            choice('left', 'right', 'none'),
        ),

        precedencegroup_relation: $ => seq(
            choice('higherThan', 'lowerThan'),
            ':',
            field('group', repeat1_with_separator($._any_identifier, ',')),
        ),

        protocol_declaration: $ => seq(
            optional($._attributes),
            optional($.access_level_modifier),
            'protocol',
            field('name', $._type_identifier),
            optional($.generic_parameter_list),
            // Protocols have the special `AnyObject` constraint that can
            // appear first in their inheritance clauses, but since that would
            // also be _syntactically_ valid for any other type it is not
            // really within our purview here to specially validate that.
            optional($.inheritance_clause),
            optional($.generic_where_clause),
            $.protocol_body,
        ),

        protocol_body: $ => seq(
            '{',
            repeat($._protocol_member_decl),
            '}',
        ),

        _protocol_member_decl: $ => seq(
            choice(
                $.associatedtype,
                $.init_requirement,
                $.method_requirement,
                $.operator_requirement,
                $.property_requirement,
                $.subscript_requirement,
                $.typealias,
            ),
            $._semicolon,
        ),

        associatedtype: $ => seq(
            optional($._attributes),
            optional($.access_level_modifier),
            'associatedtype',
            field('name', $._type_identifier),
            optional($.generic_parameter_list),
            optional(field('constraints', $.inheritance_clause)),
            optional(seq(
                '=',
                field('default_value', $._type)
            )),
            optional($.generic_where_clause),
        ),

        init_requirement: $ => seq(
            optional($._attributes),
            'init',
            optional(choice('?', '!')),
            optional($.generic_parameter_list),
            $.parameter_list,
            optional(choice('throws', 'rethrows')),
            optional($.generic_where_clause),
        ),

        method_requirement: $ => seq(
            optional($._attributes),
            optional($.protocol_member_modifiers),
            'func',
            field('name', $._any_identifier),
            optional($.generic_parameter_list),
            $.parameter_list,
            optional($.function_result),
            optional($.generic_where_clause),
        ),

        operator_requirement: $ => seq(
            optional($._attributes),
            optional($.protocol_member_modifiers),
            'func',
            field('name', $.operator_name),
            optional($.generic_parameter_list),
            $.parameter_list,
            optional($.function_result),
            optional($.generic_where_clause),
        ),

        property_requirement: $ => seq(
            optional($.protocol_member_modifiers),
            'var',
            field('name', $._any_identifier),
            ':',
            field('property_type', $._type),
            '{',
            optional('mutating'), 'get', optional($._result_effects),
            optional(choice(
                'nonmutating set',
                'set'
            )),
            '}'
        ),

        subscript_requirement: $ => seq(
            optional($.protocol_member_modifiers),
            'subscript',
            optional($.generic_parameter_list),
            $.parameter_list,
            '->',
            field('return_type', $._type),
            optional($.generic_where_clause),
        ),

        protocol_member_modifiers: $ => any_subsequence(
            'mutating',
            'optional',
            'static'
        ),

        struct_declaration: $ => seq(
            optional($._attributes),
            optional($.access_level_modifier),
            'struct',
            field('name', $._type_identifier),
            optional($.generic_parameter_list),
            optional($.inheritance_clause),
            optional($.generic_where_clause),
            $.type_body,
        ),

        subscript_declaration: $ => seq(
            optional($._attributes),
            repeat($._member_decl_modifier),
            'subscript',
            optional($.generic_parameter_list),
            $.parameter_list,
            '->',
            field('return_type', $._type),
            optional($.generic_where_clause),
            choice(
                field('implicit_getter', $.block),
                $.accessor_block,
            ),
        ),

        typealias: $ => prec.right(seq(
            optional($._attributes),
            optional($.access_level_modifier),
            'typealias',
            field('name', $._type_identifier),
            optional($.generic_parameter_list),
            '=',
            field('value', $._type),
            optional($.generic_where_clause),
            $._semicolon,
        )),

        variable_declaration: $ => top_level_decl_rule(
            $,
            $._variable_property_common,
            // Additional prefixes
            'lazy'
        ),

        variable_property_declaration: $ => type_member_decl_rule(
            $, $._variable_property_common
        ),

        _variable_property_common: $ => choice(
            $._computed_property,
            $._stored_variable,
        ),

        _computed_property: $ => seq(
            'var',
            field('name', $._any_identifier),
            $._type_annotation,
            choice(
                field('implicit_getter', $.block),
                $.accessor_block,
            )
        ),

        _stored_variable: $ => seq(
            'var',
            $._property_binding,
            choice(
                $.property_observer_block,
                $._semicolon,
            ),
        ),

        accessor_block: $ => seq(
            '{',
            // Permissive. Semantically a getter or a reader is required, along
            // with any combination of the others.
            choice(
                repeat1(choice(
                    $.getter_clause,
                    $.initializer_clause,
                    $.modify_clause,
                    $.reader_clause,
                    $.setter_clause,
                    $.unsafe_accessor_clause,
                )),
            ),
            '}'
        ),

        getter_clause: $ => seq(
            optional($._attributes),
            optional('mutating'),
            'get',
            optional($._result_effects),
            $.block,
        ),

        initializer_clause: $ => seq(
            optional($._attributes),
            'init',
            optional($.init_accessor_parameter_list),
            $.block,
        ),

        init_accessor_parameter_list: $ => seq(
            '(',
            repeat1_with_separator(
                field('parameter', $.identifier),
                ','
            ),
            ')',
        ),

        modify_clause: $ => seq(
            optional($._attributes),
            optional('nonmutating'),
            '_modify',
            $.block,
        ),

        reader_clause: $ => seq(
            optional($._attributes),
            optional('mutating'),
            '_read',
            // Effects are not actually allowed right now but we can expect they
            // will be eventually.
            optional($._result_effects),
            $.block,
        ),

        setter_clause: $ => seq(
            optional($._attributes),
            optional('nonmutating'),
            'set',
            optional($._setter_name),
            $.block,
        ),

        unsafe_accessor_clause: $ => seq(
            optional($._attributes),
            optional('nonmutating'),
            choice(
                'unsafeAddress',
                'unsafeMutableAddress',
            ),
            optional($._setter_name),
            $.block,
        ),

        property_observer_block: $ => seq(
            $._variable_property_observer_block,
            '{',
            either_or_both_unordered(
                $.will_set_clause,
                $.did_set_clause,
            ),
            '}'
        ),

        did_set_clause: $ => seq(
            'didSet',
            optional($._setter_name),
            field('body', $.block),
        ),

        will_set_clause: $ => seq(
            'willSet',
            optional($._setter_name),
            field('body', $.block),
        ),

        //MARK:- Common rules

        //MARK:- Inheritance and generics

        inheritance_clause: $ => seq(
            ':',
            comma_list(
                seq(optional($.type_attributes),
                    choice(
                        $.simple_type,
                        $.protocol_composition_type,
                        $.protocol_nonconformance,
                    )),
            ),
        ),

        generic_argument_list: $ => seq(
            $._generic_arg_open_delim,
            '<',
            // Parameter packs make empty generic args legal
            optional(comma_list_allow_trailing(
                seq(
                    optional($.type_attributes),
                    $._type,
                )
            )),
            '>'
        ),

        generic_parameter_list: $ => seq(
            '<',
            comma_list_allow_trailing($.generic_parameter),
            '>'
        ),

        generic_parameter: $ => seq(
            choice(
                $.parameter_pack,
                field('name', $._type_identifier),
            ),
            optional(field('requirement', seq(
                ':',
                choice(
                    $.simple_type,
                    $.protocol_composition_type,
                    $.protocol_nonconformance,
                ),
            ))),
        ),

        parameter_pack: $ => seq(
            'each',
            field('name', $._type_identifier),
        ),

        generic_where_clause: $ => seq(
            'where',
            comma_list($.generic_requirement),
        ),

        generic_requirement: $ => seq(
            field('type', choice(
                $.simple_type,
                $.pack_expansion_type,
                $.pack_reference,
            )),
            choice(':', '=='),
            field('constraint', choice($._type)),
        ),

        where_clause: $ => seq(
            'where', $._expression
        ),

        //MARK:- Attributes and modifiers

        // Syntax only here; not all of these elements are valid _semantically_
        // in all places where this grammar accepts them.

        _attributes: $ => repeat1(choice(
            $.attribute,
            $.availability_attribute,
            $.backdeploy_attribute,
            $.objc_name_attribute,
        )),

        availability_attribute: $ => seq(
            '@available',
            '(',
            repeat1_with_separator($.platform_specifier, ','),
            repeat(seq(
                ',',
                $._availability_detail,
            )),
            ')',
        ),

        backdeploy_attribute: $ => seq(
            '@backDeployed',
            '(',
            'before:',
            $.platform_specifier,
            ')',
        ),

        platform_specifier: $ => choice(
            seq($.platform_name, $.version_number),
            alias('*', $.wildcard),
        ),

        platform_name: $ => choice(
            'BackDeploy',
            'iOS',
            'iOSApplicationExtension',
            'Linux',
            'macCatalyst',
            'macCatalystApplicationExtension',
            'macOS',
            'macOSApplicationExtension',
            'OpenBSD',
            'OSX',
            'OSXApplicationExtension',
            '_PackageDescription',
            'swift',
            'SwiftStdlib',
            'tvOS',
            'tvOSApplicationExtension',
            'watchOS',
            'watchOSApplicationExtension',
            'Windows',
        ),

        version_number: $ => /\d+(\.\d+){0,2}/,

        _availability_detail: $ => choice(
            $.availability_version_detail,
            $.availability_explanation,
        ),

        availability_version_detail: $ => choice(
            'deprecated',
            'noasync',
            'unavailable',
            seq(
                choice('introduced:',
                       'deprecated:',
                       'obsoleted:'),
                $.version_number,
            ),
        ),

        availability_explanation: $ => seq(
            choice('message:', 'renamed:'),
            $.string_literal,
        ),

        attribute: $ => prec.left(seq(
            '@',
            field('name', $._any_identifier),
            optional($.generic_argument_list),
            optional(alias($._attribute_arguments, $.argument_list)),
        )),

        _attribute_arguments: $ => seq(
            '(',
            repeat1_with_separator($.argument, ','),
            ')',
        ),

        objc_name_attribute: $ => seq(
            '@objc',
            optional(seq(
                '(',
                choice(
                    field('class_name', $.identifier),
                    $.objc_method_name
                ),
                ')',
            )),
        ),

        objc_method_name: $ => repeat1(field('label', $._label)),

        // Attributes that only apply to type descriptors
        type_attributes: $ => repeat1($.type_attribute),
        type_attribute: $ => choice(
            '@autoclosure',
            seq('@convention',
                '(',
                choice('block', 'c', 'swift'),
                ')'
               ),
            '@escaping',
            '@retroactive',
            '@Sendable',
            '@unchecked',
        ),

        // Modifiers that can be applied to decls outside a type body
        _top_level_modifier: $ => choice(
            $.arc_modifier,
            $.ownership_modifier,
            $.access_level_modifier,
        ),

        reference_decl_modifier: $ => choice(
            'distributed',
            'final',
            'open'
        ),

        ownership_modifier: $ => choice(
            seq(
                'unowned',
                optional(choice('(safe)', '(unsafe)',)),
            ),
            'weak',
        ),

        // Miscellaneous modifiers that only apply to type members
        member_modifier: $ => choice(
            'class',
            'distributed',
            'dynamic',
            'final',
            'lazy',
            'mutating',
            'nonisolated',
            'open',
            'override',
            'static',
        ),

        _member_decl_modifier: $ => prec.left(choice(
            $.access_level_modifier,
            $.arc_modifier,
            $.member_modifier,
            $.ownership_modifier,
        )),

        arc_modifier: $ => choice(
            'consuming',
            'borrowing',
            'nonconsuming',

            // Deprecated as of SE-0377 but still probably to be found in the
            // wild
            '__consuming',
            '__owned',
            '__shared',
        ),

        access_level_modifier: $ => seq(
            choice(
                'private',
                'fileprivate',
                'internal',
                'package',
                'public',
                // Although 'open' is officially an access level modifier, it only
                // applies to type members, whereas the rest of these can apply
                // to any declaration.
            ),
            optional(seq('(', 'set', ')'))
        ),

        isolation_modifier: $ => choice(
            'isolated',
            'sending',
        ),

        //MARK:- Patterns

        _binding_pattern: $ => choice(
            $._identifier_pattern,
            $.tuple_binding,
        ),

        _identifier_pattern: $ => seq(
            field('name', $._any_identifier),
            optional($._type_annotation),
        ),

        tuple_binding: $ => seq(
            '(',
            comma_list_2_allow_trailing($._tuple_binding_element),
            ')',
            optional($._type_annotation),
        ),

        _tuple_binding_element: $ => seq(
            // Labels are not allowed in variable decls, but they are in pattern
            // matches.
            optional(field('label', $._label)),
            $._any_identifier,
        ),

        // Patterns for switches, case conditions, etc.
        _matching_pattern: $ => choice(
            $.binding_match_pattern,
            $.call_pattern,
            $._cast_pattern,
            $.member_pattern,
            $.optional_pattern,
            $._primary_expression_pattern,
            $.range_pattern,
            $.tuple_pattern,
        ),

        binding_match_pattern: $ => prec.right(seq(
            choice('let', 'var'),
            field('binding', $._matching_pattern),
        )),

        call_pattern: $ => seq(
            field('function',  choice(
                $._any_identifier,
                $.generic_type,
            )),
            $.argument_list,
        ),

        _cast_pattern: $ => choice(
            $.as_pattern,
            $.is_pattern,
        ),

        as_pattern: $ => seq(
            field('operand', $._matching_pattern),
            'as',
            field('type', $._type),
        ),

        is_pattern: $ => seq(
            'is',
            $._type
        ),

        member_pattern: $ => seq(
            optional(field('base', choice(
                $.generic_type,
                $._type_identifier,
            ))),
            '.',
            repeat1_with_separator(
                field('member', $._any_identifier),
                '.'
            ),
            optional(seq(
                '(',
                optional($.assoc_value_patterns),
                ')',
            )),
        ),

        assoc_value_patterns: $ => repeat1_with_separator(
            seq(
                optional(field('label', $._label)),
                $._matching_pattern,
            ),
            ','
        ),

        tuple_pattern: $ => seq(
            '(',
            comma_list_2_allow_trailing($.tuple_pattern_element),
            ')',
        ),

        tuple_pattern_element: $ => seq(
            optional(field('label', $._label)),
            $._matching_pattern,
        ),

        optional_pattern: $ => prec.right(seq(
            $._matching_pattern,
            token.immediate('?'),
        )),

        _primary_expression_pattern: $ => choice(
            $.array_literal,
            $.dictionary_literal,
            $.escaped_identifier,
            $.identifier,
            $.key_path_expression,
            $._literal,
            $.regex_literal,
            $.selector_expression,
            $.self,
            $.super,
            $.synthesized_identifier,
        ),

        range_pattern: $ => either_or_both_with_separator(
            //TODO: Disallow half-open partial range with only lower bound
            field('lower_bound', $._range_pattern_bound),
            choice('...', '..<'),
            field('upper_bound', $._range_pattern_bound),
        ),

        _range_pattern_bound: $ => optionally_in_parens(choice(
            $._any_identifier,
            $.implicit_closure_argument,
            $.numeric_literal,
            $.self,
            $.string_literal,
            $.super,
        )),

        //MARK:- Types

        _type: $ => prec.left(choice(
            $.array_type,
            $.dictionary_type,
            $.existential_type,
            $.function_type,
            $.iuo_type,
            $.opaque_type,
            $.optional_type,
            $.pack_expansion_type,
            $.pack_reference,
            $.parenthesized_type,
            $.protocol_composition_type,
            $.simple_type,
            $.tuple_type,
            $.void_type,
        )),

        array_type: $ => seq(
            '[',
            field('element', $._type),
            ']'
        ),

        dictionary_type: $ => seq(
            '[',
            field('key', $._type),
            ':',
            field('value', $._type),
            ']'
        ),

        existential_type: $ => prec.right(choice(
            seq('any', alias($.simple_type, '_simple_type')),
            seq('any', $.protocol_nonconformance),
            seq('any', '(', $.protocol_composition_type, ')'),
            seq(
                '(',
                'any',
                alias($.simple_type, '_simple_type'),
                ')',
                choice(
                    '?',
                    seq('.', 'Type')
                ),
            ),
        )),

        function_type: $ => prec.right(1, seq(
            optional(choice(
                $.attribute,
                $.isolation_attribute,
            )),
            alias($._function_type_parameter_list, $.parameter_list),
            alias($._function_type_result, $.function_result),
        )),

        // Distinct from `function_result` because the arrow and type are
        // _required_ for a `function_type`.
        _function_type_result: $ => seq(
            optional($._result_effects),
            '->',
            optional($.isolation_modifier),
            field('return_type', $._type),
        ),

        isolation_attribute: $ => '@isolated(any)',

        _function_type_parameter_list: $ => choice(
            alias($.void_type, 'empty_list'),
            $._full_func_type_param_list,
        ),

        _full_func_type_param_list: $ => prec('_full_func_type_param_list', seq(
            '(',
            comma_list_allow_trailing(
                alias($._function_type_parameter, $.parameter),
            ),
            ')',
        )),

        _function_type_parameter: $ => seq(
            repeat($.attribute),
            optional(field('external_name', $._any_identifier)),
            optional(field('local_name', $._label)),
            $._parameter_type,
        ),

        // A type identifier that requires generic arguments, for use in places
        // that would cause a conflict with a bare identifer, such as member and
        // call expressions.
        generic_type: $ => seq(
            alias($._type_identifier, 'type_base_name'),
            $.generic_argument_list,
        ),

        iuo_type: $ => prec.left(1, seq(
            field('wrapped', $._type),
            token.immediate('!')
        )),

        opaque_type: $ => prec.right(seq(
            'some',
            choice(
                $.protocol_composition_type,
                $.protocol_nonconformance,
                alias($.simple_type, '_simple_type'),
            ),
        )),

        optional_type: $ => prec.left(1, seq(
            field('wrapped', $._type),
            token.immediate('?')
        )),

        pack_expansion_type: $ => prec.right(seq(
            'repeat',
            field(
                'pattern_type',
                choice(
                    $.member_type_pack,
                    $._type,
                )
            ),
        )),

        pack_reference: $ => seq('each', $.simple_type),

        member_type_pack: $ => seq(
            '(',
            'each',
            $.simple_type,
            ')',
            '.',
            $._type_identifier,
        ),

        parenthesized_type: $ => seq(
            '(', $._type, ')'
        ),

        protocol_composition_type: $ => prec.right(seq(
            $._composable_type,
            '&',
            $._composable_type,
        )),

        _composable_type: $ => choice(
            $.protocol_composition_type,
            $.protocol_nonconformance,
            $.simple_type,
        ),

        protocol_nonconformance: $ => seq(
            '~',
            $.simple_type,
        ),

        // A type that just consists of identifiers, generic args, and dots
        simple_type: $ => prec.right(repeat1_with_separator(
            choice(
                $.generic_type,
                //TODO: Make Foo?.Bar parse
                // $.optional_type,
                $._type_identifier,
            ),
            '.',
        )),

        tuple_type: $ => seq(
            '(',
            comma_list_2_allow_trailing($._tuple_type_element),
            ')'
        ),

        _tuple_type_element: $ => seq(
            optional(field('label', $._label)),
            $._type,
        ),

        void_type: $ => prec('void_type', seq('(', ')')),

        //MARK:- Miscellany

        block: $ => seq(
            '{',
            repeat($._statement),
            '}',
        ),

        _condition_list: $ => comma_list($._condition),

        _condition: $ => choice(
            $.case_condition,
            $._expression,
            $.optional_binding_condition,
            $.optional_shadowing_condition,
        ),

        case_condition: $ => seq(
            'case',
            field('pattern', $._matching_pattern),
            '=',
            field('value', $._expression),
        ),

        optional_binding_condition: $ => seq(
            choice('let', 'var'),
            $._binding_pattern,
            '=',
            alias($._expression, $.initializer),
        ),

        optional_shadowing_condition: $ => seq(
            choice('let', 'var'),
            $._binding_pattern,
        ),

        _property_binding: $ => repeat1_with_separator(
            prec('_property_binding', seq(
                $._binding_pattern,
                optional(field('initializer', $._property_initializer))
            )),
            ','
        ),

        _property_initializer: $ => seq(
            '=',
            field('value', $._r_expression),
        ),

        // "Argument" for set/willSet/didSet; name taken from the Swift grammar
        _setter_name: $ => field('setter_name', seq('(', $._any_identifier, ')')),

        _type_annotation: $ => seq(
            ':',
            field('type', $._type),
        ),

        identifier: $ => {
            // Characters that can start an identifier
            const id_head = /[_a-zA-Z\u00A8\u00AA\u00AD\u00AF\u00B2-\u00B5\u00B7-\u00BA\u00BC-\u00BE\u00C0-\u00D6\u00D8-\u00F6\u00F8-\u00FF\u0100-\u02FF\u0370-\u167F\u1681-\u180D\u180F-\u1DBF\u1E00-\u1FFF\u200B-\u200D\u202A-\u202E\u203F-\u2040\u2054\u2060-\u206F\u2070-\u20CF\u2100-\u218F\u2460-\u24FF\u2776-\u2793\u2C00-\u2DFF\u2E80-\u2FFF\u3004-\u3007\u3021-\u302F\u3031-\u303F\u3040-\uD7FF\uF900-\uFD3D\uFD40-\uFDCF\uFDF0-\uFE1F\uFE30-\uFE44\uFE47-\uFFFD\U00010000-\U0001FFFD\U00020000-\U0002FFFD\U00030000-\U0003FFFD\U00040000-\U0004FFFD\U00050000-\U0005FFFD\U00060000-\U0006FFFD\U00070000-\U0007FFFD\U00080000-\U0008FFFD\U00090000-\U0009FFFD\U000A0000-\U000AFFFD\U000B0000-\U000BFFFD\U000C0000-\U000CFFFD\U000D0000-\U000DFFFD\U000E0000-\U000EFFFD]/;
            // Characters legal in an identifier _after_ the first character
            const id_additional = /[0-9\u0300-\u036F\u1DC0-\u1DFF\u20D0-\u20FF\uFE20-\uFE2F]/;

            return token(seq(id_head, repeat(choice(id_head, id_additional))));
        },

        block_comment: $ => seq(
            '/*',
            $._block_comment_contents,
        ),

        line_comment: $ => token(seq(
            choice('//', '///'),
            /.*/,
        )),

        _label: $ => seq(
            choice($._any_identifier, $.synthesized_identifier),
            ':',
        ),

        statement_label: $ => $._label,

        type_body: $ => seq(
            '{', repeat($._type_member_declaration), '}'
        ),

        _semicolon: $ => choice(
            ';',
            $._automatic_semicolon,
        ),

        // Dummy rule to permit consumer introspection of grammar version. Note
        // that this is not reachable from any other rule.
        version_0_9_22: $ => '__tree_sitter_swifter_version__',
    }
});

function either_or_both(first, second) {
    return choice(
        first,
        second,
        seq(first, second),
    )
}

function either_or_both_unordered(first, second) {
    return choice(
        seq(first, optional(second)),
        seq(second, optional(first)),
    )
}

function either_or_both_with_separator(first, separator, second) {
    return choice(
        seq(first, separator),
        seq(separator, second),
        seq(first, separator, second),
    )
}

function any_subsequence(first, second, third) {
    return choice(
        seq(first, optional(second), optional(third)),
        seq(second, optional(third)),
        third,
    )
}

function repeat1_with_separator(rule, separator) {
    return seq(
        rule,
        repeat(seq(separator, rule)),
    )
}

// Match `rule • separator • rule` at least once
function repeat2_with_separator(rule, separator) {
    return seq(
        rule, separator,
        repeat1_with_separator(rule, separator)
    )
}

function comma_list(rule) {
    return repeat1_with_separator(rule, ',')
}

function comma_list_allow_trailing(rule) {
    return seq(
        repeat1_with_separator(rule, ','),
        optional(','),
    )
}

// Match `rule • ',' • rule` at least once
function comma_list_2_allow_trailing(rule) {
    return seq(
        repeat2_with_separator(rule, ','),
        optional(','),
    )
}

// Wrap the given rule so that it can also take modifiers appropriate to
// top-level declarations (but not those that apply to type members).
function top_level_decl_rule($, rule, ...other_prefixes) {
    return seq(
        optional($._attributes),
        optional(either_or_both_unordered(
            repeat1($._top_level_modifier),
            choice(...other_prefixes,),
        )),
        rule,
    )
}

// Wrap the given rule so that it can also take modifiers appropriate to
// type members.
function type_member_decl_rule($, rule, ...other_prefixes) {
    return seq(
        optional($._attributes),
        optional(either_or_both_unordered(
            repeat1($._member_decl_modifier),
            choice(...other_prefixes),
        )),
        rule,
    )
}

// Declare a compiler control statement node that can contain the given
// `statement_rule` in the condition bodies, which will either be top-level
// statements or type members.
function compiler_control_rule($, statement_rule) {
    return seq(
        '#if',
        $.compiler_condition,
        alias(repeat(statement_rule), $.compiler_condition_body),
        repeat(seq(
            '#elseif',
            $.compiler_condition,
            alias(repeat(statement_rule), $.compiler_condition_body),
        )),
        optional(seq(
            '#else',
            alias(repeat(statement_rule), $.compiler_condition_body),
        )),
        '#endif'
    )
}

function optionally_in_parens(rule) {
    return choice(
        rule,
        seq('(', rule, ')')
    )
}

// Actor and class decls are basically identical for purposes of this grammar.
function ref_declaration_rule($, keyword) {
    return seq(
        optional($._attributes),
        //TODO: Nested `private final class C {}` does not parse. (However
        // `final private class C {}` _does_.)
        optional(either_or_both_unordered(
            $.reference_decl_modifier,
            $.access_level_modifier,
        )),
        keyword,
        field('name', $._type_identifier),
        optional($.generic_parameter_list),
        optional($.inheritance_clause),
        optional($.generic_where_clause),
        $.type_body,
    )
}
